import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { editLichChieuAct } from "./module/action";
import "./taoLichChieu.css";
import Loader from "../../components/Loader/Loader";
export default function TaoLichChieu({ maPhim }) {
  const [state, setState] = useState({
    values: {
      maPhim: "",
      ngayChieuGioChieu: "",
      maRap: "",
      giaVe: "",
    },
    error: {
      ngayChieuGioChieu: "",
      maRap: "",
      giaVe: "",
    },
    formLichChieu: false,
    ngayChieuGioChieuValid: false,
    maRapValid: false,
    giaVeValid: false,
  });
  const dispatch = useDispatch();
  const handleOnChange = (e) => {
    const { value, name } = e.target;

    setState({
      ...state,
      values: {
        ...state.values,
        [name]: value,
        maPhim: maPhim,
      },
    });
    console.log(state);
  };
  //propss
  const loadingEDITlich = useSelector(
    (state) => state.quanliMovieReducer.loadingEDITlich
  );
  const dataEDITlich = useSelector(
    (state) => state.quanliMovieReducer.dataEDITlich
  );
  const errorEDITlich = useSelector(
    (state) => state.quanliMovieReducer.errorEDITlich
  );
  const handleErrors = (e) => {
    const { name, value } = e.target;
    let mess = value === "" ? `${name} không được bỏ trống` : "";
    let { ngayChieuGioChieuValid, maRapValid, giaVeValid } = state;
    switch (name) {
      case "ngayChieuGioChieu":
        ngayChieuGioChieuValid = mess === "" ? true : false;
        if (
          value &&
          !value.match(
            /(0[1-9]|[1-2][0-9]|3[0-1])[\/\-](0[1-9]|1[0-2])[\/\-][0-9]{4} (2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]/g
          )
        ) {
          mess = "Ngày chiếu giờ chiếu phải đúng định dạng dd/MM/yyyy hh:mm:ss";
          ngayChieuGioChieuValid = false;
        }
        console.log(mess, ngayChieuGioChieuValid);
        break;
      case "maRap":
        maRapValid = mess === "" ? true : false;
        break;
      case "giaVe":
        giaVeValid = mess === "" ? true : false;
        if (value && !value.match(/^[0-9]+$/)) {
          mess = "Chỉ nhập số";
          giaVeValid = false;
        }
        console.log(mess, giaVeValid);
        break;

      default:
        break;
    }
    setState({
      ...state,
      error: {
        ...state.error,
        [name]: mess,
      },
      maRapValid,
      ngayChieuGioChieuValid,
      giaVeValid,
      formLichChieu: maRapValid && ngayChieuGioChieuValid && giaVeValid,
    });
    console.log(state);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(editLichChieuAct(state.values));
  };
  const renderNoti = () => {
    return (
      (errorEDITlich && (
        <div className="alert alert-danger">{errorEDITlich.response.data}</div>
      )) ||
      (dataEDITlich && (
        <div className="alert alert-success">Thêm thành công</div>
      )) ||
      (loadingEDITlich && <Loader />)
    );
  };
  return (
    <div className="container-fluid">
      <div
        className="modal fade"
        id="createLichChieu"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLongTitle">
                Bạn muốn tạo lịch chiếu cho phim có mã {maPhim}
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="px-2 mb-2">
              <form>
                <div className="form-group">
                  <label htmlFor="ngayChieuGioChieu">
                    Ngày chiếu giờ chiếu
                  </label>
                  <input
                    id="ngayChieuGioChieu"
                    className="form-control"
                    name="ngayChieuGioChieu"
                    onChange={handleOnChange}
                    onBlur={handleErrors}
                    onKeyUp={handleErrors}
                  />
                </div>
                {state.error.ngayChieuGioChieu && (
                  <div className="alert alert-danger">
                    {state.error.ngayChieuGioChieu}
                  </div>
                )}
                <div className="form-group">
                  <label htmlFor="maRap">Mã Rạp</label>
                  <input
                    id="maRap"
                    className="form-control"
                    name="maRap"
                    onChange={handleOnChange}
                    onBlur={handleErrors}
                    onKeyUp={handleErrors}
                  />
                </div>
                {state.error.maRap && (
                  <div className="alert alert-danger">{state.error.maRap}</div>
                )}
                <div className="form-group">
                  <label htmlFor="giaVe">Giá Vé</label>
                  <input
                    id="giaVe"
                    className="form-control"
                    name="giaVe"
                    onChange={handleOnChange}
                    onBlur={handleErrors}
                    onKeyUp={handleErrors}
                  />
                </div>
                {state.error.giaVe && (
                  <div className="alert alert-danger">{state.error.giaVe}</div>
                )}
                {renderNoti()}
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={handleSubmit}
                >
                  Tạo lun
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
