import * as ActionType from "./constants";
import axios from "axios";
export const addActFilm = (data) => {
  let accessToken = "";
  accessToken = JSON.parse(localStorage.getItem("admin")).accessToken;
  return (dispatch) => {
    dispatch(actRequestAddFilm());
    axios({
      url: "https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/ThemPhim",
      method: "POST",
      data,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    })
      .then((res) => {
        console.log(res);
        dispatch(actSuccessAddFilm(res.data));
      })
      .catch((err) => {
        console.log(err);
        dispatch(actFailedAddFilm(err));
      });
  };
};
export const getActFilmGP = (data) => {
  return (dispatch) => {
    dispatch(actRequestGETFilm());
    axios({
      url: `https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=${data.maNhom}`,
      method: "GET",
    })
      .then((res) => {
        dispatch(actSuccessGETFilm(res.data));
      })
      .catch((err) => {
        dispatch(actFailedGETFilm(err));
      });
  };
};
export const getActFilmBoth = (data) => {
  return (dispatch) => {
    dispatch(actRequestGETFilm());
    axios({
      url: `https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=${data.maNhom}&tenPhim=${data.tenPhim}`,
      method: "GET",
    })
      .then((res) => {
        dispatch(actSuccessGETFilm(res.data));
      })
      .catch((err) => {
        dispatch(actFailedGETFilm(err));
      });
  };
};
export const getActFilmTenPhim = (data) => {
  return (dispatch) => {
    dispatch(actRequestGETFilm());
    axios({
      url: `https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?tenPhim=${data.tenPhim}`,
      method: "GET",
    })
      .then((res) => {
        dispatch(actSuccessGETFilm(res.data));
      })
      .catch((err) => {
        dispatch(actFailedGETFilm(err));
      });
  };
};
export const getActFilmNone = (data) => {
  return (dispatch) => {
    dispatch(actRequestGETFilm());
    axios({
      url: "https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim",
      method: "GET",
    })
      .then((res) => {
        dispatch(actSuccessGETFilm(res.data));
      })
      .catch((err) => {
        dispatch(actFailedGETFilm(err));
      });
  };
};
///EDIT
export const editActFilm = (item) => {
  return (dispatch) => {
    console.log(item);
    dispatch(actEditFlim(item));
  };
};
const actEditFlim = (item) => {
  return {
    type: ActionType.EDIT_MOVIE,
    payload: item,
  };
};
//edit submit
export const editSubmitAct = (item) => {
  let accessToken = "";
  accessToken = JSON.parse(localStorage.getItem("admin")).accessToken;
  return (dispatch) => {
    // dispatch(actRequestEDITFilm());
    axios({
      url: "https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/CapNhatPhim",
      method: "POST",
      data: item,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    })
      .then((res) => {
        dispatch(actSuccessEDITFilm(res.data));
      })
      .catch((err) => {
        dispatch(actFailedEDITFilm(err));
      });
  };
};
/// delete
export const deleteFlimAct = (maPhim) => {
  let accessToken = "";
  accessToken = JSON.parse(localStorage.getItem("admin")).accessToken;
  return (dispatch) => {
    dispatch(actRequestDELETEFilm());
    axios({
      url: `https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/XoaPhim?MaPhim=${maPhim}`,
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    })
      .then((res) => {
        console.log(res);
        dispatch(actSuccessDELETEFilm(res.data, maPhim));
      })
      .catch((err) => {
        console.log(err);
        dispatch(actFailedDELETEFilm(err));
      });
  };
};
// edit lịch chiếu phim
export const editLichChieuAct = (data) => {
  let accessToken = "";
  accessToken = JSON.parse(localStorage.getItem("admin")).accessToken;
  return (dispatch) => {
    dispatch(actRequestEDITLichChieu());
    axios({
      url: "https://movie0706.cybersoft.edu.vn/api/QuanLyDatVe/TaoLichChieu",
      method: "POST",
      data: data,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    })
      .then((res) => {
        dispatch(actSuccessEDITLichChieu(res.data));
      })
      .catch((err) => {
        dispatch(actFailedEDITLichChieu(err));
      });
  };
};
const actRequestAddFilm = () => {
  return {
    type: ActionType.ADDREQUEST_MOVIE,
  };
};
const actSuccessAddFilm = (data) => {
  return {
    type: ActionType.ADDSUCCESS_MOVIE,
    payload: data,
  };
};
const actFailedAddFilm = (error) => {
  return {
    type: ActionType.ADDFAILED_MOVIE,
    payload: error,
  };
};
const actRequestGETFilm = () => {
  return {
    type: ActionType.GETREQUEST_MOVIE,
  };
};
const actSuccessGETFilm = (data) => {
  return {
    type: ActionType.GETSUCCESS_MOVIE,
    payload: data,
  };
};
const actFailedGETFilm = (error) => {
  return {
    type: ActionType.GETFAILED_MOVIE,
    payload: error,
  };
};
const actRequestEDITFilm = () => {
  return {
    type: ActionType.EDITREQUEST_MOVIE,
  };
};
const actSuccessEDITFilm = (data) => {
  return {
    type: ActionType.EDITSUCCESS_MOVIE,
    payload: data,
  };
};
const actFailedEDITFilm = (error) => {
  return {
    type: ActionType.EDITFAILED_MOVIE,
    payload: error,
  };
};
const actRequestDELETEFilm = () => {
  return {
    type: ActionType.DELETEREQUEST_MOVIE,
  };
};
const actSuccessDELETEFilm = (data, maPhim) => {
  return {
    type: ActionType.DELETESUCCESS_MOVIE,
    payload: data,
    maPhim: maPhim,
  };
};
const actFailedDELETEFilm = (error) => {
  return {
    type: ActionType.DELETEFAILED_MOVIE,
    payload: error,
  };
};
const actRequestEDITLichChieu = () => {
  return {
    type: ActionType.EDITREQUEST_LichChieu,
  };
};
const actSuccessEDITLichChieu = (data) => {
  return {
    type: ActionType.EDITSUCCESS_LichChieu,
    payload: data,
  };
};
const actFailedEDITLichChieu = (error) => {
  return {
    type: ActionType.EDITFAILED_LichChieu,
    payload: error,
  };
};
