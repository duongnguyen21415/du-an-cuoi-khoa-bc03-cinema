import * as ActionType from "./constants";
import axios from "axios";
export const addAdmin = (user) => {
  let accessToken = "";
  if (localStorage.getItem("admin")) {
    accessToken = JSON.parse(localStorage.getItem("admin")).accessToken;
  }
  return (dispatch) => {
    dispatch(actAddAdminRequest());
    axios({
      url: "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/ThemNguoiDung",
      method: "POST",
      data: user,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    })
      .then((result) => {
        console.log(result);
        dispatch(actAddAdminSuccess(result.data));
      })
      .catch((error) => {
        dispatch(actAddAdminFailed(error));
        console.log(error);
      });
  };
};
export const editAdmin = () => {};
export const getAdmin = () => {
  return (dispatch) => {
    dispatch(actGETAdminRequest());
    axios({
      url: "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/LayDanhSachNguoiDung",
      method: "GET",
    })
      .then((res) => {
        console.log(res);
        dispatch(actGETAdminSuccess(res.data));
      })
      .catch((err) => {
        console.log(err);
        dispatch(actGETAdminFailed(err));
      });
  };
};
export const getAdmimWithName = (value) => {
  return (dispatch) => {
    dispatch(actGETAdminRequest());
    axios({
      url: `https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/LayDanhSachNguoiDung?tuKhoa=${value.hoTen}`,
      method: "GET",
    })
      .then((res) => {
        console.log(res);
        dispatch(actGETAdminSuccess(res.data));
      })
      .catch((err) => {
        console.log(err);
        dispatch(actGETAdminFailed(err));
      });
  };
};
export const getAdmimWithGP = (value) => {
  return (dispatch) => {
    dispatch(actGETAdminRequest());
    axios({
      url: `https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=${value.maNhom}`,
      method: "GET",
    })
      .then((res) => {
        console.log(res);
        dispatch(actGETAdminSuccess(res.data));
      })
      .catch((err) => {
        console.log(err);
        dispatch(actGETAdminFailed(err));
      });
  };
};
export const getAdmimBoth = (value) => {
  return (dispatch) => {
    dispatch(actGETAdminRequest());
    axios({
      url: `https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=${value.maNhom}&tuKhoa=${value.hoTen}`,
      method: "GET",
    })
      .then((res) => {
        console.log(res);
        dispatch(actGETAdminSuccess(res.data));
      })
      .catch((err) => {
        console.log(err);
        dispatch(actGETAdminFailed(err));
      });
  };
};
export const editActAdmin = (value) => {
  let accessToken = "";
  if (localStorage.getItem("admin")) {
    accessToken = JSON.parse(localStorage.getItem("admin")).accessToken;
  }
  return (dispatch) => {
    dispatch(actEDITAdminRequest());
    axios({
      url: "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung",
      data: value,
      method: "PUT",
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    })
      .then((res) => {
        console.log(res);
        dispatch(actEDITAdminSuccess(res.data));
      })
      .catch((err) => {
        console.log(err);
        dispatch(actEDITAdminFailed(err));
      });
  };
};
export const deleteActAdmin = (taiKhoan) => {
  let accessToken = "";
  if (localStorage.getItem("admin")) {
    accessToken = JSON.parse(localStorage.getItem("admin")).accessToken;
  }
  return (dispatch) => {
    dispatch(actDELETEAdminRequest());
    axios({
      url: `https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`,

      method: "DELETE",
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    })
      .then((res) => {
        console.log(res);
        dispatch(actDELETEAdminSuccess(res.data, taiKhoan));
      })
      .catch((err) => {
        console.log(err);
        dispatch(actDELETEAdminFailed(err));
      });
  };
};
const actAddAdminRequest = () => {
  return {
    type: ActionType.ADDADMIN_REQUEST,
  };
};

const actAddAdminSuccess = (data) => {
  return {
    type: ActionType.ADDADMIN_SUCCESS,
    payload: data,
  };
};

const actAddAdminFailed = (error) => {
  return {
    type: ActionType.ADDADMIN_FAILED,
    payload: error,
  };
};
const actGETAdminRequest = () => {
  return {
    type: ActionType.GETADMIN_REQUEST,
  };
};

const actGETAdminSuccess = (data) => {
  return {
    type: ActionType.GETADMIN_SUCCESS,
    payload: data,
  };
};

const actGETAdminFailed = (error) => {
  return {
    type: ActionType.GETADMIN_FAILED,
    payload: error,
  };
};
const actEDITAdminRequest = () => {
  return {
    type: ActionType.EDITADMIN_REQUEST,
  };
};

const actEDITAdminSuccess = (data) => {
  return {
    type: ActionType.EDITADMIN_SUCCESS,
    payload: data,
  };
};

const actEDITAdminFailed = (error) => {
  return {
    type: ActionType.EDITADMIN_FAILED,
    payload: error,
  };
};
const actDELETEAdminRequest = () => {
  return {
    type: ActionType.DELETEADMIN_REQUEST,
  };
};

const actDELETEAdminSuccess = (data, taiKhoan) => {
  return {
    type: ActionType.DELETEADMIN_SUCCESS,
    payload: data,
    taiKhoan: taiKhoan,
  };
};

const actDELETEAdminFailed = (error) => {
  return {
    type: ActionType.DELETEADMIN_FAILED,
    payload: error,
  };
};
