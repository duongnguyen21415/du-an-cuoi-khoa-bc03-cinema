import * as ActionType from "./constants";
let initialState = {
  loading: false,
  data: null,
  error: null,
  loadingGET: false,
  dataGET: null,
  errorGET: null,
  loadingEDIT: false,
  dataEDIT: null,
  errorEDIT: null,
  loadingDELETE: false,
  dataDELETE: null,
  errorDELETE: null,
};
const addADMINReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.ADDADMIN_FAILED:
      state.loading = false;
      state.data = null;
      state.error = action.payload;
      return { ...state };
    case ActionType.ADDADMIN_REQUEST:
      state.loading = true;
      state.data = null;

      state.error = null;
      return { ...state };
    case ActionType.ADDADMIN_SUCCESS:
      state.loading = false;
      state.data = action.payload;

      state.error = null;
      return { ...state };
    case ActionType.GETADMIN_FAILED:
      state.loadingGET = false;
      state.dataGET = null;
      state.errorGET = action.payload;
      return { ...state };
    case ActionType.GETADMIN_REQUEST:
      state.loadingGET = true;
      state.dataGET = null;

      state.errorGET = null;
      return { ...state };
    case ActionType.GETADMIN_SUCCESS:
      state.loadingGET = false;
      state.dataGET = action.payload;

      state.errorGET = null;
      return { ...state };
    case ActionType.EDITADMIN_FAILED:
      state.loadingEDIT = false;
      state.dataEDIT = null;
      state.errorEDIT = action.payload;
      return { ...state };
    case ActionType.EDITADMIN_REQUEST:
      state.loadingEDIT = true;
      state.dataEDIT = null;

      state.errorEDIT = null;
      return { ...state };
    case ActionType.EDITADMIN_SUCCESS:
      state.loadingEDIT = false;
      state.dataEDIT = action.payload;

      state.errorEDIT = null;

      return { ...state };
    case ActionType.DELETEADMIN_FAILED:
      state.loadingDELETE = false;
      state.dataDELETE = null;
      state.errorDELETE = action.payload;
      return { ...state };
    case ActionType.DELETEADMIN_REQUEST:
      state.loadingDELETE = true;
      state.dataDELETE = null;

      state.errorDELETE = null;
      return { ...state };
    case ActionType.DELETEADMIN_SUCCESS:
      state.loadingDELETE = false;
      state.dataDELETE = action.payload;

      state.errorDELETE = null;
      console.log("success", action.payload, action.taiKhoan, state.dataGET);
      state.dataGET.map((item, index) => {
        if (item.taiKhoan === action.taiKhoan) {
          state.dataGET.splice(index, 1);
        }
      });
      return { ...state };

    default:
      return { ...state };
  }
};
export default addADMINReducer;
