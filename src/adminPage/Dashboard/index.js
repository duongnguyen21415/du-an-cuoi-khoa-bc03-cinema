import React, { useCallback, useMemo, useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./index.scss";
import { getUserAct, getProfit } from "./module/action";
import { clearAct } from "./module/action";
import LsDatVe from "./LsDatVe";
export default function Dashboard() {
  const [state, setState] = useState({
    khachHang: 0,
  });
  const dataUser = useSelector(
    (state) => state.quanliDashboardReducer.dataUser
  );
  const errorUser = useSelector(
    (state) => state.quanliDashboardReducer.errorUser
  );
  const doanhThu = useSelector(
    (state) => state.quanliDashboardReducer.doanhThu
  );
  const objectName = useSelector(
    (state) => state.quanliDashboardReducer.objectName
  );
  const dispatch = useDispatch();

  const showDoanhThu = () => {};
  const showDoanhThuMemo = useMemo(() => showDoanhThu(), []);

  const renderDsNguoiDung = useEffect(() => {
    console.log("disMount");
    dispatch(getUserAct());
  }, []);
  const renderNoti = () => {
    return (
      errorUser && (
        <div className="alert alert-danger">{errorUser.response.data}</div>
      )
    );
  };
  useEffect(() => {
    return () => {
      console.log("clear index");
      dispatch(clearAct());
    };
  }, []);
  useEffect(() => {
    let khachHang = 0;
    console.log("showdoanh thu", dataUser);

    if (dataUser) {
      dataUser.forEach((item, index) => {
        if (item.maLoaiNguoiDung === "KhachHang") {
          khachHang += 1;

          dispatch(getProfit({ taiKhoan: item.taiKhoan }));
        }
      });
    }
    setState({
      khachHang: khachHang,
    });
  }, [dataUser]);

  const renderTopDoanhThu = () => {
    let objectMax = {};
    let objectMin = {};
    objectName.forEach((item, index) => {
      if (index === 0) {
        objectMax = item;
        objectMin = item;
      } else if (objectMax.doanhThu < item.doanhThu) {
        objectMax = item;
        console.log("2");
      }
      if (objectMin.doanhThu > item.doanhThu) {
        objectMin = item;
      }
    });
    return (
      objectName && (
        <div>
          <h3 className="text-success">
            Phim: {objectMax.tenPhim}, đạt top doanh thu:{objectMax.doanhThu}
          </h3>
          <h3 className="text-danger">
            Phim: {objectMin.tenPhim}, đội sổ doanh thu:{objectMin.doanhThu}
          </h3>
        </div>
      )
    );
  };

  return (
    <>
      {/* Navbar */}
      {console.log("render return ")}
      <h1>Thong ke nguoi dung</h1>
      <p>Tong so tai khoan: {dataUser && dataUser.length} tai khoan</p>
      {renderNoti()}

      <p>So tai khoan khach hang la: {state.khachHang}</p>
      <h3>Doanh thu tong cong {doanhThu ? doanhThu : "0d"}</h3>
      {renderTopDoanhThu()}

      <LsDatVe />
      <p>HElo</p>
      <p>HElo</p>
      <p>HElo</p>
      <p>HElo</p>

      <p>HElo</p>
      <p>HElo</p>

      <p>HElo</p>
      <p>HElo</p>
      <p>HElo</p>

      <p>HElo</p>
    </>
  );
}
