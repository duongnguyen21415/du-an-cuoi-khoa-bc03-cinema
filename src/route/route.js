import AddAdmin from "../adminPage/AddAdmin";
import AddFilm from "../adminPage/AddFilm";
import Dashboard from "../adminPage/Dashboard";
import DeleteFilm from "../adminPage/DeleteFilm";
import EditFilm from "../adminPage/EditFilm";
import DanMyNhan from "../components/Home/News/ListNews/DanMyNhan";
import DaoDienTyDo from "../components/Home/News/ListNews/DaoDienTyDo";
import Fast9 from "../components/Home/News/ListNews/Fast9";
import Loki from "../components/Home/News/ListNews/Loki";
import MortalKombat from "../components/Home/News/ListNews/MortalKombat";
import TiecTrangMau from "../components/Home/News/ListNews/TiecTrangMau";
import TruyCungGietTan from "../components/Home/News/ListNews/TruyCungGietTan";
import DetailMovie from "../homePages/DetailMovie/DetailMovie";
import Home from "../homePages/Home/Home";

const routeHome = [
  {
    exact: true,
    path: "/",
    component: Home,
  },
  {
    exact: false,
    path: "/detail-movie/:id",
    component: DetailMovie,
  },
  {
    exact: false,
    path: "/news/fast9",
    component: Fast9,
  },
  {
    exact: false,
    path: "/news/loki",
    component: Loki,
  },
  {
    exact: false,
    path: "/news/tiec-trang-mau",
    component: TiecTrangMau,
  },
  {
    exact: false,
    path: "/news/mortal-kombat",
    component: MortalKombat,
  },
  {
    exact: false,
    path: "/news/dan-my-nhan",
    component: DanMyNhan,
  },
  {
    exact: false,
    path: "/news/truy-cung-giet-tan",
    component: TruyCungGietTan,
  },
  {
    exact: false,
    path: "/news/dao-dien-ty-usd",
    component: DaoDienTyDo,
  },
];
const routeAdmin = [
  {
    exact: false,
    path: "/admin/dashboard",
    component: Dashboard,
  },
  {
    exact: false,
    path: "/admin/addfilm",
    component: AddFilm,
  },
  {
    exact: false,
    path: "/admin/editfilm",
    component: EditFilm,
  },
  {
    exact: false,
    path: "/admin/deletefilm",
    component: DeleteFilm,
  },
  {
    exact: false,
    path: "/admin/addAdmin",
    component: AddAdmin,
  },
];
export { routeHome, routeAdmin };
