import React, { memo } from "react";
import Post from "./Post";
function RenderComment({ posts, id, props }) {
  const renderTable = () => {
    console.log("1");
    return posts.map((post) => {
      if (post.data && post.data.id === id) {
        console.log(id);
        return (
          <Post
            key={post.id}
            profilePic={post.data.profilePic}
            image={post.data.image}
            message={post.data.message}
            timestamp={post.data.timestamp}
            username={post.data.username}
            danhGia={post.data.danhGia}
            likeComment={post.data.likeComment}
            id={post.id}
            props={props}
            nguoiLike={post.data.nguoiLike}
          />
        );
      }
    });
  };
  return <>{renderTable()}</>;
}
export default memo(RenderComment);
