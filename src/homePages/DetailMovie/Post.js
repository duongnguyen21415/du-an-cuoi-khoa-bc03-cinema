import React from "react";
import Avatar from "@material-ui/core/Avatar";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import { makeStyles } from "@material-ui/core/styles";
import Rating from "@material-ui/lab/Rating";
import Box from "@material-ui/core/Box";
import db from "./../LoginHome/firebase";
const labels = {
  0: "0",
  0.5: "0.5",
  1: "1.0",
  1.5: "1.5",
  2: "2.0",
  2.5: "2.5",
  3: "3.0",
  3.5: "3.5",
  4: "4.0",
  4.5: "4.5",
  5: "5.0",
};

const useStyles = makeStyles({
  root: {
    width: 100,
    display: "flex",
    alignItems: "center",
    lineHeight: "0px",

    flexDirection: "column",
    justifyContent: "center",
  },
});

function HoverRating(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Box ml={0} className="mb-2 h5" style={{ color: "green" }}>
        {labels[props.danhGia !== "" ? props.danhGia : 2.5]}
      </Box>

      <Rating
        name="hover-feedback"
        value={props.danhGia !== "" ? props.danhGia : 2.5}
        precision={0.5}
        size="small"
        readOnly
      />
    </div>
  );
}
export default function Post({
  profilePic,
  image,
  username,
  timestamp,
  message,
  danhGia,
  likeComment,
  id,
  props,
  nguoiLike,
}) {
  let taiKhoan =
    (localStorage.getItem("userGoo") &&
      JSON.parse(localStorage.getItem("userGoo")).uid) ||
    (localStorage.getItem("userFace") &&
      JSON.parse(localStorage.getItem("userFace")).uid) ||
    (localStorage.getItem("userHome") &&
      JSON.parse(localStorage.getItem("userHome")).taiKhoan);
  let d = new Date(timestamp?.toDate()) + "";
  const likeIcon = () => {
    if (
      localStorage.getItem("userHome") ||
      localStorage.getItem("userFace") ||
      localStorage.getItem("userGoo")
    ) {
      if (nguoiLike && nguoiLike.includes(taiKhoan)) {
        likeComment = likeComment - 1;
        nguoiLike.splice(nguoiLike.indexOf(taiKhoan, 1));
        db.collection("1234").doc(id).update({
          likeComment: likeComment,
          nguoiLike: nguoiLike,
        });
      } else {
        nguoiLike.push(taiKhoan);
        console.log("like");
        likeComment += 1;
        console.log(id);
        console.log(likeComment);
        db.collection("1234").doc(id).update({
          likeComment: likeComment,
          nguoiLike: nguoiLike,
        });
      }
    } else {
      props.props.history.push("/login");
    }
  };
  return (
    <div className="post">
      <div className="d-flex justify-content-between">
        <div className="post__Top">
          <Avatar src={profilePic} className="post__Avatar" />
          <div className="post__TopInfo">
            <h3>{username}</h3>
            <p>{d}</p>
          </div>
        </div>
        <HoverRating danhGia={danhGia} />
      </div>
      <div className="post__Bottom">
        <p>{message}</p>
      </div>
      <div className="post__image">
        <img src={image} alt="" />
      </div>
      <div className="post__options">
        <div className="post__option">
          {(localStorage.getItem("userHome") ||
            localStorage.getItem("userFace") ||
            localStorage.getItem("userGoo")) &&
          nguoiLike &&
          nguoiLike.includes(taiKhoan) ? (
            <button className="text-primary" onClick={likeIcon}>
              <ThumbUpIcon />
            </button>
          ) : (
            <button onClick={likeIcon}>
              <ThumbUpIcon />
            </button>
          )}
          <p>{likeComment}</p>
        </div>
      </div>
    </div>
  );
}
