import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class ShowTimes extends Component {
  renderShowTimes = () => {
    const { showTimes, id, hinhAnh, tenPhim } = this.props;
    console.log(showTimes);
    if (showTimes) {
      return (
        <>
          <div className="film__item">
            <div className="film__info">
              <div className="film__img">
                <Link to={`/detail-movie/${id}`}>
                  <img src={hinhAnh} alt />
                </Link>
              </div>
              <div className="film__detail">
                <p className="name">
                  <span>{id}</span>
                  {tenPhim}
                </p>
                <p className="time">
                  {showTimes[0].lichChieuPhim[0].thoiLuong} phút - Cinema - IMDB
                  - Steven
                </p>
              </div>
            </div>
            <div className="film__schedule">
              <div className="film__quality">2D Digital</div>
              <div className="film__time">
                {showTimes[0].lichChieuPhim.map((item) => {
                  return (
                    <Link
                      key={item.maLichChieu}
                      to={`/booking/${item.maLichChieu}`}
                    >
                      <span>
                        {new Date(item.ngayChieuGioChieu).toLocaleTimeString()}
                      </span>
                    </Link>
                  );
                })}
              </div>
            </div>
          </div>
        </>
      );
    }
  };

  render() {
    return <>{this.renderShowTimes()}</>;
  }
}

const mapStateToProps = (state) => {
  return {
    showTimes: state.theaterToolsHomeReducer.theaterShowTimesInDetailMovie,
  };
};

export default connect(mapStateToProps, null)(ShowTimes);
