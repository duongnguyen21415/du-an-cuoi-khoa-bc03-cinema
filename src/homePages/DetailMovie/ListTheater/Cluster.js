import React, { Component } from "react";
import { connect } from "react-redux";
import { actTheaterShowTimesInDetailMovie } from "../../../components/Home/Slides/Tools/ModulesTools/action";

class Cluster extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imagesCinema: [
        {
          maCumRap: "bhd",
          img: "/images/bhd-img-theater.jpg",
        },
        {
          maCumRap: "cgv",
          img: "/images/cgv-img-theater.png",
        },
        {
          maCumRap: "cns",
          img: "/images/cns-img-theater.jpg",
        },
        {
          maCumRap: "glx",
          img: "/images/galaxy-img-theater.jpg",
        },
        {
          maCumRap: "lot",
          img: "/images/lotte-img-theater.jpg",
        },
        {
          maCumRap: "meg",
          img: "/images/megastar-img-theater.jpg",
        },
      ],
    };
  }

  renderCluster = () => {
    const { TheaterCluster } = this.props;
    console.log(TheaterCluster);
    if (TheaterCluster) {
      return TheaterCluster.map((item, index) => {
        let imgCinema = this.state.imagesCinema.filter(
          (items) => items.maCumRap === item.maCumRap.slice(0, 3)
        );
        console.log(imgCinema);
        if (index === 0) {
          return (
            <>
              <div
                key={item.maCumRap}
                className="address__item"
                onClick={(e) => {
                  console.log(e.target);
                  this.handleTheaterShowtimes(item.tenCumRap);
                  let array = document.getElementsByClassName("shadow__item");
                  console.log(array, array[0].className);
                  for (let i = 0; i < array.length; i++) {
                    array[i].className = array[i].className.replace(
                      "active",
                      ""
                    );
                  }
                  e.target.className += " active";
                }}
              >
                <div className="address__img">
                  <img src={imgCinema[0].img} alt="imgCinema" />
                </div>
                <div className="address__info">
                  <span style={{ color: "#8bc541" }}>{item.tenCumRap}</span>
                </div>
                <div className="shadow__item active"></div>
              </div>
            </>
          );
        } else {
          return (
            <>
              <div
                key={item.maCumRap}
                className="address__item"
                onClick={(e) => {
                  console.log(e.target);
                  this.handleTheaterShowtimes(item.tenCumRap);
                  let array = document.getElementsByClassName("shadow__item");
                  console.log(array, array[0].className);
                  for (let i = 0; i < array.length; i++) {
                    array[i].className = array[i].className.replace(
                      "active",
                      ""
                    );
                  }
                  e.target.className += " active";
                }}
              >
                <div className="address__img">
                  <img src={imgCinema[0].img} alt="imgCinema" />
                </div>
                <div className="address__info">
                  <span style={{ color: "#8bc541" }}>{item.tenCumRap}</span>
                </div>
                <div className="shadow__item"></div>
              </div>
            </>
          );
        }
      });
    }
  };

  handleTheaterShowtimes = (tenCumRap) => {
    const { TheaterCluster } = this.props;
    let listTheaterMovie = TheaterCluster.filter(
      (item) => item.tenCumRap === tenCumRap
    );
    this.props.getShowTimes(listTheaterMovie);
  };

  render() {
    const { TheaterCluster } = this.props;
    return (
      <>
        {this.renderCluster()}
        {TheaterCluster ? (
          <>{this.handleTheaterShowtimes(TheaterCluster[0].tenCumRap)}</>
        ) : (
          <></>
        )}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    TheaterCluster: state.theaterToolsHomeReducer.theaterClusterInDetailMovie,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getShowTimes: (lichChieu) => {
      dispatch(actTheaterShowTimesInDetailMovie(lichChieu));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cluster);
