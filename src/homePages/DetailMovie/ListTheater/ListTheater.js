import React, { Component } from "react";
import { connect } from "react-redux";
import {
  actTheaterClusterInDetailMovie,
  fetchTheaterToolsHome,
} from "./../../../components/Home/Slides/Tools/ModulesTools/action";
import Loader from "../../../components/Loader/Loader";
import Cluster from "./Cluster";
import ShowTimes from "./ShowTimes";

class ListTheater extends Component {
  componentDidMount() {
    const { id } = this.props;
    this.props.getTheaterMovie(id);
  }

  renderLogoTheaterMovie = () => {
    const { loading, theaterMovie } = this.props;
    console.log("theater movie", theaterMovie);
    if (loading) return <Loader />;
    if (theaterMovie) {
      console.log(theaterMovie.heThongRapChieu);
      return theaterMovie.heThongRapChieu.map((item, index) => {
        console.log(item, theaterMovie.heThongRapChieu);
        if (index === 0) {
          return (
            <>
              <li key={item.maHeThongRap} name={item.maHeThongRap}>
                <a
                  onClick={(e) => {
                    this.handleTheaterClusterShowtimes(item.cumRapChieu);
                    let arrayLogoName = document.getElementsByClassName("logo");
                    for (let i = 0; i < arrayLogoName.length; i++) {
                      arrayLogoName[i].className = arrayLogoName[
                        i
                      ].className.replace("active", "");
                    }
                    e.target.className += " active";
                  }}
                >
                  <img src={item.logo} alt="logo" className="logo active" />
                </a>
              </li>
            </>
          );
        } else {
          return (
            <>
              <li key={item.maHeThongRap} name={item.maHeThongRap}>
                <a
                  onClick={(e) => {
                    this.handleTheaterClusterShowtimes(item.cumRapChieu);
                    let arrayLogoName = document.getElementsByClassName("logo");
                    for (let i = 0; i < arrayLogoName.length; i++) {
                      arrayLogoName[i].className = arrayLogoName[
                        i
                      ].className.replace("active", "");
                    }
                    e.target.className += " active";
                  }}
                >
                  <img src={item.logo} alt="logo" className="logo" />
                </a>
              </li>
            </>
          );
        }
      });
    }
  };

  handleTheaterClusterShowtimes = (cumRapChieu) => {
    this.props.getTheaterCluster(cumRapChieu);
  };

  render() {
    const { id, theaterMovie } = this.props;
    return (
      <>
        <section id="cum-rap" className="theaters theater-detail">
          <div className="container">
            <div className="theaters__info">
              <ul className="theaters__logo">
                {this.renderLogoTheaterMovie()}
              </ul>
              <div className="theaters__address scroll-bar">
                {this.props.theaterMovie ? (
                  <>
                    {this.handleTheaterClusterShowtimes(
                      this.props.theaterMovie.heThongRapChieu.length !== 0
                        ? this.props.theaterMovie.heThongRapChieu[0].cumRapChieu
                        : ""
                    )}
                  </>
                ) : (
                  <></>
                )}
                <Cluster />
              </div>
              <div className="theaters__film scroll-bar">
                {theaterMovie ? (
                  <>
                    <ShowTimes
                      id={id}
                      hinhAnh={theaterMovie.hinhAnh}
                      tenPhim={theaterMovie.tenPhim}
                    />
                  </>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </div>
        </section>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.theaterToolsHomeReducer.loading,
    theaterMovie: state.theaterToolsHomeReducer.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getTheaterMovie: (maPhim) => {
      dispatch(fetchTheaterToolsHome(maPhim));
    },
    getTheaterCluster: (cumRapChieu) => {
      dispatch(actTheaterClusterInDetailMovie(cumRapChieu));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListTheater);
