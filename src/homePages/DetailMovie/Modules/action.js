import * as ActionType from "./constants";
import axios from "axios";

export const fetchDetailMovieHome = (id) => {
  return (dispatch) => {
    dispatch(actDetailMovieHomeRequest());
    axios({
      url: `https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`,
      method: "GET",
    })
      .then((res) => {
        console.log("rap chieu trong detail", res.data, res.data.lichChieu[0]);
        let data = res.data.lichChieu.filter(
          (item) => new Date(item.ngayChieuGioChieu) >= new Date()
        );

        res.data.lichChieu = data;
        console.log("data tong sau filter", res.data);
        dispatch(actDetailMovieHomeSuccess(res.data));
      })
      .catch((err) => {
        dispatch(actDetailMovieHomeFail(err));
      });
  };
};

const actDetailMovieHomeRequest = () => {
  return {
    type: ActionType.DETAIL_MOVIE_HOME_REQUEST,
  };
};

const actDetailMovieHomeSuccess = (data) => {
  return {
    type: ActionType.DETAIL_MOVIE_HOME_SUCCESS,
    payload: data,
  };
};

const actDetailMovieHomeFail = (err) => {
  return {
    type: ActionType.DETAIL_MOVIE_HOME_FAIL,
    payload: err,
  };
};
