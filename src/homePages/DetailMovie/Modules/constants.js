export const DETAIL_MOVIE_HOME_REQUEST =
  "@detailMovieHomeReducer/DETAIL_MOVIE_HOME_REQUEST";
export const DETAIL_MOVIE_HOME_SUCCESS =
  "@detailMovieHomeReducer/DETAIL_MOVIE_HOME_SUCCESS";
export const DETAIL_MOVIE_HOME_FAIL =
  "@detailMovieHomeReducer/DETAIL_MOVIE_HOME_FAIL";
