import React, { Component } from "react";
import Footer from "../../components/Home/Footer/Footer";
import Modal from "../../components/Home/Modal/Modal";
import { connect } from "react-redux";
import { fetchDetailMovieHome } from "./Modules/action";
import Loader from "../../components/Loader/Loader";
import { actModalHomeSuccess } from "./../../components/Home/Modal/Modules/action";
import CommentBox from "./Comment";
import { makeStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import "./../../assets/sass/Home/Comment/detail.scss";
import ListTheater from "./ListTheater/ListTheater";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    "& > * + *": {
      marginLeft: theme.spacing(2),
    },
    width: "100%",
    height: "100%",
    position: "relative",
  },
}));

function CircularDeterminate({ danhGia }) {
  const classes = useStyles();
  let danhGiacirle = danhGia * 10;
  return (
    <div className={classes.root}>
      <div className="circleBorder w-100"></div>
      <CircularProgress
        variant="determinate"
        id="circleSuccess"
        className="ml-0"
        thickness={2.8}
        value={danhGiacirle}
      />

      <h2 id="soDiem">{danhGia}</h2>
    </div>
  );
}
class DetailMovie extends Component {
  constructor(props) {
    super(props);
    this.state = {
      danhGiaComment: null,
    };
  }
  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.getDetailMovie(id);
  }
  renderStart = (danhGia) => {
    if (danhGia >= 0 && danhGia < 2) {
      return (
        <div className="smallStar">
          <img src="/images/star1.2.png" alt />
        </div>
      );
    } else if (danhGia == 2) {
      return (
        <div className="smallStar">
          <img src="/images/star1.png" alt />
        </div>
      );
    } else if (danhGia > 2 && danhGia < 4) {
      return (
        <>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.2.png" alt />
          </div>
        </>
      );
    } else if (danhGia == 4) {
      return (
        <>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
        </>
      );
    } else if (danhGia > 4 && danhGia < 6) {
      return (
        <>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.2.png" alt />
          </div>
        </>
      );
    } else if (danhGia == 6) {
      return (
        <>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
        </>
      );
    } else if (danhGia > 6 && danhGia < 8) {
      return (
        <>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.2.png" alt />
          </div>
        </>
      );
    } else if (danhGia == 8) {
      return (
        <>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
        </>
      );
    } else if (danhGia > 8 && danhGia < 10) {
      return (
        <>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
        </>
      );
    } else if (danhGia == 10) {
      return (
        <>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
          <div className="smallStar">
            <img src="/images/star1.png" alt />
          </div>
        </>
      );
    }
  };
  conversionDate = (day) => {
    let ngayChieu = new Date(day);
    let ngay = ngayChieu.getDate();
    let thang = ngayChieu.getMonth() + 1;
    let nam = ngayChieu.getFullYear();
    if (String(ngay).length < 2) {
      ngay = "0" + ngay;
    }
    if (String(thang).length < 2) {
      thang = "0" + thang;
    }
    return ngay + "." + thang + "." + nam;
  };
  //render Lich chiếu
  renderLichChieu = () => {
    this.setState({
      danhGiaComment: null,
    });
  };
  //render Commment
  renderComment = () => {
    this.setState({
      danhGiaComment: "Comment",
    });
  };
  renderInfoMovie = () => {
    const { data } = this.props;
    console.log("this  props id", this.props);
    if (data && data.lichChieu[0] !== undefined) {
      return (
        <>
          <div className="movie">
            <div className="movie-img styleBlur">
              <img src={data.hinhAnh} alt="background-detail-movie" />
            </div>
            <div className="styleGradient" />
            <div className="row filmPosterTop">
              <div className="col-3 filmInTop">
                <div className="posterMain">
                  <img src={data.hinhAnh} />
                  <button
                    className="playTrailerDetail"
                    type="button"
                    data-toggle="modal"
                    data-target={"#" + data.biDanh}
                    onClick={() => this.props.getModalItem(data)}
                  >
                    <img src="/images/play-video.png" alt />
                  </button>
                </div>
              </div>
              <div className="col-5 infoMain">
                <div>
                  <span className="detailMainInfo1">
                    {this.conversionDate(data.ngayKhoiChieu)}
                  </span>
                </div>
                <div>
                  <span>
                    <span className="ageType ">{data.maNhom}</span>
                    <span className="detailMainInfo1 h2">{data.tenPhim}</span>
                  </span>
                </div>
                <div>
                  <span className="detailMainInfo1">
                    {data.lichChieu[0] ? data.lichChieu[0].thoiLuong : "120"}{" "}
                    phút - 0 IMDb - 2D/Digital
                  </span>
                  <br />
                </div>
              </div>
              <div className="col-2"></div>
              <div className="col-2 circleStar">
                <div className="cirlcePercent c100">
                  <CircularDeterminate danhGia={data.danhGia} />
                </div>
                <div className="starMain justify-content-center">
                  {this.renderStart(data.danhGia)}
                </div>
              </div>
            </div>
            <div className="movie-content">
              <p>Nội dung</p>
              <p>{data.moTa}</p>
            </div>
          </div>
          <div className="banner-mobile d-none">
            <div className="posterMobile">
              <img src={data.hinhAnh} />
              <button
                className="playTrailerMobile"
                type="button"
                data-toggle="modal"
                data-target={"#" + data.biDanh}
                onClick={() => this.props.getModalItem(data)}
              >
                <img src="/images/play-video.png" alt />
              </button>
              <div className="styleGradient" />
            </div>
            <div className="detail-mobile">
              <div className="col-6 infoMobile">
                <div>
                  <span className="date-time">
                    {this.conversionDate(data.ngayKhoiChieu)}
                  </span>
                </div>
                <div>
                  <span>
                    <span className="ageType ">{data.maNhom}</span>
                    <span className="detailMainInfo1 h2">{data.tenPhim}</span>
                  </span>
                </div>
                <div>
                  <span className="date-time">
                    {data.lichChieu[0].thoiLuong} phút - 0 IMDb - 2D/Digital
                  </span>
                  <br />
                </div>
              </div>
              <div className="col-6 circleStar">
                <div className="cirlcePercent c100">
                  <CircularDeterminate danhGia={data.danhGia} />
                </div>
                <div className="starMain justify-content-center">
                  {this.renderStart(data.danhGia)}
                </div>
              </div>
            </div>
            <div className="content-mobile">
              <p>Nội dung</p>
              <p>{data.moTa}</p>
            </div>
          </div>
        </>
      );
    }
    if (data) {
      return (
        <>
          <div className="movie">
            <div className="movie-img styleBlur">
              <img src={data.hinhAnh} alt="background-detail-movie" />
            </div>
            <div className="styleGradient" />
            <div className="row filmPosterTop">
              <div className="col-3 filmInTop">
                <div className="posterMain">
                  <img src={data.hinhAnh} />
                  <button
                    className="playTrailerDetail"
                    type="button"
                    data-toggle="modal"
                    data-target={"#" + data.biDanh}
                    onClick={() => this.props.getModalItem(data)}
                  >
                    <img src="/images/play-video.png" alt />
                  </button>
                </div>
              </div>
              <div className="col-5 infoMain">
                <div>
                  <span className="detailMainInfo1">
                    {this.conversionDate(data.ngayKhoiChieu)}
                  </span>
                </div>
                <div>
                  <span>
                    <span className="ageType ">{data.maNhom}</span>
                    <span className="detailMainInfo1 h2">{data.tenPhim}</span>
                  </span>
                </div>
                <div>
                  <span className="detailMainInfo1">
                    {data.lichChieu[0] ? data.lichChieu[0].thoiLuong : "120"}{" "}
                    phút - 0 IMDb - 2D/Digital
                  </span>
                  <br />
                </div>
              </div>
              <div className="col-2"></div>
              <div className="col-2 circleStar">
                <div className="cirlcePercent c100">
                  <CircularDeterminate danhGia={data.danhGia} />
                </div>
                <div className="starMain justify-content-center">
                  {this.renderStart(data.danhGia)}
                </div>
              </div>
            </div>
            <div className="movie-content">
              <p>Nội dung</p>
              <p>{data.moTa}</p>
            </div>
          </div>
          <div className="banner-mobile d-none">
            <div className="posterMobile">
              <img src={data.hinhAnh} />
              <button
                className="playTrailerMobile"
                type="button"
                data-toggle="modal"
                data-target={"#" + data.biDanh}
                onClick={() => this.props.getModalItem(data)}
              >
                <img src="/images/play-video.png" alt />
              </button>
              <div className="styleGradient" />
            </div>
            <div className="detail-mobile">
              <div className="col-6 infoMobile">
                <div>
                  <span className="date-time">
                    {this.conversionDate(data.ngayKhoiChieu)}
                  </span>
                </div>
                <div>
                  <span>
                    <span className="ageType ">{data.maNhom}</span>
                    <span className="detailMainInfo1 h2">{data.tenPhim}</span>
                  </span>
                </div>
                <div>
                  <span className="date-time">phút - 0 IMDb - 2D/Digital</span>
                  <br />
                </div>
              </div>
              <div className="col-6 circleStar">
                <div className="cirlcePercent c100">
                  <CircularDeterminate danhGia={data.danhGia} />
                </div>
                <div className="starMain justify-content-center">
                  {this.renderStart(data.danhGia)}
                </div>
              </div>
            </div>
            <div className="content-mobile">
              <p>Nội dung</p>
              <p>{data.moTa}</p>
            </div>
          </div>
        </>
      );
    }
  };

  render() {
    if (this.props.loading) return <Loader />;
    return (
      <>
        <Modal />
        {this.state.danhGiaComment === null ? (
          <div className="detail-movie">
            {this.renderInfoMovie()}
            <div className="contentMain">
              <ul className="nav-center mb-3">
                <li className="lich-chieu">
                  <button
                    href="#"
                    className="showingInfo h3  text-decoration-none px-2 mr-2 btn btn-outline-danger"
                    onClick={this.renderLichChieu}
                  >
                    Lịch Chiếu
                  </button>
                </li>
                <li>
                  <button
                    href="#"
                    className="commentTab h3 text-decoration-none px-2 btn btn-outline-dark text-white"
                    onClick={this.renderComment}
                  >
                    Đánh Giá
                  </button>
                </li>
              </ul>
              <ListTheater id={this.props.match.params.id} />
            </div>
          </div>
        ) : (
          <div className="detail-movie">
            {this.renderInfoMovie()}
            <div className="contentMain">
              <ul className="nav-center mb-3">
                <li className="lich-chieu">
                  <button
                    href="#"
                    className="showingInfo h3  text-decoration-none  px-2 mr-2 btn btn-outline-dark text-white "
                    onClick={this.renderLichChieu}
                  >
                    Lịch Chiếu
                  </button>
                </li>
                <li className="active">
                  <button
                    href="#"
                    className="commentTab h3 text-decoration-none px-2 btn btn-outline-danger "
                    onClick={this.renderComment}
                  >
                    Đánh Giá
                  </button>
                </li>
              </ul>
              <div className="row">
                <div className="col-10 mx-auto">
                  <CommentBox props={this.props} />
                </div>
              </div>
            </div>
          </div>
        )}
        <Footer />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.detailMovieHomeReducer.loading,
    data: state.detailMovieHomeReducer.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getDetailMovie: (id) => {
      dispatch(fetchDetailMovieHome(id));
    },
    getModalItem: (movie) => {
      dispatch(actModalHomeSuccess(movie));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailMovie);
