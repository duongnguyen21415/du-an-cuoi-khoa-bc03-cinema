import React, { Component } from "react";
import Avatar from "@material-ui/core/Avatar";
import "./../../adminPage/AddFilm/taoLichChieu.css";
import "./../../assets/sass/Home/Comment/index.scss";
import Rating from "@material-ui/lab/Rating";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import PhotoCameraSharpIcon from "@material-ui/icons/PhotoCameraSharp";

import "./../../assets/sass/Home/Comment/post.scss";
import db from "./../LoginHome/firebase";
import firebase from "firebase";
import RenderComment from "./RenderComment";
const labels = {
  0: "0",
  0.5: "0.5",
  1: "1.0",
  1.5: "1.5",
  2: "2.0",
  2.5: "2.5",
  3: "3.0",
  3.5: "3.5",
  4: "4.0",
  4.5: "4.5",
  5: "5.0",
};

const useStyles = makeStyles({
  root: {
    width: 200,
    display: "flex",
    alignItems: "center",
    lineHeight: "0px",

    flexDirection: "column",
    justifyContent: "spacebetween",
  },
});

function HoverRating(props) {
  const [value, setValue] = React.useState(2.5);
  const [hover, setHover] = React.useState(-1);
  const classes = useStyles();

  return (
    <div className={classes.root}>
      {value !== null && (
        <Box ml={2} className="mb-2 ml-0 h1" style={{ color: "green" }}>
          {labels[hover !== -1 ? hover : value]}
        </Box>
      )}
      <Rating
        name="hover-feedback"
        value={value}
        precision={0.5}
        size="large"
        onChange={(event, newValue) => {
          setValue(newValue);
          console.log(newValue);
          props.danhGia(newValue);
        }}
        onChangeActive={(event, newHover) => {
          setHover(newHover);
        }}
      />
    </div>
  );
}
export default class CommentBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valueMes: "",
      imgUrl: "",
      danhGia: 2.5,
      posts: [],
    };
    this.close = React.createRef();
  }
  //map time
  // mapTimeComment = () => {
  //   console.log("1");
  //   return this.state.posts.map((post) => {
  //     if (post.data && post.data.id === this.props.id) {
  //       return (
  //         <Post
  //           key={post.id}
  //           profilePic={post.data.profilePic}
  //           image={post.data.image}
  //           message={post.data.message}
  //           timestamp={post.data.timestamp}
  //           username={post.data.username}
  //           danhGia={post.data.danhGia}
  //         />
  //       );
  //     }
  //   });
  // };
  danhGiaFun = (danhGia) => {
    this.setState({
      danhGia,
    });
  };
  handleOnSubmit = (e) => {
    e.preventDefault();

    if (
      localStorage.getItem("userGoo") ||
      localStorage.getItem("userFace") ||
      localStorage.getItem("userHome")
    ) {
      db.collection("1234").add({
        message: this.state.valueMes,
        timestamp: firebase.firestore.FieldValue.serverTimestamp(),
        profilePic:
          (localStorage.getItem("userGoo") &&
            JSON.parse(localStorage.getItem("userGoo")).photoURL) ||
          (localStorage.getItem("userFace") &&
            JSON.parse(localStorage.getItem("userFace")).photoURL) ||
          (localStorage.getItem("userHome") &&
            JSON.parse(localStorage.getItem("userHome"))
              .hoTen.slice(0, 1)
              .toUpperCase()),
        username:
          (localStorage.getItem("userHome") &&
            JSON.parse(localStorage.getItem("userHome")).hoTen) ||
          (localStorage.getItem("userGoo") &&
            JSON.parse(localStorage.getItem("userGoo")).displayName) ||
          (localStorage.getItem("userFace") &&
            JSON.parse(localStorage.getItem("userFace")).displayName),
        image: this.state.imgUrl,
        danhGia: this.state.danhGia,
        id: this.props.props.match.params.id,
        likeComment: 0,
        nguoiLike: [],
      });
    } else {
      this.props.props.history.push("/login");
    }

    this.setState({
      valueMes: "",
      imgUrl: "",
    });
    this.close.current.click();
  };
  dangNhapPage = () => {
    this.props.props.history.push("/login");
  };
  componentDidMount() {
    db.collection("1234").onSnapshot((snapshot) => {
      let arr = snapshot.docs.map((doc) => ({ id: doc.id, data: doc.data() }));

      if (arr) {
        let arrSapXep = arr.sort(function (a, b) {
          if (a.data && b.data) {
            return (
              new Date(b.data.timestamp?.toDate()) -
              new Date(a.data.timestamp?.toDate())
            );
          }
        });
        this.setState({
          posts: arrSapXep,
        });
      }
    });
  }

  render() {
    return (
      <div className="w-100">
        {localStorage.getItem("userHome") ||
        localStorage.getItem("userGoo") ||
        localStorage.getItem("userFace") ? (
          <button
            type="button"
            className="btn btn-primary d-flex w-100 align-items-center justify-content-between bg-white"
            data-toggle="modal"
            data-target="#exampleModalCenter"
          >
            <div className="d-flex align-items-center">
              {" "}
              {(localStorage.getItem("userHome") && (
                <Avatar>
                  {JSON.parse(localStorage.getItem("userHome"))
                    .hoTen.slice(0, 1)
                    .toUpperCase()}
                </Avatar>
              )) ||
                (localStorage.getItem("userGoo") && (
                  <Avatar
                    src={JSON.parse(localStorage.getItem("userGoo")).photoURL}
                  />
                )) ||
                (localStorage.getItem("userFace") && (
                  <Avatar
                    src={JSON.parse(localStorage.getItem("userFace")).photoURL}
                  />
                )) || <Avatar />}
              <span className="ml-1 text-dark">Bạn nghĩ gì về phim này?</span>
            </div>
            <img src="https://tix.vn/app/assets/img/icons/listStar.png" />
          </button>
        ) : (
          <button
            onClick={this.dangNhapPage}
            className="btn btn-outline-danger w-100"
          >
            Nhấp vào đây để đăng nhập và bình luận nào
          </button>
        )}
        <div
          className="modal fade"
          id="exampleModalCenter"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="exampleModalCenterTitle"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content px-3">
              <button
                type="button"
                className="close text-right"
                data-dismiss="modal"
                aria-label="Close"
                ref={this.close}
              >
                <span aria-hidden="true">×</span>
              </button>
              <div className="modal-body d-flex flex-column align-items-center">
                <HoverRating danhGia={this.danhGiaFun} />
                <form className="messageSender w-100 mb-3">
                  <input
                    value={this.state.valueMes}
                    className="messageSender_input w-100 px-1 pt-1 pb-5 mb-2"
                    placeholder="Bạn đang nghĩ gì?"
                    onChange={(e) =>
                      this.setState({ valueMes: e.target.value })
                    }
                  />
                  <div className="d-flex justify-content-between">
                    <input
                      className="btn btn-outline-success"
                      placeholder="Ảnh URL"
                      value={this.state.imgUrl}
                      onChange={(e) => {
                        this.setState({ imgUrl: e.target.value });
                        console.log(e.target.value);
                      }}
                    />
                    <button
                      type="submit"
                      onClick={this.handleOnSubmit}
                      className="btn btn-danger"
                    >
                      Đăng
                    </button>
                  </div>
                </form>
                <div></div>
              </div>
            </div>
          </div>
        </div>
        <RenderComment
          posts={this.state.posts}
          id={this.props.props.match.params.id}
          props={this.props}
        />
      </div>
    );
  }
}
