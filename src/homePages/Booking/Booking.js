import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchListBookingHome } from "./Modules/action";
import ListSeat from "./Seat/ListSeat";
import SeatName from "./InfoTickets/SeatName";
import Loader from "./../../components/Loader/Loader";
import { Link, NavLink } from "react-router-dom";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as yup from "yup";
import { fetchBookingHome } from "./Modules/BookingTickets/action";
import CountDownTimer from "./CountDownTimer/CountDownTimer";
import Avatar from "@material-ui/core/Avatar";

const bookingHomeSchema = yup.object().shape({
  email: yup
    .string()
    .required("* Vui lòng không để trống")
    .email("* Vui lòng nhập mail chính xác"),
  phone: yup
    .string()
    .required("* Vui lòng không để trống")
    .matches(
      /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/,
      "* Số đt ko hợp lệ"
    ),
});

class Booking extends Component {
  constructor(props) {
    super(props);
    this.state = {
      arraySeat: [],
      booking: {
        maLichChieu: 0,
        danhSachVe: [
          {
            maGhe: 0,
            giaVe: 0,
          },
        ],
        taiKhoanNguoiDung: "",
      },
      isLogin: true,
    };
  }

  logOutHome = () => {
    localStorage.removeItem("userHome") ||
      localStorage.removeItem("userGoo") ||
      localStorage.removeItem("userFace");
    this.setState({
      isLogin: !this.state.isLogin,
    });
  };

  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.getListBookingHome(id);
  }

  handleChosenSeat = (item) => {
    let arraySeatUpdate = [...this.state.arraySeat];
    if (arraySeatUpdate === "") {
      arraySeatUpdate.push(item);
      this.setState({ arraySeat: arraySeatUpdate });
    } else {
      let index = arraySeatUpdate.findIndex(
        (items) => items.maGhe === item.maGhe
      );
      if (index !== -1) {
        arraySeatUpdate.splice(index, 1);
      } else {
        arraySeatUpdate.push(item);
      }
      this.setState({ arraySeat: arraySeatUpdate });
    }
  };

  handleInfoSeat = () => {
    const { arraySeat } = this.state;
    if (arraySeat) {
      return arraySeat.map((item) => {
        return <SeatName key={item.maGhe} item={item} />;
      });
    }
  };

  handleTotalsPriceTickets = () => {
    const { arraySeat } = this.state;
    if (arraySeat != "") {
      return arraySeat.reduce((totals, item) => {
        return (totals += item.giaVe);
      }, 0);
    }
  };

  booking = () => {
    // let maGhe = [];
    // for (let i = 0; i < this.state.arraySeat.length; i++) {
    //   maGhe.push({
    //     maGhe: this.state.arraySeat[i].maGhe,
    //     giaVe: this.state.arraySeat[i].giaVe,
    //   });
    // }
    // this.setState(
    //   {
    //     ...this.state,
    //     booking: {
    //       maLichChieu: this.props.match.params.id,
    //       danhSachVe: maGhe,
    //       taiKhoanNguoiDung:
    //         JSON.parse(localStorage.getItem("userHome")).taiKhoan ||
    //         JSON.parse(localStorage.getItem("userGoo")).displayName ||
    //         JSON.parse(localStorage.getItem("userFace")).displayName,
    //     },
    //   },
    //   () => {
    //     console.log(this.state.booking);
    //     this.props.getTickets(this.state.booking, this.props.history);
    //   }
    // );
    if (localStorage.getItem("userHome")) {
      let maGhe = [];
      for (let i = 0; i < this.state.arraySeat.length; i++) {
        maGhe.push({
          maGhe: this.state.arraySeat[i].maGhe,
          giaVe: this.state.arraySeat[i].giaVe,
        });
      }
      this.setState(
        {
          ...this.state,
          booking: {
            maLichChieu: this.props.match.params.id,
            danhSachVe: maGhe,
            taiKhoanNguoiDung: JSON.parse(localStorage.getItem("userHome"))
              .taiKhoan,
          },
        },
        () => {
          console.log(this.state.booking);
          this.props.getTickets(this.state.booking, this.props.history);
        }
      );
    } else {
      alert(
        "Tính năng này chưa có trên tài khoản FaceBook và Google, để đặt vé bạn vui lòng truy cập bằng tài khoản bạn tự đăng ký. Thank you"
      );
    }
  };

  render() {
    if (
      !(
        localStorage.getItem("userHome") ||
        localStorage.getItem("userGoo") ||
        localStorage.getItem("userFace")
      )
    ) {
      this.props.history.push("/login");
    }
    console.log(this.props.match.params.id, this.props.data);
    const { data, loading } = this.props;
    if (loading) return <Loader />;
    return (
      <>
        {data ? (
          <>
            <div className="headerBooking" id="stepCheckout">
              <div>
                <ul className="pl-5">
                  <li className="active px-3" style={{ fontSize: 13 }}>
                    <span className="stepNumber mr-2">01 </span>
                    CHỌN GHẾ &amp; THANH TOÁN
                  </li>
                  <li className="px-3 step02">
                    <span className="stepNumber mr-2">02 </span>
                    KẾT QUẢ ĐẶT VÉ
                  </li>
                  <li className="mobile d-none">
                    <span className="stepNumber mr-2">03 </span>
                    THANH TOÁN
                  </li>
                  {localStorage.getItem("userHome") ||
                  localStorage.getItem("userGoo") ||
                  localStorage.getItem("userFace") ? (
                    <>
                      <li className="nav-item user-home d-flex align-items-center">
                        {(localStorage.getItem("userHome") && (
                          <Avatar>
                            {JSON.parse(localStorage.getItem("userHome"))
                              .hoTen.slice(0, 1)
                              .toUpperCase()}
                          </Avatar>
                        )) ||
                          (localStorage.getItem("userGoo") && (
                            <Avatar
                              src={
                                JSON.parse(localStorage.getItem("userGoo"))
                                  .photoURL
                              }
                            />
                          )) ||
                          (localStorage.getItem("userFace") && (
                            <Avatar
                              src={
                                JSON.parse(localStorage.getItem("userFace"))
                                  .photoURL
                              }
                            />
                          ))}
                        <span className="pr-3 pl-1">
                          {(localStorage.getItem("userHome") &&
                            JSON.parse(localStorage.getItem("userHome"))
                              .hoTen) ||
                            (localStorage.getItem("userGoo") &&
                              JSON.parse(localStorage.getItem("userGoo"))
                                .displayName) ||
                            (localStorage.getItem("userFace") &&
                              JSON.parse(localStorage.getItem("userFace"))
                                .displayName)}
                        </span>
                        <button
                          type="button"
                          className="btn btn-secondary btn-sigh-out"
                          onClick={this.logOutHome}
                        >
                          Đăng Xuất
                        </button>
                      </li>
                    </>
                  ) : (
                    <li className="nav-item">
                      <NavLink
                        className="nav-link icon__1 d-flex align-items-center"
                        to="/login"
                      >
                        <Avatar />
                        <span className="border-right pr-3 pl-1">
                          Đăng Nhập
                        </span>
                      </NavLink>
                    </li>
                  )}
                </ul>
              </div>
            </div>
            <div
              id="leftCheckout"
              style={{
                backgroundImage: `url(${data.thongTinPhim.hinhAnh})`,
              }}
            >
              <div className="modalleft" />
            </div>
            <div id="seatCheckout">
              <div id="seattv2">
                <div className="topContent">
                  <div className="lefttittle">
                    <div className="logocinema">
                      <Link to="/">
                        <img src="/images/logo-cinema-2.jpg" />
                      </Link>
                    </div>
                    <div className="contentcinema">
                      <p
                        className="mb-0"
                        style={{ color: "#9C9C9C", fontSize: 13 }}
                      >
                        <span className="text-dark">
                          {data.thongTinPhim.tenCumRap}
                        </span>
                      </p>
                      <p style={{ color: "#9b9b9b" }}>
                        - {data.thongTinPhim.gioChieu} -{" "}
                        {data.thongTinPhim.tenRap}
                      </p>
                    </div>
                  </div>
                  <CountDownTimer history={this.props.history} />
                </div>
                <div className="seatmap">
                  <div className="parent-panzoom">
                    <div id="seatmanroom">
                      <div className="sreen">
                        <div className="namesreen">
                          <img src="/images/screen.png" alt />
                        </div>
                      </div>
                      <ListSeat
                        listSeat={data}
                        handleChosenSeat={this.handleChosenSeat}
                      />
                    </div>
                  </div>
                  <div className="noteseat">
                    <div className="typeseats">
                      <div className="typeseat">
                        <i className="fa fa-couch d-block text-center"></i>
                        <span className="nameseat text-center">Ghế Thường</span>
                      </div>
                      <div className="typeseat">
                        <i
                          className="fa fa-couch d-block text-center"
                          style={{ color: "#f7b500" }}
                        ></i>
                        <span className="nameseat text-center">Ghế Vip</span>
                      </div>
                      <div className="typeseat">
                        <i
                          className="fa fa-couch d-block text-center"
                          style={{ color: "#00ac4d" }}
                        ></i>
                        <span className="nameseat text-center">
                          Ghế đang chọn
                        </span>
                      </div>
                      <div className="typeseat">
                        <i
                          className="fa fa-couch d-block text-center"
                          style={{ color: "#fb4226" }}
                        ></i>
                        <span className="nameseat text-center">
                          Ghế đã có người chọn
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="form-pay-mobile d-none">
                <div className="contentFullRight">
                  <div className="total">
                    <p className="totalCost">
                      {this.handleTotalsPriceTickets()} đ
                    </p>
                  </div>
                  <div className="chair">
                    <div className="info">
                      <span style={{ color: "#fb4226" }}>Ghế : </span>
                      {this.handleInfoSeat()}
                    </div>
                  </div>
                  <Formik
                    initialValues={{
                      email: "",
                      phone: "",
                    }}
                    onSubmit={this.booking}
                    validationSchema={bookingHomeSchema}
                  >
                    {() => (
                      <Form>
                        <div className="infouser">
                          <div className="form-group row">
                            <label
                              className="no-font-weight"
                              htmlFor="emailCheckout"
                            >
                              Email
                            </label>
                            <Field
                              id="emailCheckout"
                              type="text"
                              className="form-control"
                              name="email"
                            />
                            <ErrorMessage name="email">
                              {(msg) => (
                                <div style={{ color: "red" }}>{msg}</div>
                              )}
                            </ErrorMessage>
                          </div>
                        </div>
                        <div className="infouser">
                          <div className="form-group row">
                            <label
                              className="no-font-weight"
                              htmlFor="phoneCheckout"
                            >
                              Phone
                            </label>
                            <Field
                              className="form-control"
                              name="phone"
                              type="text"
                              id="phoneCheckout"
                            />
                            <ErrorMessage name="phone">
                              {(msg) => (
                                <div style={{ color: "red" }}>{msg}</div>
                              )}
                            </ErrorMessage>
                          </div>
                        </div>
                        <div className="buyticket">
                          <button type="submit">Đặt Vé</button>
                        </div>
                      </Form>
                    )}
                  </Formik>
                  <div className="methodpay">
                    <div className="titlemethodpay">Hình thức thanh toán</div>
                    <div
                      style={{
                        color: "#FB4226",
                        padding: "5px 0",
                        fontSize: 13,
                      }}
                    >
                      Vui lòng chọn ghế để hiển thị phương thức thanh toán phù
                      hợp.
                    </div>
                  </div>
                  <div className="notice">
                    <img className="icon" src="/images/exclamation.png" alt />
                    <span className="title">
                      Vé đã mua không thể đổi hoặc hoàn tiền <br />
                    </span>
                    <span className="title">
                      Mã vé sẽ được gửi qua tin nhắn
                      <span style={{ color: "#f79320" }}>ZMS</span>
                      (tin nhắn Zalo) và
                      <span style={{ color: "#f79320" }}>Email</span>
                      đã nhập.
                    </span>
                    <br />
                  </div>
                </div>
                <div />
              </div>
            </div>
            <div id="rightCheckout" className="form-pay">
              <div className="contentFullRight">
                <div className="total">
                  <p className="totalCost">
                    {this.handleTotalsPriceTickets()} đ
                  </p>
                </div>
                <div className="filmname">
                  <span className="c16">{data.danhSachGhe[0].maRap}</span>
                  <span className="movie-name">
                    {data.thongTinPhim.tenPhim}
                  </span>
                  <div className="contentcinema">
                    <div className="address">
                      <p
                        className="mb-0"
                        style={{ color: "#9C9C9C", fontSize: 16 }}
                      >
                        <span className="text-dark">
                          {data.thongTinPhim.tenCumRap}
                        </span>
                      </p>
                    </div>
                  </div>
                  <div>
                    {data.thongTinPhim.ngayChieu} - {data.thongTinPhim.gioChieu}
                    - {data.thongTinPhim.tenRap}
                  </div>
                </div>
                <div className="chair">
                  <div className="info">
                    <span style={{ color: "#fb4226" }}>Ghế : </span>
                    {this.handleInfoSeat()}
                  </div>
                  <div className="totalchair">
                    <span>Giá : {this.handleTotalsPriceTickets()} đ</span>
                  </div>
                </div>
                <Formik
                  initialValues={{
                    email: "",
                    phone: "",
                  }}
                  onSubmit={this.booking}
                  validationSchema={bookingHomeSchema}
                >
                  {() => (
                    <Form>
                      <div className="infouser">
                        <div className="form-group row">
                          <label
                            className="no-font-weight"
                            htmlFor="emailCheckout"
                          >
                            Email
                          </label>
                          <Field
                            id="emailCheckout"
                            type="text"
                            className="form-control"
                            name="email"
                            style={{ border: 0 }}
                          />
                          <ErrorMessage name="email">
                            {(msg) => <div style={{ color: "red" }}>{msg}</div>}
                          </ErrorMessage>
                        </div>
                      </div>
                      <div className="infouser">
                        <div className="form-group row">
                          <label
                            className="no-font-weight"
                            htmlFor="phoneCheckout"
                          >
                            Phone
                          </label>
                          <Field
                            className="form-control"
                            name="phone"
                            type="text"
                            id="phoneCheckout"
                          />
                          <ErrorMessage name="phone">
                            {(msg) => <div style={{ color: "red" }}>{msg}</div>}
                          </ErrorMessage>
                        </div>
                      </div>
                      <div className="buyticket">
                        <button type="submit">Đặt Vé</button>
                      </div>
                    </Form>
                  )}
                </Formik>
                <div className="methodpay">
                  <div className="titlemethodpay">Hình thức thanh toán</div>
                  <div
                    style={{ color: "#FB4226", padding: "5px 0", fontSize: 13 }}
                  >
                    Vui lòng chọn ghế để hiển thị phương thức thanh toán phù
                    hợp.
                  </div>
                </div>
                <div className="notice">
                  <img className="icon" src="/images/exclamation.png" alt />
                  <span className="title">
                    Vé đã mua không thể đổi hoặc hoàn tiền <br />
                  </span>
                  <span className="title">
                    Mã vé sẽ được gửi qua tin nhắn
                    <span style={{ color: "#f79320" }}>ZMS</span>
                    (tin nhắn Zalo) và
                    <span style={{ color: "#f79320" }}>Email</span>
                    đã nhập.
                  </span>
                  <br />
                </div>
              </div>
              <div />
            </div>
          </>
        ) : (
          <></>
        )}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.listBookingHomeReducer.loading,
    data: state.listBookingHomeReducer.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getListBookingHome: (id) => {
      dispatch(fetchListBookingHome(id));
    },
    getTickets: (tickets, history) => {
      dispatch(fetchBookingHome(tickets, history));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Booking);
