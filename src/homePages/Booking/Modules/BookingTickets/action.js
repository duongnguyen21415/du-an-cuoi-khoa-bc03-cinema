import * as ActionType from "./constants";
import axios from "axios";
import { Redirect } from "react-router-dom";

export const fetchBookingHome = (tickets, history) => {
  let accessToken = "";
  if (localStorage.getItem("userHome")) {
    accessToken = JSON.parse(localStorage.getItem("userHome")).accessToken;
  }
  return (dispatch) => {
    dispatch(actBookingHomeRequest());
    axios({
      url: "https://movie0706.cybersoft.edu.vn/api/QuanLyDatVe/DatVe",
      method: "POST",
      data: tickets,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    })
      .then((res) => {
        dispatch(actBookingHomeSuccess(res.data));
        alert("Bạn đã đặt vé thành công");
        history.push("/");
      })
      .catch((err) => {
        dispatch(actBookingHomeFail(err));
        alert("Chức năng đặt vé đang được cập nhật. Vui lòng quay lại sau");
        history.push("/");
      });
  };
};

const actBookingHomeRequest = () => {
  return {
    type: ActionType.BOOKING_HOME_REQUEST,
  };
};

const actBookingHomeSuccess = (data) => {
  return {
    type: ActionType.BOOKING_HOME_SUCCESS,
    payload: data,
  };
};

const actBookingHomeFail = (err) => {
  return {
    type: ActionType.BOOKING_HOME_FAIL,
    payload: err,
  };
};
