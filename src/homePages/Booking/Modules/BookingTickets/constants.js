export const BOOKING_HOME_REQUEST = "@bookingHomeReducer/BOOKING_HOME_REQUEST";
export const BOOKING_HOME_SUCCESS = "@bookingHomeReducer/BOOKING_HOME_SUCCESS";
export const BOOKING_HOME_FAIL = "@bookingHomeReducer/BOOKING_HOME_FAIL";
