import * as ActionType from "./constants";

let initialState = {
  loading: false,
  data: null,
  err: null,
};

const listBookingHomeReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.LIST_BOOKING_HOME_REQUEST:
      state.loading = true;
      state.data = null;
      state.err = null;
      return { ...state };
    case ActionType.LIST_BOOKING_HOME_SUCCESS:
      state.loading = false;
      state.data = action.payload;
      state.err = null;
      return { ...state };
    case ActionType.LIST_BOOKING_HOME_FAIL:
      state.loading = false;
      state.data = null;
      state.err = action.payload;
      return { ...state };
    default:
      return { ...state };
  }
};

export default listBookingHomeReducer;
