import * as ActionType from "./constants";
import axios from "axios";

export const fetchListBookingHome = (id) => {
  return (dispatch) => {
    dispatch(actListBookingHomeRequest());
    axios({
      url: `https://movie0706.cybersoft.edu.vn/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${id}`,
      method: "GET",
    })
      .then((res) => {
        dispatch(actListBookingHomeSuccess(res.data));
      })
      .catch((err) => {
        dispatch(actListBookingHomeFail(err));
      });
  };
};

const actListBookingHomeRequest = () => {
  return {
    type: ActionType.LIST_BOOKING_HOME_REQUEST,
  };
};

const actListBookingHomeSuccess = (data) => {
  return {
    type: ActionType.LIST_BOOKING_HOME_SUCCESS,
    payload: data,
  };
};

const actListBookingHomeFail = (err) => {
  return {
    type: ActionType.LIST_BOOKING_HOME_FAIL,
    payload: err,
  };
};
