export const LIST_BOOKING_HOME_REQUEST =
  "@listBookingHomeReducer/LIST_BOOKING_HOME_REQUEST";
export const LIST_BOOKING_HOME_SUCCESS =
  "@listBookingHomeReducer/LIST_BOOKING_HOME_SUCCESS";
export const LIST_BOOKING_HOME_FAIL =
  "@listBookingHomeReducer/LIST_BOOKING_HOME_FAIL";
