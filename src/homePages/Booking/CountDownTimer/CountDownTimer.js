import React, { Component } from "react";
let interval;
export default class CountDownTimer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      minutes: 5,
      seconds: "00",
    };
  }
  secondTime = () => {
    console.log("state.", this.state.seconds);
    if (
      this.state.seconds !== 0 &&
      this.state.seconds !== 1 &&
      this.state.seconds !== "00"
    ) {
      this.setState({
        seconds: --this.state.seconds,
      });
    } else if (this.state.seconds === 1) {
      this.setState({
        seconds: "00",
      });
    } else if (this.state.seconds === "00") {
      if (this.state.minutes !== 0) {
        this.setState({
          seconds: 59,
          minutes: --this.state.minutes,
        });
      } else {
        this.props.history.goBack();
      }
    }
  };

  componentDidMount() {
    // setInterval(this.secondTime, 1000);

    console.log("hiện");
    // setInterval(this.secondTime, 1000);
    interval = setInterval(this.secondTime, 1000);
    console.log("xuống");
    // setTimeout(() => clearInterval(interval), 315000);
  }
  // componentDidUpdate() {
  //   setInterval(this.secondTime, 1000);
  // }
  componentWillUnmount() {
    if (interval !== null) {
      clearInterval(interval);
    }
  }
  mapSecond = () => {
    if (this.state.seconds >= 1 && this.state.seconds <= 9) {
      let mapNumber = `0${this.state.seconds}`;
      return mapNumber;
    } else {
      return this.state.seconds;
    }
  };
  render() {
    return (
      <>
        <div className="righttittle">
          <p className="info1">thời gian giữ ghế</p>
          <p className="info2 font-weight-bold">
            <span className="minute">{this.state.minutes}</span>:
            <span className="second">{this.mapSecond()}</span>
          </p>
        </div>
      </>
    );
  }
}
