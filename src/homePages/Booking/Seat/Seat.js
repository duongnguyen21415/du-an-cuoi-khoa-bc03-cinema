import React, { Component } from "react";
import classNames from "classnames";

export default class Seat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      colorPickSeat: false,
      colorSeat: true,
      colorSeatPicked: true,
    };
  }

  handleSeat = (item) => {
    this.setState({
      colorPickSeat: !this.state.colorPickSeat,
      colorSeat: !this.state.colorSeat,
    });
    if (item.daDat === false) {
      this.props.handleChosenSeat(item);
    } else {
      alert("Ghế đã có người đặt. Vui lòng chọn ghế khác");
    }
  };

  render() {
    const { item } = this.props;
    console.log(item);
    return (
      <>
        {item.daDat === true ? (
          <>
            <button
              className={classNames("seat-wrapper", {
                colorSeatPicked: this.state.colorSeatPicked,
              })}
              onClick={() => {
                this.handleSeat(item);
              }}
            >
              <i className="fa fa-couch"></i>
            </button>
          </>
        ) : (
          <></>
        )}
        {item.loaiGhe === "Vip" && item.daDat === false ? (
          <>
            <button
              className={classNames("seat-wrapper", {
                colorPickSeat: this.state.colorPickSeat,
                colorSeat: this.state.colorSeat,
              })}
              onClick={() => {
                this.handleSeat(item);
              }}
            >
              <i className="fa fa-couch"></i>
            </button>
          </>
        ) : (
          <></>
        )}
        {item.loaiGhe === "Thuong" && item.daDat === false ? (
          <>
            <button
              className={classNames("seat-wrapper", {
                colorPickSeat: this.state.colorPickSeat,
              })}
              onClick={() => {
                this.handleSeat(item);
              }}
            >
              <i className="fa fa-couch"></i>
            </button>
          </>
        ) : (
          <></>
        )}
      </>
    );
  }
}
