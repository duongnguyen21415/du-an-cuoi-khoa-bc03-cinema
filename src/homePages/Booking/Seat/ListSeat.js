import React, { Component } from "react";
import Seat from "./Seat";

export default class ListSeat extends Component {
  renderListSeatNormal(a, b) {
    const { listSeat, handleChosenSeat } = this.props;
    if (listSeat && listSeat.danhSachGhe) {
      let listSeatNormal = listSeat.danhSachGhe.filter(
        (item) => item.loaiGhe === "Thuong"
      );
      let listSeatRow = listSeatNormal.slice(a, b);
      return listSeatRow.map((item) => {
        return (
          <Seat
            key={item.maGhe}
            item={item}
            handleChosenSeat={handleChosenSeat}
          />
        );
      });
    }
  }

  renderListSeatVip(a, b) {
    const { listSeat, handleChosenSeat } = this.props;
    if (listSeat && listSeat.danhSachGhe) {
      let listSeatVip = listSeat.danhSachGhe.filter(
        (item) => item.loaiGhe === "Vip"
      );
      let listSeatRow = listSeatVip.slice(a, b);
      return listSeatRow.map((item) => {
        return (
          <Seat
            key={item.maGhe}
            item={item}
            handleChosenSeat={handleChosenSeat}
          />
        );
      });
    }
  }

  render() {
    return (
      <>
        <div className="listseat">
          <div
            style={{
              textAlign: "left",
              margin: "auto",
              width: "fit-content",
            }}
          >
            <div className="rowseat">
              <span className="block-seat">
                <span className="rowname">
                  <span style={{ fontSize: 14 }}>A</span>
                </span>
              </span>
              {this.renderListSeatNormal(0, 10)}
            </div>
            <div className="rowseat">
              <span className="block-seat">
                <span className="rowname">
                  <span style={{ fontSize: 14 }}>B</span>
                </span>
              </span>
              {this.renderListSeatNormal(10, 20)}
            </div>
            <div className="rowseat">
              <span className="block-seat">
                <span className="rowname">
                  <span style={{ fontSize: 14 }}>C</span>
                </span>
              </span>
              {this.renderListSeatNormal(20, 30)}
            </div>
            <div className="rowseat">
              <span className="block-seat">
                <span className="rowname">
                  <span style={{ fontSize: 14 }}>D</span>
                </span>
              </span>
              {this.renderListSeatNormal(30, 40)}
            </div>
            <div className="rowseat">
              <span className="block-seat">
                <span className="rowname">
                  <span style={{ fontSize: 14 }}>E</span>
                </span>
              </span>
              {this.renderListSeatNormal(40, 50)}
            </div>
            <div className="rowseat">
              <span className="block-seat">
                <span className="rowname">
                  <span style={{ fontSize: 14 }}>F</span>
                </span>
              </span>
              {this.renderListSeatNormal(50, 60)}
            </div>
            <div className="rowseat">
              <span className="block-seat">
                <span className="rowname">
                  <span style={{ fontSize: 14 }}>G</span>
                </span>
              </span>
              {this.renderListSeatVip(0, 10)}
            </div>
            <div className="rowseat">
              <span className="block-seat">
                <span className="rowname">
                  <span style={{ fontSize: 14 }}>H</span>
                </span>
              </span>
              {this.renderListSeatVip(10, 20)}
            </div>
            <div className="rowseat">
              <span className="block-seat">
                <span className="rowname">
                  <span style={{ fontSize: 14 }}>I</span>
                </span>
              </span>
              {this.renderListSeatVip(20, 30)}
            </div>
            <div className="rowseat">
              <span className="block-seat">
                <span className="rowname">
                  <span style={{ fontSize: 14 }}>J</span>
                </span>
              </span>
              {this.renderListSeatVip(30, 40)}
              <span className="block-seat ">
                <span className="rowname m-0" />
              </span>
              <span className="block-seat " id="seat">
                <img className="entrance" src="/images/exit.png" alt />
              </span>
            </div>
          </div>
        </div>
      </>
    );
  }
}
