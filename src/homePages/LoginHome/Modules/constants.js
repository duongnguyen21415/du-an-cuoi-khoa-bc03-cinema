export const LOGIN_HOME_REQUEST = "@loginHomeReducer/LOGIN_HOME_REQUEST";
export const LOGIN_HOME_SUCCESS = "@loginHomeReducer/LOGIN_HOME_SUCCESS";
export const LOGIN_HOME_FAIL = "@loginHomeReducer/LOGIN_HOME_FAIL";
export const LOGOUT_HOME = "@logoutHomeReducer/LOGOUT_HOME";
export const LOGINWITHGOOGLE_SUCCESS =
  "@loginHomeReducer/LOGINWITHGOOGLE_SUCCESS";
export const LOGINWITHGOOGLE_FAILED =
  "@loginHomeReducer/LOGINWITHGOOGLE_FAILED";
export const LOGINWITHGOOGLE_REQUEST =
  "@loginHomeReducer/LOGINWITHGOOGLE_REQUEST";
