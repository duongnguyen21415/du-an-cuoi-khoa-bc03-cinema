import * as ActionType from "./constants";
import axios from "axios";

export const fetchLoginHome = (user, history) => {
  return (dispatch) => {
    dispatch(actLoginHomeRequest());
    axios({
      url: "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
      method: "POST",
      data: user,
    })
      .then((res) => {
        if (res.data.maLoaiNguoiDung === "QuanTri") {
          return Promise.reject({
            response: { data: "Tài khoản hoặc mật khẩu không đúng!" },
          });
        }
        history.goBack();
        dispatch(actLoginHomeSuccess(res.data));
        localStorage.setItem("userHome", JSON.stringify(res.data));
      })
      .catch((err) => {
        dispatch(actLoginHomeFail(err));
      });
  };
};
export const actFetchLogoutHome = () => {
  return (dispatch) => {
    dispatch(actLogoutHome());
  };
};
//Google
export const actLoginGoogle = (user, history) => {};
const actLogoutHome = () => {
  return {
    type: ActionType.LOGOUT_HOME,
  };
};

const actLoginHomeRequest = () => {
  return {
    type: ActionType.LOGIN_HOME_REQUEST,
  };
};

const actLoginHomeSuccess = (data) => {
  return {
    type: ActionType.LOGIN_HOME_SUCCESS,
    payload: data,
  };
};

const actLoginHomeFail = (err) => {
  return {
    type: ActionType.LOGIN_HOME_FAIL,
    payload: err,
  };
};
const actLoginGoogleRequest = () => {
  return {
    type: ActionType.LOGINWITHGOOGLE_REQUEST,
  };
};

const actLoginGoogleSuccess = (data) => {
  return {
    type: ActionType.LOGINWITHGOOGLE_SUCCESS,
    payload: data,
  };
};

const actLoginGoogleFailed = (err) => {
  return {
    type: ActionType.LOGINWITHGOOGLE_FAILED,
    payload: err,
  };
};
