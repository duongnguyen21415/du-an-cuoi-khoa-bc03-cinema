import React from "react";
import { Route } from "react-router-dom";
import Header from "./../components/Home/Header";
function LayoutHome(props) {
  return (
    <>
      <Header />
      {props.children}
    </>
  );
}

export default function HomeTemplate(props) {
  const { exact, path, component } = props;
  return (
    <LayoutHome>
      <Route exact={exact} path={path} component={component} />
    </LayoutHome>
  );
}
