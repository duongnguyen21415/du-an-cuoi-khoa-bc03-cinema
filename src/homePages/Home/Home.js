import React, { Component } from "react";
import ShowTimes from "../../components/Home/ShowTimes/ShowTimes";
import Slides from "../../components/Home/Slides/Slides";
import Modal from "./../../components/Home/Modal/Modal";
import Theaters from "./../../components/Home/Theaters/Theaters";
import News from "./../../components/Home/News/News";
import Apps from "../../components/Home/Apps/Apps";
import Footer from "../../components/Home/Footer/Footer";

export default class Home extends Component {
  render() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    return (
      <>
        <Modal />
        <Slides />
        <ShowTimes />
        <Theaters />
        <News props={this.props} />
        <Apps />
        <Footer />
      </>
    );
  }
}
