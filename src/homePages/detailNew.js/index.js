import React, { Component } from "react";
import Footer from "./../../components/Home/Footer/Footer";
export default class DetailNew extends Component {
  componentDidMount() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }
  render() {
    return (
      <>
        <section className="list-news">
          <div className="container ng-scope" id="detailNews">
            <div className="row">
              <div className="col-sm-12 title">
                <p className="ng-binding">
                  Dàn mỹ nhân trong thế giới điện ảnh của quái kiệt Christopher
                  Nolan
                </p>
                <div className="author ng-binding ng-scope" ng-if="!isMobile">
                  STEVEN <span className="ng-binding" /> 21.12.21
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 count">
                <div
                  className="wrapIcon like wrapIconLike"
                  data-type={1}
                  data-id={7848}
                  ng-click="likeAction($event, news, keyCache)"
                >
                  <img
                    className="iconFacebook postLike"
                    ng-src="app/assets/img/icons/like.png"
                    src="/images/like.png"
                  />
                  <span className="amount like ng-binding">0 LIKES</span>
                </div>
                <div className="wrapIcon wrapIconComment" ng-click="cmt()">
                  <img
                    className="iconFacebook"
                    src="/images/comment.png"
                    ng-click="cmt()"
                  />
                  <span className="amount ng-binding">0 BÌNH LUẬN</span>
                </div>
                <div className="wrapIcon wrapIconShare" ng-click="shareNews()">
                  <img
                    className="iconFacebook"
                    src="/images/sharing-icon.png"
                  />
                  <span className="amount share ng-binding">0 CHIA SẺ</span>
                </div>
              </div>
            </div>
            <div className="row">
              <div
                className="col-sm-12 content ng-binding"
                ng-bind-html="newsContent"
              ></div>
            </div>
          </div>
        </section>
      </>
    );
  }
}
