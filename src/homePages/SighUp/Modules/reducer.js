import * as ActionType from "./constants";

const initialState = {
  loading: false,
  data: null,
  error: null,
};

const sighUpHomeReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.SIGHUP_HOME_REQUEST:
      state.loading = true;
      state.data = null;
      state.err = null;
      return { ...state };

    case ActionType.SIGHUP_HOME_SUCCESS:
      state.loading = false;
      state.data = action.payload;
      state.err = null;
      return { ...state };

    case ActionType.SIGHUP_HOME_FAIL:
      state.loading = false;
      state.data = null;
      state.err = action.payload;
      return { ...state };

    default:
      return { ...state };
  }
};

export default sighUpHomeReducer;
