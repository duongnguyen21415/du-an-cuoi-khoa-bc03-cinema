import * as ActionType from "./constants";
import axios from "axios";

export const fetchSighUpHome = (user, history) => {
  return (dispatch) => {
    dispatch(actSighUpHomeRequest());
    axios({
      url: "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangKy",
      method: "POST",
      data: user,
    })
      .then((res) => {
        dispatch(actSighUpHomeSuccess(res.data));
        alert("Bạn đã đăng ký thành công");
        history.push("/");
      })
      .catch((err) => {
        dispatch(actSighUpHomeFail(err));
        alert("Tài khoản(mail) đã tồn tại. Vui lòng đăng ký lại");
      });
  };
};

const actSighUpHomeRequest = () => {
  return {
    type: ActionType.SIGHUP_HOME_REQUEST,
  };
};

const actSighUpHomeSuccess = (data) => {
  return {
    type: ActionType.SIGHUP_HOME_SUCCESS,
    payload: data,
  };
};

const actSighUpHomeFail = (err) => {
  return {
    type: ActionType.SIGHUP_HOME_FAIL,
    payload: err,
  };
};
