export const SIGHUP_HOME_REQUEST = "@sighUpHomeReducer/SIGHUP_HOME_REQUEST";
export const SIGHUP_HOME_SUCCESS = "@sighUpHomeReducer/SIGHUP_HOME_SUCCESS";
export const SIGHUP_HOME_FAIL = "@sighUpHomeReducer/SIGHUP_HOME_FAIL";
