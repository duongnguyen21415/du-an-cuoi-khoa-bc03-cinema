import { BrowserRouter, Route, Switch } from "react-router-dom";
import AdminTemplate from "./adminPage";
import AuthAdmin from "./adminPage/AuthPage";
import LoginHome from "./homePages/LoginHome/LoginHome";
import PageNotFound from "./homePages/PageNotFound/PageNotFound";
import { routeHome, routeAdmin } from "./route/route";
import HomeTemplate from "./homePages/HomeTemplate";
import { connect } from "react-redux";
import * as ActionType from "./homePages/LoginHome/Modules/constants";
import { Component } from "react";
import SighUp from "./homePages/SighUp/SighUp";
import Booking from "./homePages/Booking/Booking";

class App extends Component {
  renderRouteHome = (route) => {
    if (route && route.length > 0) {
      return route.map((item, index) => {
        return (
          <HomeTemplate
            key={index}
            exact={item.exact}
            path={item.path}
            component={item.component}
          />
        );
      });
    }
  };

  renderAdminHome = (route) => {
    if (route && route.length > 0) {
      return route.map((item, index) => {
        return (
          <AdminTemplate
            key={index}
            exact={item.exact}
            path={item.path}
            Component={item.component}
          />
        );
      });
    }
  };

  getUserHomeFromLocal = () => {
    const userHomeStr = localStorage.getItem("userHome");
    if (userHomeStr) {
      this.props.dispatch({
        type: ActionType.LOGIN_HOME_SUCCESS,
        payload: JSON.parse(userHomeStr),
      });
    }
  };

  componentDidMount() {
    this.getUserHomeFromLocal();
  }

  render() {
    return (
      <BrowserRouter>
        <Switch>
          {this.renderRouteHome(routeHome)}
          {this.renderAdminHome(routeAdmin)}
          <Route path="/admin/auth" component={AuthAdmin} />
          <Route path="/login" component={LoginHome} />
          <Route path="/sigh-up" component={SighUp} />
          <Route path="/booking/:id" component={Booking} />
          <Route path="*" component={PageNotFound} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default connect()(App);
