import React, { Component } from "react";
import Footer from "./../../Footer/Footer";
import CommentBoxNews from "./CommentBBox";
import { scroller } from "react-scroll";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import ChatBubbleIcon from "@material-ui/icons/ChatBubble";
import db from "./../../../../homePages/LoginHome/firebase.js";
import "./../../../../assets/sass/Home/news/index.scss";
import ShareIcon from "@material-ui/icons/Share";
let taiKhoan =
  (localStorage.getItem("userGoo") &&
    JSON.parse(localStorage.getItem("userGoo")).uid) ||
  (localStorage.getItem("userFace") &&
    JSON.parse(localStorage.getItem("userFace")).uid) ||
  (localStorage.getItem("userHome") &&
    JSON.parse(localStorage.getItem("userHome")).taiKhoan);
export default class TruyCungGietTan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      soLikeArr: [],
      soLike: 0,
      nguoiLikeFast: [],
      nguoiLikeLoki: [],
      nguoiLikeTiec: [],
      nguoiLikeKom: [],
    };
    this.newsMoreButton = React.createRef();
  }
  componentDidMount() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    db.collection("news").onSnapshot((snapshot) => {
      let arr = snapshot.docs.map((doc) => ({ id: doc.id, data: doc.data() }));
      let soLikeArr;
      db.collection("soLikeBV").onSnapshot((snap) => {
        soLikeArr = snap.docs.map((doc) => ({ id: doc.id, data: doc.data() }));
        let nguoiLikeFast;

        soLikeArr.forEach((item) => {
          if (item.data.id === "/news/truy-cung-giet-tan") {
            nguoiLikeFast = item.data.nguoiLike;
          }
        });

        this.setState({
          posts: arr,
          soLikeArr,
          nguoiLikeFast,
        });
      });
    });
  }
  likeIconBV = (id) => {
    if (
      localStorage.getItem("userHome") ||
      localStorage.getItem("userFace") ||
      localStorage.getItem("userGoo")
    ) {
      this.state.soLikeArr.forEach((item) => {
        if (
          item.data.id == id &&
          item.data.nguoiLike &&
          item.data.nguoiLike.includes(taiKhoan)
        ) {
          let soLike = item.data.soLike - 1;
          item.data.nguoiLike.splice(item.data.nguoiLike.indexOf(taiKhoan, 1));
          console.log(item.data.nguoiLike, "nguoi like");
          db.collection("soLikeBV").doc(item.id).update({
            nguoiLike: item.data.nguoiLike,
            soLike,
          });
        } else if (
          item.data.id == id &&
          !(item.data.nguoiLike && item.data.nguoiLike.includes(taiKhoan))
        ) {
          item.data.nguoiLike.push(taiKhoan);
          console.log(item.data.nguoiLike, "nguoi like");
          let soLike = item.data.soLike + 1;

          db.collection("soLikeBV").doc(item.id).update({
            nguoiLike: item.data.nguoiLike,
            soLike: soLike,
          });
        }
      });
    } else {
      console.log("props like", this.props);
      this.props.history.push("/login");
    }
  };
  renderSoLike = (id) => {
    let soLike = 0;
    this.state.soLikeArr.forEach((item) => {
      if (item.data.id === id) {
        soLike = item.data.soLike;
      }
    });
    return soLike;
  };
  renderCM = (id) => {
    let soCM = 0;
    this.state.posts.map((item) => {
      if (item.data.id === id) {
        soCM += 1;
      }
    });
    return soCM;
  };
  handleSrollTo = (name, to) => {
    scroller.scrollTo(name, {
      duration: 1000,
      smooth: true,
    });
  };

  render() {
    return (
      <>
        <section className="list-news">
          <div className="container ng-scope" id="detailNews">
            <div className="row">
              <div className="col-sm-12 title">
                <p className="ng-binding">
                  Truy Cùng Giết Tận - Cuộc tái ngộ của hai 'ông hoàng phòng vé'
                  xứ Hàn
                </p>
                {/* ngIf: !isMobile */}
                <div className="author ng-binding ng-scope" ng-if="!isMobile">
                  STEVEN <span className="ng-binding" /> 21.12.21
                </div>
                {/* end ngIf: !isMobile */}
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 count">
                <div
                  className="wrapIcon like wrapIconLike news2__icon mx-0"
                  data-type={1}
                  data-id={7848}
                  ng-click="likeAction($event, news, keyCache)"
                >
                  {(localStorage.getItem("userHome") ||
                    localStorage.getItem("userFace") ||
                    localStorage.getItem("userGoo")) &&
                  this.state.nguoiLikeFast &&
                  this.state.nguoiLikeFast.includes(taiKhoan) ? (
                    <button
                      className="text-primary mr-4"
                      onClick={() =>
                        this.likeIconBV("/news/truy-cung-giet-tan")
                      }
                    >
                      <ThumbUpIcon />
                    </button>
                  ) : (
                    <button
                      className="mr-4"
                      onClick={() =>
                        this.likeIconBV("/news/truy-cung-giet-tan")
                      }
                    >
                      <ThumbUpIcon />
                    </button>
                  )}
                  <span className="mr-5">
                    {this.renderSoLike("/news/truy-cung-giet-tan")} LIKE
                  </span>
                  <button
                    onClick={() =>
                      this.handleSrollTo("scrollTo", "/news/truy-cung-giet-tan")
                    }
                    className="mr-4"
                  >
                    <ChatBubbleIcon />
                  </button>
                  <span className="mr-5">
                    {this.renderCM("/news/truy-cung-giet-tan")} BÌNH LUẬN
                  </span>
                  <ShareIcon className="shareIcon mr-4" />
                  <span className="amount share ng-binding">0 CHIA SẺ</span>
                </div>
                <div
                  className="wrapIcon wrapIconShare"
                  ng-click="shareNews()"
                ></div>
              </div>
            </div>
            <div className="row">
              <div
                className="col-sm-12 content ng-binding"
                ng-bind-html="newsContent"
              >
                <p>
                  <strong>Truy Cùng Giết Tận</strong>&nbsp;(tựa Tiếng Anh:&nbsp;
                  <strong>
                    <em>Deliver Us From Evil)</em>
                  </strong>
                  &nbsp;là câu chuyện của In-nam (Hwang Jung Min) – một tay sát
                  thủ quyết định giải nghệ sau khi thực hiện hợp đồng cuối cùng
                  của mình. Chưa kịp rửa tay gác kiếm, In-nam chợt nhận được
                  thông tin con gái mình bị bắt cóc ở Thái Lan. In-nam đến Thái
                  Lan để tìm kiếm cô con gái anh chưa từng gặp mặt cùng với Yui
                  (Park Jeong Min). Cuộc chạy đua với thời gian trở nên khó khăn
                  hơn bao giờ hết khi cả hai phải đương đầu với sự bám đuổi gắt
                  gao từ Ray (Lee Jung-jae). Tên “đồ tể” máu lạnh truy sát
                  In-nam sau khi biết rằng anh trai mình đã bị giết hại, Ray
                  quyết tâm đòi lại món nợ máu này bằng mọi giá.
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    alt
                    src="https://s3img.vcdn.vn/123phim/2020/08/DUE_STI-PRE-20-c6ba27.jpg"
                    style={{ width: "90%" }}
                  />
                </p>
                <p>
                  Sau 7 năm kể từ&nbsp;<em>New World</em>&nbsp;– bộ phim đạt
                  thành tích hơn 4.68 triệu vé, hai tên tuổi lão làng trong làng
                  điện ảnh Hàn Quốc mới tiếp tục tái hợp trong&nbsp;
                  <strong>Truy Cùng Giết Tận</strong>&nbsp;– một bộ phim hành
                  động siêu “nặng đô”. Đạo diễn Hong Won Chan cho biết, phim sẽ
                  đưa các cảnh hành động lên một tầm cao mới, từ đánh nhau tay
                  đôi, đâm dao, bắn súng đến cả nổ bom! Bộ phim được quay tại ba
                  quốc gia Thái Lan, Nhật Bản và Hàn Quốc, trong đó Thái Lan là
                  bối cảnh chính chiếm khoảng 80% phim.&nbsp;
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    alt
                    src="https://s3img.vcdn.vn/123phim/2020/08/DUE_STI-PRE-11-86801a.jpg"
                    style={{ width: "90%" }}
                  />
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    alt
                    src="https://s3img.vcdn.vn/123phim/2020/08/DUE_STI-PRE-20-ef5a99.jpg"
                    style={{ width: "90%" }}
                  />
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    alt
                    src="https://s3img.vcdn.vn/123phim/2020/08/DUE_STI-PRE-15-af1f41.jpg"
                    style={{ width: "90%" }}
                  />
                </p>
                <p>
                  “Nhân vật trong phim rất đặc biệt nên bối cảnh phim cũng cần
                  thể hiện được điều đó. Bangkok là một thành phố vô cùng ấn
                  tượng. Phần lớn thành phố rất hiện đại, nhưng bên cạnh đó cũng
                  có những khía cạnh hỗn loạn và thần bí.” – đạo diễn Hong chia
                  sẻ. Đạo diễn hình ảnh của phim Hong Kyeong Pyo, cũng là người
                  đứng sau những thước phim đẹp ấn tượng của Kí Sinh Trùng đã
                  nhận xét&nbsp;<strong>Truy Cùng Giết Tận</strong>&nbsp;là một
                  thử thách đối với ông vì bối cảnh phim rất đa dạng. Ê kíp đã
                  vô cùng vất vả để thể hiện được những phong cách hành động
                  khác nhau thay đổi qua từng bối cảnh.
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    alt
                    src="https://s3img.vcdn.vn/123phim/2020/08/DUE_STI-PRE-16-2a953f.jpg"
                    style={{ width: "90%" }}
                  />
                </p>
                <p>
                  <strong>Truy Cùng Giết Tận</strong>&nbsp;dẫn đầu lượng đặt vé
                  sớm, chiếm tới 35.5% trước khi khởi chiếu tại Hàn vào ngày
                  05.08. Phim đang nhận được những lời khen có cánh từ giới phê
                  bình về cả nội dung và diễn xuất:&nbsp;
                  <em>
                    “Cuộc truy sát nghẹt thở giữa 2 người đàn ông có mục tiêu và
                    cảm xúc hoàn toàn đối lập!”, “Hành động gay cấn với diễn
                    xuất đỉnh cao của các diễn viên.”, “Bộ phim chiếm trọn mọi
                    khung hình trên màn ảnh rộng.”
                  </em>
                </p>
                <p>
                  Phim dự kiến sẽ ra mắt tại Việt Nam vào
                  <strong> 04.09.2020</strong>.
                </p>
                <p style={{ textAlign: "right" }}>
                  <strong>STEVEN</strong>
                </p>
              </div>
            </div>
            {/* ngIf: news.author_name !== '' */}
            <div className="row" id="scrollTo">
              <div className="col-sm-12 line">
                <hr />
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <div id="detailReviewer">
                  <div className="row isFlex detailMainStyle">
                    <div
                      className="col-sm-12 col-xs-12 dadInputReviewer dadInputReviewerNews newDesign"
                      ng-click="openCommentBox()"
                    >
                      {/* <span className="imgReviewer"> */}
                      {/*                  
                        <img
                          ng-if="user.user_id"
                          ng-src="https://graph.facebook.com/123455529585217/picture?type=square"
                          className="ng-scope"
                          src="https://graph.facebook.com/123455529585217/picture?type=square"
                        />
                      
                      </span>
                      <input
                        className="inputReviwer"
                        type="text"
                        placeholder="Bạn nghĩ gì về bài viết này?"
                        readOnly="readonly"
                      /> */}
                      <div className="row">
                        <div className="col-12 mx-auto">
                          <CommentBoxNews props={this.props} />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* LIST COMMENTS */}
                  <div id="listComment">
                    {/* ngRepeat: comment in newsComments */}
                  </div>
                </div>
                {/* ngIf: (newsComments.length > 5) && (showCurrentComment < newsComments.length) */}
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 line">
                <hr />
              </div>
            </div>
            <div className="news2">
              <div className="news2__item">
                <div className="news2__img">
                  <img src="/images/news2.png" alt="imagesNews" />
                </div>
                <div className="news2__info">
                  <h1>
                    Tiệc Trăng Máu chính thức cán mốc 100 tỷ chỉ sau 2 tuần công
                    chiếu
                  </h1>
                  <p>
                    Sau 2 tuần ra mắt, Tiệc Trăng Máu chính thức gia nhập câu
                    lạc bộ phim điện ảnh đạt 100 tỷ đồng doanh thu phòng vé. Dàn
                    ngôi sao “bạc tỷ” của phim cũng lần đầu tiên hội tụ đầy đủ
                    trong một khung hình để ăn mừng thành tích ấn tượng của tác
                    phẩm.
                  </p>
                  <div className="news2__icon">
                    <i className="fa fa-thumbs-up" />
                    <span>2</span>
                    <i className="fa fa-comment-alt" />
                    <span>0</span>
                  </div>
                </div>
              </div>
              <div className="news2__item">
                <div className="news2__img">
                  <img src="/images/mortal-kombat-1.png" alt="imagesNews" />
                </div>
                <div className="news2__info">
                  <h1>
                    [MORTAL KOMBAT: CUỘC CHIẾN SINH TỬ] - GỌI TÊN NHỮNG PHIM
                    ĐIỆN ẢNH NỔI TIẾNG ĐƯỢC CHUYỂN THỂ TỪ CÁC TỰA GAME ĐÌNH ĐÁM
                  </h1>
                  <p>
                    Bên cạnh những kịch bản gốc mới mẻ và đầy bất ngờ, Hollywood
                    cũng không thiếu những tác phẩm đình đám được chuyển thể từ
                    tiểu thuyết, phim hoạt hình, hay thậm chí là cả trò chơi
                    điện tử. Với ý tưởng thú vị cùng cốt truyện mang đậm tính
                    phiêu lưu, các tựa game đã trở thành nguồn cảm hứng độc đáo
                    cho các nhà làm phim. Không chỉ vậy, kể cả với các fan của
                    tựa game gốc, việc được thấy các nhân vật mình yêu thích
                    trên màn ảnh rộng cũng là một trải nghiệm không thể bỏ qua.
                    Hãy cùng điểm lại những bộ phim đình đám bậc nhất của
                    Hollywood được chuyển thể từ các trò chơi điện tử nhé.
                  </p>
                  <div className="news2__icon">
                    <i className="fa fa-thumbs-up" />
                    <span>1</span>
                    <i className="fa fa-comment-alt" />
                    <span>1</span>
                  </div>
                </div>
              </div>
              <div className="news2__item news2__item2">
                <div className="item2__row">
                  <div className="item2__info">
                    <div className="item2__img">
                      <img src="/images/news5.png" alt="imagesNews" />
                    </div>
                    <div className="item2__detail">
                      <p>
                        Dàn mỹ nhân trong thế giới điện ảnh của quái kiệt
                        Christopher Nolan
                      </p>
                    </div>
                  </div>
                </div>
                <div className="item2__row">
                  <div className="item2__info">
                    <div className="item2__img">
                      <img src="/images/fast9-1.jpg" alt="poster" />
                    </div>
                    <div className="item2__detail">
                      <p>
                        Lộ diện phản diện mới của series Fast and Furious,
                        'người đặc biệt' đã trở lại!
                      </p>
                    </div>
                  </div>
                </div>
                <div className="item2__row">
                  <div className="item2__info">
                    <div className="item2__img">
                      <img src="/images/news7.png" alt="imagesNews" />
                    </div>
                    <div className="item2__detail">
                      <p>
                        6 đạo diễn tỉ đô làm nên thành công của những bom tấn
                        đình đám nhất Hollywood
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="news__more">
              <button>XEM THÊM</button>
            </div>
            {/* ngIf: list_related.length > 4 */}
            {/* <div
              className="row isFlex text-center ViewMoreNews ng-scope"
              ng-if="list_related.length > 4"
            >
              <button
                id="btnNewsRelated"
                className="btnViewMore"
                ng-click="viewMoreNews()"
              >
                XEM THÊM
              </button>
            </div> */}
            {/* end ngIf: list_related.length > 4 */}
          </div>
        </section>
        <Footer />
      </>
    );
  }
}
