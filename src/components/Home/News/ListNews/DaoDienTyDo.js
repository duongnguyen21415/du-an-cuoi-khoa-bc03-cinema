import React, { Component } from "react";
import Footer from "./../../Footer/Footer";
import CommentBoxNews from "./CommentBBox";
import { scroller } from "react-scroll";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import ChatBubbleIcon from "@material-ui/icons/ChatBubble";
import db from "./../../../../homePages/LoginHome/firebase.js";
import "./../../../../assets/sass/Home/news/index.scss";
import ShareIcon from "@material-ui/icons/Share";
let taiKhoan =
  (localStorage.getItem("userGoo") &&
    JSON.parse(localStorage.getItem("userGoo")).uid) ||
  (localStorage.getItem("userFace") &&
    JSON.parse(localStorage.getItem("userFace")).uid) ||
  (localStorage.getItem("userHome") &&
    JSON.parse(localStorage.getItem("userHome")).taiKhoan);
export default class DaoDienTyDo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      soLikeArr: [],
      soLike: 0,
      nguoiLikeFast: [],
      nguoiLikeLoki: [],
      nguoiLikeTiec: [],
      nguoiLikeKom: [],
    };
    this.newsMoreButton = React.createRef();
  }
  componentDidMount() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    db.collection("news").onSnapshot((snapshot) => {
      let arr = snapshot.docs.map((doc) => ({ id: doc.id, data: doc.data() }));
      let soLikeArr;
      db.collection("soLikeBV").onSnapshot((snap) => {
        soLikeArr = snap.docs.map((doc) => ({ id: doc.id, data: doc.data() }));
        let nguoiLikeFast;

        soLikeArr.forEach((item) => {
          if (item.data.id === "/news/dao-dien-ty-usd") {
            nguoiLikeFast = item.data.nguoiLike;
          }
        });

        this.setState({
          posts: arr,
          soLikeArr,
          nguoiLikeFast,
        });
      });
    });
  }
  likeIconBV = (id) => {
    if (
      localStorage.getItem("userHome") ||
      localStorage.getItem("userFace") ||
      localStorage.getItem("userGoo")
    ) {
      this.state.soLikeArr.forEach((item) => {
        if (
          item.data.id == id &&
          item.data.nguoiLike &&
          item.data.nguoiLike.includes(taiKhoan)
        ) {
          let soLike = item.data.soLike - 1;
          item.data.nguoiLike.splice(item.data.nguoiLike.indexOf(taiKhoan, 1));
          console.log(item.data.nguoiLike, "nguoi like");
          db.collection("soLikeBV").doc(item.id).update({
            nguoiLike: item.data.nguoiLike,
            soLike,
          });
        } else if (
          item.data.id == id &&
          !(item.data.nguoiLike && item.data.nguoiLike.includes(taiKhoan))
        ) {
          item.data.nguoiLike.push(taiKhoan);
          console.log(item.data.nguoiLike, "nguoi like");
          let soLike = item.data.soLike + 1;

          db.collection("soLikeBV").doc(item.id).update({
            nguoiLike: item.data.nguoiLike,
            soLike: soLike,
          });
        }
      });
    } else {
      console.log("props like", this.props);
      this.props.history.push("/login");
    }
  };
  renderSoLike = (id) => {
    let soLike = 0;
    this.state.soLikeArr.forEach((item) => {
      if (item.data.id === id) {
        soLike = item.data.soLike;
      }
    });
    return soLike;
  };
  renderCM = (id) => {
    let soCM = 0;
    this.state.posts.map((item) => {
      if (item.data.id === id) {
        soCM += 1;
      }
    });
    return soCM;
  };
  handleSrollTo = (name, to) => {
    scroller.scrollTo(name, {
      duration: 1000,
      smooth: true,
    });
  };

  render() {
    return (
      <>
        <section className="list-news">
          <div className="container ng-scope" id="detailNews">
            <div className="row">
              <div className="col-sm-12 title">
                <p className="ng-binding">
                  6 đạo diễn tỉ đô làm nên thành công của những bom tấn đình đám
                  nhất Hollywood
                </p>
                {/* ngIf: !isMobile */}
                <div className="author ng-binding ng-scope" ng-if="!isMobile">
                  STEVEN <span className="ng-binding" /> 21.12.21
                </div>
                {/* end ngIf: !isMobile */}
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 count">
                <div
                  className="wrapIcon like wrapIconLike news2__icon mx-0"
                  data-type={1}
                  data-id={7848}
                  ng-click="likeAction($event, news, keyCache)"
                >
                  {/* <img
                    className="iconFacebook postLike"
                    ng-src="app/assets/img/icons/like.png"
                    src="/images/like.png"
                  />
                  <span className="amount like ng-binding">0 LIKES</span>
                </div>
                <div className="wrapIcon wrapIconComment" ng-click="cmt()">
                  <img
                    className="iconFacebook"
                    src="/images/comment.png"
                    ng-click="cmt()"
                  />
                  <span className="amount ng-binding">0 BÌNH LUẬN</span> */}
                  {(localStorage.getItem("userHome") ||
                    localStorage.getItem("userFace") ||
                    localStorage.getItem("userGoo")) &&
                  this.state.nguoiLikeFast &&
                  this.state.nguoiLikeFast.includes(taiKhoan) ? (
                    <button
                      className="text-primary mr-4"
                      onClick={() => this.likeIconBV("/news/dao-dien-ty-usd")}
                    >
                      <ThumbUpIcon />
                    </button>
                  ) : (
                    <button
                      className="mr-4"
                      onClick={() => this.likeIconBV("/news/dao-dien-ty-usd")}
                    >
                      <ThumbUpIcon />
                    </button>
                  )}
                  <span className="mr-5">
                    {this.renderSoLike("/news/dao-dien-ty-usd")} LIKE
                  </span>
                  <button
                    onClick={() =>
                      this.handleSrollTo("scrollTo", "/news/dao-dien-ty-usd")
                    }
                    className="mr-4"
                  >
                    <ChatBubbleIcon />
                  </button>
                  <span className="mr-5">
                    {this.renderCM("/news/dao-dien-ty-usd")} BÌNH LUẬN
                  </span>
                  <ShareIcon className="shareIcon mr-4" />
                  <span className="amount share ng-binding">0 CHIA SẺ</span>
                </div>
                <div
                  className="wrapIcon wrapIconShare"
                  ng-click="shareNews()"
                ></div>
              </div>
            </div>
            <div className="row">
              <div
                className="col-sm-12 content ng-binding"
                ng-bind-html="newsContent"
              >
                <p>
                  Với sự ra mắt của bom tấn&nbsp;
                  <strong>
                    <em>TENET</em>
                  </strong>
                  &nbsp;vào tháng 8 này, thầy phù thủy làm nên thành công
                  của&nbsp;<em>The Dark Knight</em>,&nbsp;
                  <em>Inception, Interstellar hay Dunkirk</em>&nbsp;-
                  Christopher Nolan tiếp tục ghi dấu ấn của mình trong danh sách
                  đạo diễn của những tượng đài phòng vé. Đây cũng là lúc để cùng
                  nhìn lại 6 gương mặt làm phim “mát tay” đem đến các tác phẩm
                  bạc tỉ sáng giá tại Hollywood.&nbsp;
                </p>
                <p>
                  <strong>1.&nbsp;Steven Spielberg</strong>
                </p>
                <p>
                  Chúng ta có thể nói gì về một cây đại thụ đã giữ vững phong độ
                  của mình trong hơn 50 năm tại Hollywood? Chính Steven
                  Spielberg đã mở đầu cho kỷ nguyên bom tấn bằng&nbsp;
                  <em>Jaws</em>&nbsp;năm 1975, rồi sau đó tiếp tục cho ra đời
                  hàng loạt tác phẩm đỉnh cao “càn quét” rạp chiếu phải nhắc tới
                  như loạt&nbsp;<em>Jurassic Park</em>&nbsp;(doanh thu 1 tỉ
                  USD),&nbsp;<em>ET: The Extra-Terrestrial</em>, series
                  <em>&nbsp;Indiana Jones</em>&nbsp;hay gần đây nhất là&nbsp;
                  <em>Ready Player One</em>.
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    alt
                    src="https://s3img.vcdn.vn/123phim/2020/08/Steven spielberg-c73ceb.jpg"
                    style={{ width: "90%" }}
                  />
                </p>
                <p>
                  Với 4 tỉ USD doanh thu nội địa và 6 tỉ USD từ thị trường quốc
                  tế, tổng doanh thu từ các phim mà ông đạo diễn đã cán mốc 10
                  tỉ USD. Trung bình, một bộ phim với Spielberg trên ghế chỉ đạo
                  sẽ thu về khoảng 330 triệu USD. Không chỉ định nghĩa cho khái
                  niệm phim bom tấn, phim của Steven Spielberg còn tiên phong
                  trong nhiều khía cạnh về nội dung và kỹ thuật điện ảnh. Có thể
                  nói,&nbsp; Spielberg đã trở thành một trong những nhà làm phim
                  quyền lực nhất thế kỷ 21 với gia tài đồ sộ trải dài từ phim
                  khoa học viễn tưởng cho tới các tác phẩm mang màu sắc lịch sử,
                  tôn giáo, nỗi đau diệt chủng của người Do Thái hay chủ nghĩa
                  khủng bố.
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    alt
                    src="https://s3img.vcdn.vn/123phim/2020/08/Steven spielberg - jurasic-3612d1.jpg"
                    style={{ width: "90%" }}
                  />
                </p>
                <p>
                  <strong>2.&nbsp;Anh em Joe và Anthony Russo</strong>
                </p>
                <p style={{ textAlign: "center" }}>
                  <strong>
                    <img
                      alt
                      src="https://s3img.vcdn.vn/123phim/2020/08/Russo-f5941d.jpg"
                      style={{ width: "90%" }}
                    />
                  </strong>
                </p>
                <p>
                  Nổi danh với thể loại siêu anh hùng, anh em nhà Russo là những
                  cái tên đứng sau thành công của các tựa phim Marvel đình đám
                  như&nbsp;
                  <em>
                    Avengers: Endgame, Avengers: Infinity War, Captain America:
                    Civil War
                  </em>
                  &nbsp;hay&nbsp;<em>Captain America: The Winter Soldier.</em>
                  &nbsp;Tổng doanh thu quốc tế từ các phim được Joe và Anthony
                  Russo “đỡ đầu” đã lên tới 6 tỉ USD, trong đó hơn 4 tỉ đô đến
                  từ siêu bom tấn&nbsp;<em>Endgame</em>&nbsp;và&nbsp;
                  <em>Infinity</em>&nbsp;như minh chứng cho khả năng “hốt bạc”
                  của bộ đôi này.&nbsp;
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    alt
                    src="https://s3img.vcdn.vn/123phim/2020/08/Russo 2-8f2574.jpg"
                    style={{ width: "90%" }}
                  />
                </p>
                <p>
                  Từ chỗ một cặp anh em vô danh khởi nghiệp với bộ phim sinh
                  viên kinh phí vỏn vẹn 10.000 USD, ngày nay bộ đôi đã ghi tên
                  mình vào danh sách những nhà làm phim xuất sắc nhất của thập
                  kỷ.&nbsp;&nbsp;
                </p>
                <p>
                  <strong>3. Peter Jackson</strong>
                </p>
                <p style={{ textAlign: "center" }}>
                  <strong>
                    <img
                      alt
                      src="https://s3img.vcdn.vn/123phim/2020/08/Peter Jackshons-4514f9.jpg"
                      style={{ width: "90%" }}
                    />
                  </strong>
                </p>
                <p>
                  14 bộ phim mà Peter Jackson tham gia trên cương vị một đạo
                  diễn có tổng doanh thu toàn cầu cán mốc hơn 6 tỉ USD trong đó
                  thành tựu lớn nhất chính là bộ ba&nbsp;<em>Chúa Nhẫn</em>
                  &nbsp;kinh điển và sau này là trilogy&nbsp;<em>Hobbit</em>
                  &nbsp;chuyển thể từ các tác phẩm đồ sộ của nhà văn J. R. R.
                  Tolkien. Peter Jackson không chỉ biết làm phim bom tấn, ông
                  còn biết làm thể nào để làm một phim bom tấn với kinh phí thấp
                  nhất. Tư duy làm phim đổi mới của ông khi quyết định quay cả
                  ba phần&nbsp;<em>Lord of the Rings</em>&nbsp;cùng lúc (thay vì
                  lần lượt từng tập như thói quen của Hollywood) đã giúp quá
                  trình sản xuất tiết kiệm được rất nhiều chi phí.
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    alt
                    src="https://s3img.vcdn.vn/123phim/2020/08/Peter Jackshons films-0f970b.jpg"
                    style={{ width: "90%" }}
                  />
                </p>
                <p>
                  Nhà làm phim đến từ New Zealand đã thay đổi cuộc chơi tại
                  Hollywood khi khám phá sức mạnh của kỹ xảo hiệu ứng kết hợp
                  chúng trong một kịch bản hành động hoành tráng để đem đến
                  những khung hình mang đậm tính sử thi.&nbsp;
                </p>
                <p>
                  <strong>4.&nbsp;</strong>
                  <strong>Michael Bay&nbsp;</strong>
                </p>
                <p style={{ textAlign: "center" }}>
                  <strong>
                    <img
                      alt
                      src="https://s3img.vcdn.vn/123phim/2020/08/Micheal Bay-09a18f.jpg"
                      style={{ width: "90%" }}
                    />
                  </strong>
                </p>
                <p>
                  “Ông hoàng cháy nổ” Michael Bay được biết tới qua thành công
                  đối với thể loại hành động, đua xe, đấu súng đậm chất
                  fanboy.&nbsp;
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    alt
                    src="https://s3img.vcdn.vn/123phim/2020/08/Micheal Bay - transformer-8f6848.jpg"
                    style={{ width: "90%" }}
                  />
                </p>
                <p>
                  Vị đạo diễn này đặc biệt ưa thích những phân cảnh cháy nổ
                  hoành tráng đã trở thành thương hiệu. Dù cho đó là các cảnh nổ
                  hóa chất (Bad Boys), nổ cơ khí (loạt Transformers) hay nổ hạt
                  nhân (Armageddon)..., phim cộp mác Michael Bay đều mang tính
                  giải trí rất “đã”. Bất chấp việc chưa được lòng giới phê bình,
                  tổng doanh thu từ các phim mà “chú Bảy” đạo diễn đã lên tới
                  hơn 6 tỉ USD cũng đủ để đưa cái tên này trở thành một trong
                  những đạo diễn nổi tiếng nhất.&nbsp;
                </p>
                <p>
                  <strong>5. James Cameron</strong>
                </p>
                <p style={{ textAlign: "center" }}>
                  <strong>
                    <img
                      alt
                      src="https://s3img.vcdn.vn/123phim/2020/08/james cameron-2c9b26.jpg"
                      style={{ width: "90%" }}
                    />
                  </strong>
                </p>
                <p>
                  Nhắc đến các đạo diễn thành công về mặt thương mại không thể
                  không nhắc tới James Cameron, nhà làm phim với những siêu phẩm
                  hái ra tiền. Sau những tác phẩm kinh điển như&nbsp;
                  <em>The Terminator</em>&nbsp;hay<em>&nbsp;Aliens</em>, những
                  tưởng một người khó thể nào vượt qua thành công do chính mình
                  đặt ra trong dòng phim khoa học viễn tưởng. Ấy thế mà năm
                  2009, James Cameron cho ra mắt&nbsp;<em>Avatar</em>&nbsp;như
                  một cú nổ lớn nhất trong lịch sử phòng vé, giúp ông trở thành
                  đạo diễn đầu tiên có hai bộ phim (<em>Avatar</em>
                  &nbsp;và&nbsp;<em>Titanic</em>) cán mốc 2 tỉ USD trong lịch
                  sử.&nbsp;
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    alt
                    src="https://s3img.vcdn.vn/123phim/2020/08/james cameron - avatar-f785b2.jpg"
                    style={{ width: "90%" }}
                  />
                </p>
                <p>
                  Ngoài thành công rực rỡ tại rạp chiếu, James Cameron cũng là
                  một trong những người tiên phong trong lĩnh vực kỹ thuật làm
                  phim khi chính ông là người mở ra kỷ nguyên cho dòng phim 3D.
                </p>
                <p>
                  <strong>6.&nbsp;</strong>
                  <strong>Christopher Nolan</strong>
                </p>
                <p style={{ textAlign: "center" }}>
                  <strong>
                    <img
                      alt
                      src="https://s3img.vcdn.vn/123phim/2020/08/Christopher Nolan - The Dark Knight-d391fd.jpg"
                      style={{ width: "90%" }}
                    />
                  </strong>
                </p>
                <p>
                  Được coi như một “kỳ lân” trong ngành đạo diễn, Christopher
                  Nolan được tán dương nhờ khả năng đạo diễn, biên kịch và sản
                  xuất ra những bộ phim đắt giá. Đó là thứ tài năng hiếm có tại
                  Hollywood, mà một người như Nolan khiến người xem phải trầm
                  trồ hết lần này đến lần khác mỗi dịp được thưởng thức phim của
                  ông. Các bộ phim của ông, hầu hết là những dự án với kịch bản
                  gốc đã đem về gần 5 tỉ đô doanh thu toàn cầu, giành được 10
                  chiến thắng và 34 đề cử Oscar.&nbsp;
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    src="https://s3img.vcdn.vn/123phim/2020/08/6-da-o-die-n-ti-do-lam-nen-thanh-cong-cua-nhu-ng-bom-ta-n-di-nh-da-m-nha-t-hollywood-15966020199934.jpg"
                    style={{ width: "90%" }}
                  />
                </p>
                <p>
                  Sau 2 năm “ngủ đông” kể từ siêu phẩm Dunkirk (2018), tháng 8
                  này, Christopher Nolan sẽ tái xuất với bom tấn hành động lấy
                  đề tài điệp viên mang tên&nbsp;
                  <strong>
                    <em>TENET</em>
                  </strong>
                  &nbsp;với sự tham gia của John David Washington, Robert
                  Pattinson, Elizabeth Debicki, Dimple Kapadia, Michael Caine và
                  Kenneth Branagh.&nbsp;&nbsp;
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    alt
                    src="https://s3img.vcdn.vn/123phim/2020/08/MV5BOGJmZjcxNTAtYjViZS00YzJmLTlkMzgtZmVkYTQ5YjUwMjIyXkEyXkFqcGdeQXVyNjMwMzc3MjE@-dedee6._V1_"
                    style={{ width: "90%" }}
                  />
                </p>
                <p>
                  Bộ phim theo chân nhân vật chính được một tổ chức gián điệp
                  chiêu mộ và dấn thân vào thế giới trắng đen lẫn lộn của mạng
                  lưới tình báo quốc tế. Nắm trong tay công nghệ hiện đại Tenet
                  - có khả năng thao túng và nghịch đảo thời gian, cả nhóm phải
                  chiến đấu vì sự tồn vong của nhân loại, ngăn chặn Thế chiến
                  thứ III từ trước khi nó xảy ra. Ghi hình tại 7 quốc gia khác
                  nhau, bao gồm Đan Mạch, Estonia, Ấn Độ, Ý, Nauy, Vương Quốc
                  Anh và Hoa Kỳ, kinh phí sản xuất lên tới hơn 200 triệu đô,
                  thậm chí Christopher Nolan còn dàn dựng một vụ va chạm bằng
                  máy bay thật, đâm vào tòa nhà thật để tạo ra một đại cảnh cháy
                  nổ hoành tráng,&nbsp;<strong>TENET&nbsp;</strong>chắc chắn sẽ
                  là một bộ phim đầy tham vọng và quy mô lớn mà vị đạo diễn kiệt
                  xuất này làm nên, một tác phẩm sẽ “
                  <em>
                    phá vỡ tất cả các quy tắc, đưa khán giả vào một chuyến phiêu
                    lưu ngoài mong đợi, chưa từng có trước đây
                  </em>
                  ”.&nbsp;
                </p>
                <p>
                  <strong>TENET</strong>&nbsp;được quay trên định dạng 70mm và
                  IMAX. &nbsp;Dự kiến khởi chiếu tại Việt Nam từ&nbsp;
                  <strong>28.08.2020</strong>
                </p>
                <p style={{ textAlign: "right" }}>
                  <strong>STEVEN</strong>
                </p>
              </div>
            </div>
            {/* ngIf: news.author_name !== '' */}
            <div className="row" id="scrollTo">
              <div className="col-sm-12 line">
                <hr />
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <div id="detailReviewer">
                  <div className="row isFlex detailMainStyle">
                    <div
                      className="col-sm-12 col-xs-12 dadInputReviewer dadInputReviewerNews newDesign"
                      ng-click="openCommentBox()"
                    >
                      {/* <span className="imgReviewer">
          
                        <img
                          ng-if="user.user_id"
                          ng-src="https://graph.facebook.com/123455529585217/picture?type=square"
                          className="ng-scope"
                          src="https://graph.facebook.com/123455529585217/picture?type=square"
                        />
                        
                      </span>
                      <input
                        className="inputReviwer"
                        type="text"
                        placeholder="Bạn nghĩ gì về bài viết này?"
                        readOnly="readonly"
                      /> */}
                      <div className="row">
                        <div className="col-12 mx-auto">
                          <CommentBoxNews props={this.props} />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* LIST COMMENTS */}
                  <div id="listComment">
                    {/* ngRepeat: comment in newsComments */}
                  </div>
                </div>
                {/* ngIf: (newsComments.length > 5) && (showCurrentComment < newsComments.length) */}
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 line">
                <hr />
              </div>
            </div>
            <div className="news2">
              <div className="news2__item">
                <div className="news2__img">
                  <img src="/images/news2.png" alt="imagesNews" />
                </div>
                <div className="news2__info">
                  <h1>
                    Tiệc Trăng Máu chính thức cán mốc 100 tỷ chỉ sau 2 tuần công
                    chiếu
                  </h1>
                  <p>
                    Sau 2 tuần ra mắt, Tiệc Trăng Máu chính thức gia nhập câu
                    lạc bộ phim điện ảnh đạt 100 tỷ đồng doanh thu phòng vé. Dàn
                    ngôi sao “bạc tỷ” của phim cũng lần đầu tiên hội tụ đầy đủ
                    trong một khung hình để ăn mừng thành tích ấn tượng của tác
                    phẩm.
                  </p>
                  <div className="news2__icon">
                    <i className="fa fa-thumbs-up" />
                    <span>2</span>
                    <i className="fa fa-comment-alt" />
                    <span>0</span>
                  </div>
                </div>
              </div>
              <div className="news2__item">
                <div className="news2__img">
                  <img src="/images/mortal-kombat-1.png" alt="imagesNews" />
                </div>
                <div className="news2__info">
                  <h1>
                    [MORTAL KOMBAT: CUỘC CHIẾN SINH TỬ] - GỌI TÊN NHỮNG PHIM
                    ĐIỆN ẢNH NỔI TIẾNG ĐƯỢC CHUYỂN THỂ TỪ CÁC TỰA GAME ĐÌNH ĐÁM
                  </h1>
                  <p>
                    Bên cạnh những kịch bản gốc mới mẻ và đầy bất ngờ, Hollywood
                    cũng không thiếu những tác phẩm đình đám được chuyển thể từ
                    tiểu thuyết, phim hoạt hình, hay thậm chí là cả trò chơi
                    điện tử. Với ý tưởng thú vị cùng cốt truyện mang đậm tính
                    phiêu lưu, các tựa game đã trở thành nguồn cảm hứng độc đáo
                    cho các nhà làm phim. Không chỉ vậy, kể cả với các fan của
                    tựa game gốc, việc được thấy các nhân vật mình yêu thích
                    trên màn ảnh rộng cũng là một trải nghiệm không thể bỏ qua.
                    Hãy cùng điểm lại những bộ phim đình đám bậc nhất của
                    Hollywood được chuyển thể từ các trò chơi điện tử nhé.
                  </p>
                  <div className="news2__icon">
                    <i className="fa fa-thumbs-up" />
                    <span>1</span>
                    <i className="fa fa-comment-alt" />
                    <span>1</span>
                  </div>
                </div>
              </div>
              <div className="news2__item news2__item2">
                <div className="item2__row">
                  <div className="item2__info">
                    <div className="item2__img">
                      <img src="/images/news5.png" alt="imagesNews" />
                    </div>
                    <div className="item2__detail">
                      <p>
                        Dàn mỹ nhân trong thế giới điện ảnh của quái kiệt
                        Christopher Nolan
                      </p>
                    </div>
                  </div>
                </div>
                <div className="item2__row">
                  <div className="item2__info">
                    <div className="item2__img">
                      <img src="/images/news6.png" alt="imagesNews" />
                    </div>
                    <div className="item2__detail">
                      <p>
                        Truy Cùng Giết Tận - Cuộc tái ngộ của hai 'ông hoàng
                        phòng vé' xứ Hàn
                      </p>
                    </div>
                  </div>
                </div>
                <div className="item2__row">
                  <div className="item2__info">
                    <div className="item2__img">
                      <img src="/images/fast9-1.jpg" alt="poster" />
                    </div>
                    <div className="item2__detail">
                      <p>
                        Lộ diện phản diện mới của series Fast and Furious,
                        'người đặc biệt' đã trở lại!
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="news__more">
              <button>XEM THÊM</button>
            </div>
            {/* ngIf: list_related.length > 4 */}
            {/* <div
              className="row isFlex text-center ViewMoreNews ng-scope"
              ng-if="list_related.length > 4"
            >
              <button
                id="btnNewsRelated"
                className="btnViewMore"
                ng-click="viewMoreNews()"
              >
                XEM THÊM
              </button>
            </div> */}
            {/* end ngIf: list_related.length > 4 */}
          </div>
        </section>
        <Footer />
      </>
    );
  }
}
