import React, { Component } from "react";
import Footer from "./../../Footer/Footer";
import CommentBoxNews from "./CommentBBox";
import { scroller } from "react-scroll";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import ChatBubbleIcon from "@material-ui/icons/ChatBubble";
import db from "./../../../../homePages/LoginHome/firebase.js";
import "./../../../../assets/sass/Home/news/index.scss";
import ShareIcon from "@material-ui/icons/Share";
let taiKhoan =
  (localStorage.getItem("userGoo") &&
    JSON.parse(localStorage.getItem("userGoo")).uid) ||
  (localStorage.getItem("userFace") &&
    JSON.parse(localStorage.getItem("userFace")).uid) ||
  (localStorage.getItem("userHome") &&
    JSON.parse(localStorage.getItem("userHome")).taiKhoan);
export default class Fast9 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      soLikeArr: [],
      soLike: 0,
      nguoiLikeFast: [],
      nguoiLikeLoki: [],
      nguoiLikeTiec: [],
      nguoiLikeKom: [],
    };
    this.newsMoreButton = React.createRef();
  }
  componentDidMount() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    db.collection("news").onSnapshot((snapshot) => {
      let arr = snapshot.docs.map((doc) => ({ id: doc.id, data: doc.data() }));
      let soLikeArr;
      db.collection("soLikeBV").onSnapshot((snap) => {
        soLikeArr = snap.docs.map((doc) => ({ id: doc.id, data: doc.data() }));
        let nguoiLikeFast;

        soLikeArr.forEach((item) => {
          if (item.data.id === "/news/fast9") {
            nguoiLikeFast = item.data.nguoiLike;
          }
        });

        this.setState({
          posts: arr,
          soLikeArr,
          nguoiLikeFast,
        });
      });
    });
  }
  likeIconBV = (id) => {
    if (
      localStorage.getItem("userHome") ||
      localStorage.getItem("userFace") ||
      localStorage.getItem("userGoo")
    ) {
      this.state.soLikeArr.forEach((item) => {
        if (
          item.data.id == id &&
          item.data.nguoiLike &&
          item.data.nguoiLike.includes(taiKhoan)
        ) {
          let soLike = item.data.soLike - 1;
          item.data.nguoiLike.splice(item.data.nguoiLike.indexOf(taiKhoan, 1));
          console.log(item.data.nguoiLike, "nguoi like");
          db.collection("soLikeBV").doc(item.id).update({
            nguoiLike: item.data.nguoiLike,
            soLike,
          });
        } else if (
          item.data.id == id &&
          !(item.data.nguoiLike && item.data.nguoiLike.includes(taiKhoan))
        ) {
          item.data.nguoiLike.push(taiKhoan);
          console.log(item.data.nguoiLike, "nguoi like");
          let soLike = item.data.soLike + 1;

          db.collection("soLikeBV").doc(item.id).update({
            nguoiLike: item.data.nguoiLike,
            soLike: soLike,
          });
        }
      });
    } else {
      console.log("props like", this.props);
      this.props.history.push("/login");
    }
  };
  renderSoLike = (id) => {
    let soLike = 0;
    this.state.soLikeArr.forEach((item) => {
      if (item.data.id === id) {
        soLike = item.data.soLike;
      }
    });
    return soLike;
  };
  renderCM = (id) => {
    let soCM = 0;
    this.state.posts.map((item) => {
      if (item.data.id === id) {
        soCM += 1;
      }
    });
    return soCM;
  };
  handleSrollTo = (name, to) => {
    scroller.scrollTo(name, {
      duration: 1000,
      smooth: true,
    });
  };

  render() {
    return (
      <>
        <section className="list-news">
          <div className="container ng-scope" id="detailNews">
            <div className="row">
              <div className="col-sm-12 title">
                <p className="ng-binding">
                  Lộ diện phản diện mới của series Fast and Furious, 'người đặc
                  biệt' đã trở lại!
                </p>
                {/* ngIf: !isMobile */}
                <div className="author ng-binding ng-scope" ng-if="!isMobile">
                  STEVEN <span className="ng-binding" /> 21.12.21
                </div>
                {/* end ngIf: !isMobile */}
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 count">
                <div
                  className="wrapIcon like wrapIconLike news2__icon mx-0"
                  data-type={1}
                  data-id={7848}
                  ng-click="likeAction($event, news, keyCache)"
                >
                  {/* <img
                    className="iconFacebook postLike"
                    ng-src="app/assets/img/icons/like.png"
                    src="/images/like.png"
                  />
                  <span className="amount like ng-binding">0 LIKES</span>
                </div>
                <div className="wrapIcon wrapIconComment" ng-click="cmt()">
                  <img
                    className="iconFacebook"
                    src="/images/comment.png"
                    ng-click="cmt()"
                  />
                  <span className="amount ng-binding">0 BÌNH LUẬN</span> */}
                  {(localStorage.getItem("userHome") ||
                    localStorage.getItem("userFace") ||
                    localStorage.getItem("userGoo")) &&
                  this.state.nguoiLikeFast &&
                  this.state.nguoiLikeFast.includes(taiKhoan) ? (
                    <button
                      className="text-primary mr-4"
                      onClick={() => this.likeIconBV("/news/fast9")}
                    >
                      <ThumbUpIcon />
                    </button>
                  ) : (
                    <button
                      className="mr-4"
                      onClick={() => this.likeIconBV("/news/fast9")}
                    >
                      <ThumbUpIcon />
                    </button>
                  )}
                  <span className="mr-5">
                    {this.renderSoLike("/news/fast9")} LIKE
                  </span>
                  <button
                    onClick={() =>
                      this.handleSrollTo("scrollTo", "/news/fast9")
                    }
                    className="mr-4"
                  >
                    <ChatBubbleIcon />
                  </button>
                  <span className="mr-5">
                    {this.renderCM("/news/fast9")} BÌNH LUẬN
                  </span>
                  <ShareIcon className="shareIcon mr-4" />
                  <span className="amount share ng-binding">0 CHIA SẺ</span>
                </div>
                <div className="wrapIcon wrapIconShare" ng-click="shareNews()">
                  {/* <img
                    className="iconFacebook"
                    src="/images/sharing-icon.png"
                  /> */}
                  {/* <span className="amount share ng-binding">0 CHIA SẺ</span> */}
                </div>
              </div>
            </div>
            <div className="row">
              <div
                className="col-sm-12 content ng-binding"
                ng-bind-html="newsContent"
              >
                <p>
                  Sau phần ngoại truyện&nbsp;
                  <em>Fast &amp; Furious: Hobbs &amp; Shaw</em>&nbsp;cực kì mãn
                  nhãn, điều mà fan hâm mộ của thương hiệu&nbsp;tốc độ mong
                  ngóng nhất chính là sự ra mắt của phần phim thứ 9. Mới đây,
                  bom tấn "hàng khủng"&nbsp;
                  <a href="https://tix.vn/phim/2191-fast-furious-9">
                    <strong>Fast &amp; Furious 9</strong>
                  </a>
                  &nbsp;đã bất ngờ trình làng trailer đầy kịch tính và căng
                  não&nbsp;tột độ với hàng loạt bất ngờ này đến cú sốc khác.
                </p>
                <p style={{ textAlign: "center" }} className="isVideo">
                  <iframe
                    allowFullScreen
                    frameBorder={0}
                    height={420}
                    src="https://www.youtube.com/embed/SwwlFvOwkhA"
                    width={700}
                  />
                </p>
                <p>
                  Mở đầu trailer là khung cảnh gia đình bình yên của Dom. Ngay
                  từ những nốt nhạc đầu tiên của ca khúc&nbsp;See You
                  Again&nbsp;vang lên đã làm khán giả của{" "}
                  <em>Fast &amp; Furious</em> phấn khích vô cùng. Bên Letty và
                  cậu con trai nhỏ Brian, Dom (Vin Diesel) đã có một mái nhà
                  hạnh phúc, trọn vẹn sau bao sóng gió.
                </p>
                <p>
                  Thế nhưng những nguy hiểm vẫn luôn dình dập xung quanh anh,
                  khán giả chứng kiến Letty trao cho Brian chiếc thánh giá để
                  bảo vệ cho cậu bé. <em>"Bố sẽ luôn trong tim con."</em>
                  &nbsp;lời nhắn nhủ của Dom báo hiệu một cuộc chiến mới mà anh
                  phải đương đầu để giữ an toàn cho gia đình thân yêu. Cuộc
                  chiến này chắc chắn sẽ mạo hiểm hơn bao giờ hết, vì giờ đây
                  Dom đang có quá nhiều điều quý giá không thể đánh mất.
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    src="https://s3img.vcdn.vn/123phim/2020/02/9f4aeb23bd3a1829c0b7b0c70b69e382.jpg"
                    style={{ width: "80%" }}
                  />
                </p>
                <p style={{ textAlign: "center" }}>
                  <em>
                    John Cena thủ vai phản diện chính -&nbsp;em trai ruột của
                    Dom.
                  </em>
                </p>
                <p>
                  Và bất ngờ đầu tiên đã được trailer hé lộ. Kẻ phản diện lần
                  này mà Dom và những người bạn phải đối đầu là một siêu trộm,
                  một sát thủ siêu đẳng và một tay đua trình cao. Nhưng nếu chỉ
                  có vậy, Jacob chưa hẳn là đối thủ ngang tầm với Dom và những
                  người bạn. Phản diện chính của&nbsp;
                  <a href="https://tix.vn/phim/2191-fast-furious-9">
                    <strong>Fast &amp; Furious 9</strong>
                  </a>
                  &nbsp;do cựu đô vật John Cena thủ vai không ai khác chính là
                  em trai ruột của Dom - người đã dành cả cuộc đời để tìm cách
                  “triệt hạ” và vượt mặt Dom. Không chỉ dừng lại ở đó, hắn còn
                  “hợp tác” với Cipher - kẻ thù cũ từ phần&nbsp;
                  <em>The Fate of the Furious</em>. Cú bắt tay tử thần của hai
                  ác nhân quá am hiểu Dom sẽ đẩy gia đình của anh vào hiểm nguy
                  khó lường.
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    src="https://s3img.vcdn.vn/123phim/2020/02/312d94469a7c1e94e5cf6c687caa1f39.jpg"
                    style={{ width: "80%" }}
                  />
                </p>
                <p style={{ textAlign: "center" }}>
                  <em>Cipher (Charlize Theron) sẽ&nbsp;quay trở lại.</em>
                </p>
                <p>
                  Để chống lại thế lực siêu cường này, Dom không thể thiếu đi
                  những người trợ thủ mà anh coi như người thân. Các fan sẽ được
                  gặp lại dàn nhân vật hết sức quen thuộc của series như Letty,
                  Tej, Roman Pearce và Sean Boswell - nam chính của phần ngoại
                  truyện&nbsp;<em>Tokyo Drift</em>.
                </p>
                <p>
                  Sự xuất hiện của Sean đã được nhiều fan đồn đoán từ khá lâu.
                  Màn tái xuất của Sean trong&nbsp;
                  <a href="https://tix.vn/phim/2191-fast-furious-9">
                    <strong>Fast &amp; Furious 9</strong>
                  </a>
                  &nbsp;đồng thời xác nhận cho sự&nbsp;trở lại được mong đợi
                  nhất và cũng gây sốc nhất loạt phim&nbsp;
                  <em>Fast &amp; Furious</em>. Han - thành viên của gia đình{" "}
                  <em>Fast &amp; Furious</em> được cho là tử nạn trong vụ nổ ở
                  phần&nbsp;<em>Tokyo Drift</em>
                  &nbsp;cũng được "hồi sinh" trong phần phim thứ 9 này. Vậy là
                  sau câu nói úp mở của Shaw trong&nbsp;
                  <em>Fast &amp; Furious: Hobbs &amp; Shaw</em>, anh chàng được
                  yêu thích nhất nhì gia đình <em>Fast &amp; Furious</em> đã
                  quay về "bằng xương bằng thịt", làm thỏa lòng người hâm mộ đợi
                  chờ suốt 14 năm qua.
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    src="https://s3img.vcdn.vn/123phim/2020/02/395bacc7d41f414caf77a6aa86a3b1fb.jpg"
                    style={{ width: "80%" }}
                  />
                </p>
                <p>
                  Chừng đó bất ngờ đã là quá đủ để khán giả háo hức nóng lòng
                  tới ngày khởi chiếu vào mùa hè năm nay. Nhưng bên cạnh câu
                  chuyện hấp dẫn,&nbsp;
                  <a href="https://tix.vn/phim/2191-fast-furious-9">
                    <strong>Fast &amp; Furious 9</strong>
                  </a>
                  &nbsp;còn chiêu đãi người xem những pha hành động hoành tráng
                  gấp nhiều lần phần trước. Vẫn là mẫu xe cơ bắp cổ điển đặc
                  trưng nhưng chiếc Pontiac Fiero do Dom cầm lái được gắn động
                  cơ tên lửa với sức mạnh đủ để anh "bay tận cung trăng". Ngoài
                  ra, khán giả còn phải à ố đầy bất ngờ bởi hàng loạt pha lao xe
                  qua vách núi, dàn xe container lật ngược hay đối đầu với đội
                  quân trực thăng, hay những khúc đua xe, nẹp bô “căng đét”.
                </p>
                <p>
                  Bộ phim tiếp tục được đạo diễn bởi Justin Lin -&nbsp;người đã
                  đem lại thành công vang dội cho&nbsp;
                  <em>Fast &amp; Furious</em> (2009), <em>Fast Five</em>{" "}
                  (2011)&nbsp;và&nbsp;
                  <em>Fast &amp; Furious 6</em> (2013), cùng sự góp mặt của dàn
                  diễn viên quen thuộc:&nbsp;Vin Diesel,&nbsp;Charlize Theron,
                  Michelle Rodriguez, Jordana Brewster, Tyrese Gibson, Ludacris,
                  Helen Mirren, Sung Kang và gương mặt “nặng ký” mới - John
                  Cena.
                </p>
                <p style={{ textAlign: "center" }}>
                  <img src="/images/fast9-5.jpg" style={{ width: "80%" }} />
                </p>
                <p>
                  Các nhà sản xuất của&nbsp;<em>Fast &amp; Furious</em>
                  &nbsp;chưa bao giờ khiến khán giả phải thất vọng trước độ mãn
                  nhãn được nâng tầm lên một trình độ hoàn toàn mới qua từng
                  phần.&nbsp;
                  <a href="https://tix.vn/phim/2191-fast-furious-9">
                    <strong>Fast &amp; Furious 9</strong>
                  </a>
                  &nbsp;hứa hẹn là bom tấn&nbsp;phá đảo&nbsp;vô số kỷ lục trong
                  năm 2020 này.
                </p>
                <p style={{ textAlign: "right" }}>
                  <strong>STEVEN</strong>
                </p>
              </div>
            </div>
            {/* ngIf: news.author_name !== '' */}
            <div className="row" id="scrollTo">
              <div className="col-sm-12 line">
                <hr />
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <div id="detailReviewer">
                  <div className="row isFlex detailMainStyle">
                    <div
                      className="col-sm-12 col-xs-12 dadInputReviewer dadInputReviewerNews newDesign"
                      ng-click="openCommentBox()"
                    >
                      {/* <span className="imgReviewer">
     
                        <img
                          ng-if="user.user_id"
                          ng-src="https://graph.facebook.com/123455529585217/picture?type=square"
                          className="ng-scope"
                          src="https://graph.facebook.com/123455529585217/picture?type=square"
                        />
                     
                      </span>
                      <input
                        className="inputReviwer"
                        type="text"
                        placeholder="Bạn nghĩ gì về bài viết này?"
                        readOnly="readonly"
                      /> */}
                      <div className="row">
                        <div className="col-12 mx-auto">
                          <CommentBoxNews props={this.props} />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* LIST COMMENTS */}
                  <div id="listComment">
                    {/* ngRepeat: comment in newsComments */}
                  </div>
                </div>
                {/* ngIf: (newsComments.length > 5) && (showCurrentComment < newsComments.length) */}
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 line">
                <hr />
              </div>
            </div>
            <div className="news2">
              <div className="news2__item">
                <div className="news2__img">
                  <img src="/images/news2.png" alt="imagesNews" />
                </div>
                <div className="news2__info">
                  <h1>
                    Tiệc Trăng Máu chính thức cán mốc 100 tỷ chỉ sau 2 tuần công
                    chiếu
                  </h1>
                  <p>
                    Sau 2 tuần ra mắt, Tiệc Trăng Máu chính thức gia nhập câu
                    lạc bộ phim điện ảnh đạt 100 tỷ đồng doanh thu phòng vé. Dàn
                    ngôi sao “bạc tỷ” của phim cũng lần đầu tiên hội tụ đầy đủ
                    trong một khung hình để ăn mừng thành tích ấn tượng của tác
                    phẩm.
                  </p>
                  <div className="news2__icon">
                    <i className="fa fa-thumbs-up" />
                    <span>2</span>
                    <i className="fa fa-comment-alt" />
                    <span>0</span>
                  </div>
                </div>
              </div>
              <div className="news2__item">
                <div className="news2__img">
                  <img src="/images/mortal-kombat-1.png" alt="imagesNews" />
                </div>
                <div className="news2__info">
                  <h1>
                    [MORTAL KOMBAT: CUỘC CHIẾN SINH TỬ] - GỌI TÊN NHỮNG PHIM
                    ĐIỆN ẢNH NỔI TIẾNG ĐƯỢC CHUYỂN THỂ TỪ CÁC TỰA GAME ĐÌNH ĐÁM
                  </h1>
                  <p>
                    Bên cạnh những kịch bản gốc mới mẻ và đầy bất ngờ, Hollywood
                    cũng không thiếu những tác phẩm đình đám được chuyển thể từ
                    tiểu thuyết, phim hoạt hình, hay thậm chí là cả trò chơi
                    điện tử. Với ý tưởng thú vị cùng cốt truyện mang đậm tính
                    phiêu lưu, các tựa game đã trở thành nguồn cảm hứng độc đáo
                    cho các nhà làm phim. Không chỉ vậy, kể cả với các fan của
                    tựa game gốc, việc được thấy các nhân vật mình yêu thích
                    trên màn ảnh rộng cũng là một trải nghiệm không thể bỏ qua.
                    Hãy cùng điểm lại những bộ phim đình đám bậc nhất của
                    Hollywood được chuyển thể từ các trò chơi điện tử nhé.
                  </p>
                  <div className="news2__icon">
                    <i className="fa fa-thumbs-up" />
                    <span>1</span>
                    <i className="fa fa-comment-alt" />
                    <span>1</span>
                  </div>
                </div>
              </div>
              <div className="news2__item news2__item2">
                <div className="item2__row">
                  <div className="item2__info">
                    <div className="item2__img">
                      <img src="/images/news5.png" alt="imagesNews" />
                    </div>
                    <div className="item2__detail">
                      <p>
                        Dàn mỹ nhân trong thế giới điện ảnh của quái kiệt
                        Christopher Nolan
                      </p>
                    </div>
                  </div>
                </div>
                <div className="item2__row">
                  <div className="item2__info">
                    <div className="item2__img">
                      <img src="/images/news6.png" alt="imagesNews" />
                    </div>
                    <div className="item2__detail">
                      <p>
                        Truy Cùng Giết Tận - Cuộc tái ngộ của hai 'ông hoàng
                        phòng vé' xứ Hàn
                      </p>
                    </div>
                  </div>
                </div>
                <div className="item2__row">
                  <div className="item2__info">
                    <div className="item2__img">
                      <img src="/images/news7.png" alt="imagesNews" />
                    </div>
                    <div className="item2__detail">
                      <p>
                        6 đạo diễn tỉ đô làm nên thành công của những bom tấn
                        đình đám nhất Hollywood
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="news__more">
              <button>XEM THÊM</button>
            </div>
            {/* ngIf: list_related.length > 4 */}
            {/* <div
            className="row isFlex text-center ViewMoreNews ng-scope"
            ng-if="list_related.length > 4"
          >
            <button
              id="btnNewsRelated"
              className="btnViewMore"
              ng-click="viewMoreNews()"
            >
              XEM THÊM
            </button>
          </div> */}
            {/* end ngIf: list_related.length > 4 */}
          </div>
        </section>
        <Footer />
      </>
    );
  }
}
