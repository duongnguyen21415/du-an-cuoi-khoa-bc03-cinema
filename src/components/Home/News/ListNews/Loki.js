import React, { Component } from "react";
import Footer from "./../../Footer/Footer";
import CommentBoxNews from "./CommentBBox";
import { scroller } from "react-scroll";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import ChatBubbleIcon from "@material-ui/icons/ChatBubble";
import db from "./../../../../homePages/LoginHome/firebase.js";
import "./../../../../assets/sass/Home/news/index.scss";
import ShareIcon from "@material-ui/icons/Share";
let taiKhoan =
  (localStorage.getItem("userGoo") &&
    JSON.parse(localStorage.getItem("userGoo")).uid) ||
  (localStorage.getItem("userFace") &&
    JSON.parse(localStorage.getItem("userFace")).uid) ||
  (localStorage.getItem("userHome") &&
    JSON.parse(localStorage.getItem("userHome")).taiKhoan);
export default class Loki extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      soLikeArr: [],
      soLike: 0,
      nguoiLikeFast: [],
      nguoiLikeLoki: [],
      nguoiLikeTiec: [],
      nguoiLikeKom: [],
    };
    this.newsMoreButton = React.createRef();
  }
  componentDidMount() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    db.collection("news").onSnapshot((snapshot) => {
      let arr = snapshot.docs.map((doc) => ({ id: doc.id, data: doc.data() }));
      let soLikeArr;
      db.collection("soLikeBV").onSnapshot((snap) => {
        soLikeArr = snap.docs.map((doc) => ({ id: doc.id, data: doc.data() }));
        let nguoiLikeFast;

        soLikeArr.forEach((item) => {
          if (item.data.id === "/news/loki") {
            nguoiLikeFast = item.data.nguoiLike;
          }
        });

        this.setState({
          posts: arr,
          soLikeArr,
          nguoiLikeFast,
        });
      });
    });
  }
  likeIconBV = (id) => {
    if (
      localStorage.getItem("userHome") ||
      localStorage.getItem("userFace") ||
      localStorage.getItem("userGoo")
    ) {
      this.state.soLikeArr.forEach((item) => {
        if (
          item.data.id == id &&
          item.data.nguoiLike &&
          item.data.nguoiLike.includes(taiKhoan)
        ) {
          let soLike = item.data.soLike - 1;
          item.data.nguoiLike.splice(item.data.nguoiLike.indexOf(taiKhoan, 1));
          console.log(item.data.nguoiLike, "nguoi like");
          db.collection("soLikeBV").doc(item.id).update({
            nguoiLike: item.data.nguoiLike,
            soLike,
          });
        } else if (
          item.data.id == id &&
          !(item.data.nguoiLike && item.data.nguoiLike.includes(taiKhoan))
        ) {
          item.data.nguoiLike.push(taiKhoan);
          console.log(item.data.nguoiLike, "nguoi like");
          let soLike = item.data.soLike + 1;

          db.collection("soLikeBV").doc(item.id).update({
            nguoiLike: item.data.nguoiLike,
            soLike: soLike,
          });
        }
      });
    } else {
      console.log("props like", this.props);
      this.props.history.push("/login");
    }
  };
  renderSoLike = (id) => {
    let soLike = 0;
    this.state.soLikeArr.forEach((item) => {
      if (item.data.id === id) {
        soLike = item.data.soLike;
      }
    });
    return soLike;
  };
  renderCM = (id) => {
    let soCM = 0;
    this.state.posts.map((item) => {
      if (item.data.id === id) {
        soCM += 1;
      }
    });
    return soCM;
  };
  handleSrollTo = (name, to) => {
    scroller.scrollTo(name, {
      duration: 1000,
      smooth: true,
    });
  };
  render() {
    return (
      <>
        <section className="list-news">
          <div className="container ng-scope" id="detailNews">
            <div className="row">
              <div className="col-sm-12 title">
                <p className="ng-binding">
                  Bí mật đằng sau phiên bản nữ của Loki
                </p>
                {/* ngIf: !isMobile */}
                <div className="author ng-binding ng-scope" ng-if="!isMobile">
                  STEVEN <span className="ng-binding" /> 21.12.21
                </div>
                {/* end ngIf: !isMobile */}
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 count">
                <div
                  className="wrapIcon like wrapIconLike news2__icon mx-0"
                  data-type={1}
                  data-id={7848}
                  ng-click="likeAction($event, news, keyCache)"
                >
                  {(localStorage.getItem("userHome") ||
                    localStorage.getItem("userFace") ||
                    localStorage.getItem("userGoo")) &&
                  this.state.nguoiLikeFast &&
                  this.state.nguoiLikeFast.includes(taiKhoan) ? (
                    <button
                      className="text-primary mr-4"
                      onClick={() => this.likeIconBV("/news/loki")}
                    >
                      <ThumbUpIcon />
                    </button>
                  ) : (
                    <button
                      className="mr-4"
                      onClick={() => this.likeIconBV("/news/loki")}
                    >
                      <ThumbUpIcon />
                    </button>
                  )}
                  <span className="mr-5">
                    {this.renderSoLike("/news/loki")} LIKE
                  </span>
                  <button
                    onClick={() => this.handleSrollTo("scrollTo", "/news/loki")}
                    className="mr-4"
                  >
                    <ChatBubbleIcon />
                  </button>
                  <span className="mr-5">
                    {this.renderCM("/news/loki")} BÌNH LUẬN
                  </span>
                  <ShareIcon className="shareIcon mr-4" />
                  <span className="amount share ng-binding">0 CHIA SẺ</span>
                </div>
                <div
                  className="wrapIcon wrapIconShare"
                  ng-click="shareNews()"
                ></div>
              </div>
            </div>
            <div className="row">
              <div
                className="col-sm-12 content ng-binding"
                ng-bind-html="newsContent"
              >
                <p>
                  Mini-series&nbsp;
                  <a href="https://tix.vn/phim/2191-fast-furious-9">
                    <strong>"Loki"</strong>
                  </a>
                  &nbsp;đã hé lộ danh tính kẻ khủng bố dòng thời gian mà TVA
                  đang truy đuổi. Xung quanh thân thế của nhân vật là nhiều bí
                  mật chưa được giải đáp.
                  <hr />
                  <p>
                    Ở đoạn cuối &nbsp;
                    <a href="https://tix.vn/phim/2191-fast-furious-9">
                      <strong>“Loki 2”</strong>
                    </a>
                    &nbsp; , tức 1/3 chặng đường của tác phẩm, khán giả đã được
                    thấy cận mặt “Loki phản diện” từng xuất hiện trong tập mở
                    màn. Đây là một phụ nữ có tên Lady Loki (Sophia Di
                    Martinos). Lady Loki đang âm thầm tiến hành kế hoạch phá
                    hoại dòng thời gian mà TVA dốc công bảo vệ. Lady Loki cũng
                    âm thầm lên sẵn những kế hoạch riêng cho Loki - khi anh đang
                    buộc phải làm quen với công việc cổ cồn, bàn giấy.
                  </p>
                </p>
                <p style={{ textAlign: "center" }} className="isVideo">
                  <iframe
                    allowFullScreen
                    frameBorder={0}
                    height={420}
                    width={700}
                    src="https://www.youtube.com/embed/nW948Va-l10"
                  ></iframe>
                </p>
                <h1>Lady Loki là ai?</h1>
                <p>
                  PHIM ẢNH PHIM TRUYỀN HÌNH Bí mật đằng sau phiên bản nữ của
                  Loki Anh Phan Thứ năm, 17/6/2021 15:55 (GMT+7)Mini-series
                  &nbsp;
                  <a href="https://tix.vn/phim/2191-fast-furious-9">
                    <strong>"Loki"</strong>
                  </a>
                  &nbsp; đã hé lộ danh tính kẻ khủng bố dòng thời gian mà TVA
                  đang truy đuổi. Xung quanh thân thế của nhân vật là nhiều bí
                  mật chưa được giải đáp. Ở đoạn cuối Loki 2, tức 1/3 chặng
                  đường của tác phẩm, khán giả đã được thấy cận mặt “Loki phản
                  diện” từng xuất hiện trong tập mở màn. Đây là một phụ nữ có
                  tên Lady Loki (Sophia Di Martinos). Lady Loki đang âm thầm
                  tiến hành kế hoạch phá hoại dòng thời gian mà TVA dốc công bảo
                  vệ. Lady Loki cũng âm thầm lên sẵn những kế hoạch riêng cho
                  Loki - khi anh đang buộc phải làm quen với công việc cổ cồn,
                  bàn giấy. Lady Loki là ai? Trong truyện tranh, Lady Loki được
                  tạo ra sau Ragnarok - sự kiện đánh dấu ngày tàn của Asgard.
                  Sau khi quê nhà bị hủy diệt, linh hồn người dân Asgard đã chu
                  du tới Trái Đất, tìm kiếm những vật chủ mới. Tại đây, hồn của
                  Loki nhập vào thể xác của một phụ nữ. Vừa ra đời, Lady Loki đã
                  lộ bản chất phản diện khi bắt tay với Doom - kẻ thù truyền
                  kiếp của Fantastic Four.
                </p>
                <p style={{ textAlign: "center" }}>
                  <img src="/images/loki-2.jpg" style={{ width: "80%" }} />
                </p>
                <p style={{ textAlign: "center" }}>
                  <em>
                    Lady Loki là bất ngờ lớn nhất của tập 2 mini-series Loki.
                  </em>
                </p>
                <p>
                  Năm 2014, hình tượng Loki trong truyện tranh (và trên màn ảnh)
                  có những thay đổi lớn về bản chất. Loki đã không còn bị bó hẹp
                  trong định nghĩa “gã Thần Lừa Lọc". Như Odin từng tuyên bố
                  hùng hồn: “Loki là con trai, con gái và cũng là con ta”.
                </p>
                <p>
                  Từ đây, một phiên bản Lady Loki khác chính thức ra đời. Lady
                  Loki không còn sống kiếp “hồn Trương Ba da hàng thịt” như
                  người tiền nhiệm, nhưng cũng chưa đạt tới mức tồn tại độc lập
                  khỏi Loki gốc. Nói cách khác, Lady Loki về bản chất vẫn chỉ là
                  tâm hồn và xác thịt của Loki, với “một chút” thay đổi về "giao
                  diện".
                </p>
                <p>
                  Vũ trụ Điện ảnh Marvel đã cài cắm manh mối về sự toan tính của
                  Loki từ khá sớm. Từ đó đến nay, kịch bản đã âm thầm phục vụ
                  cho sự xuất hiện của Lady Loki. Trong dòng thời gian bị xáo
                  trộn, biến thể của Loki đã chọn một gương mặt khác, một giới
                  tính khác để xuất hiện trước mặt bản gốc của mình.
                </p>
                <h1>Lady Loki của MCU mạnh đến đâu?</h1>
                <p>
                  Trong truyện tranh, Lady Loki và Loki có sức mạnh, điểm yếu và
                  tư tưởng giống hệt nhau. Tuy nhiên, các biên kịch MCU đã thay
                  đổi chi tiết này khi đưa Lady Loki từ trang sách lên màn ảnh.
                  Theo đó, mỗi biến thể được sinh ra trong dòng thời gian bị xáo
                  trộn sẽ có bản năng, sức mạnh lẫn diện mạo khác hẳn nguyên
                  mẫu.
                </p>
                <p>
                  Điều này dẫn đến kết quả là sự xuất hiện của một Lady Loki
                  vượt trội về nhiều mặt. Một mặt, ả nắm rõ những ngón “tủ” của
                  Loki như kỹ năng phóng dao, cận chiến điêu luyện, tạo ra ảo
                  ảnh, tự nhân bản mình. Nhưng mặt khác, Lady Loki cũng đã cho
                  thấy sự ưu việt của mình so với bản gốc.
                </p>
                <p>
                  Loki kiểm soát tâm trí nạn nhân bằng cách cài cắm vào đó những
                  câu từ mang tính thao túng, khiến họ vô thức hành động theo
                  mục đích và kế hoạch của mình. Năng lực tẩy não của Lokie vận
                  hành theo cơ chế 1-1, tức một hung thủ và một nạn nhân.
                </p>
                <p style={{ textAlign: "center" }}>
                  <img src="/images/loki-3.png" style={{ width: "80%" }} />
                </p>
                <p style={{ textAlign: "center" }}>
                  <em>
                    Lady Loki trong truyện tranh là phiên bản ngang tài ngang
                    sức với bản gốc Loki.
                  </em>
                </p>
                <p>
                  Nhưng với Lady Loki, ác nữ không chỉ thao túng mà còn tiến xa
                  tới chỗ hoàn toàn kiểm soát tâm trí nạn nhân. Năng lực này lây
                  lan như một thứ dịch bệnh, có thể truyền thẳng từ nạn nhân này
                  sang nạn nhân khác mà không cần tới sự hiện diện của Lady
                  Loki. Điều này chứng minh sức mạnh của ác nữ không chỉ ấn
                  tượng hơn, mà còn trên cơ Loki vài phần.
                </p>
                <h1>Lady Loki âm mưu điều gì?</h1>
                <p>
                  Cuối tập 2, Lady Loki đã hé lộ bước đầu tiên trong kế hoạch
                  phạm tội. Lady Loki thừa nhận mình liên tục phá phách trong
                  dòng thời gian nhằm lôi kéo sự chú ý của các thợ săn TVA. Tuy
                  nhiên, ác nữ không hẳn nhắm đến mục tiêu làm suy yếu lực lượng
                  TVA mà còn là thu thập thiết bị của họ - loại bom có khả năng
                  “nắn” lại dòng thời gian đang đi chệch hướng.
                </p>
                <p>
                  Theo Lady Loki, một lượng đủ lớn loại bom này sẽ đủ sức làm
                  tắc nghẽn dòng thời gian và xóa sổ các Time-Keeper. Kế hoạch
                  khủng bố dòng thời gian của Lady Loki, về cơ bản cũng chính là
                  mục đích đánh bại Time-Keeper, xưng bá đa vũ trụ của Loki.
                </p>
                <p>
                  Hiện tại, động cơ của Lady Loki vẫn chưa được làm sáng tỏ. Đó
                  có thể là cơn khát quyền lực, cũng có thể là nỗi tuyệt vọng và
                  phẫn uất khi kẻ háo danh nhận ra mình giờ mãi mãi là một kẻ vô
                  danh. Cũng không thể loại trừ khả năng tình cảnh trớ trêu hiện
                  tại sẽ đẩy hai cái đầu tội phạm lên cùng một con thuyền.
                </p>
                <p>
                  Cuối tập 2, quả bom của Lady Loki đã phát nổ, lan khắp dòng
                  thời gian, gieo rắc hỗn loạn và đẻ thêm nhiều nhánh thực tại
                  sai lệch khác. Từ đây, bước đầu tiên trong kế hoạch hạ bệ TVA
                  của ả đã hoàn thành.
                </p>
                <h1>Lady Loki có thật chỉ là Loki?</h1>
                <p>
                  Trong tập 2, Loki cũng gợi ý rằng thân thế của Lady Loki có
                  thể còn ẩn chứa nhiều bất ngờ. Đầu tiên, phim hé lộ năng lực
                  thao túng tâm trí của Lady Loki quá khác biệt so với nguyên
                  mẫu Thần Lừa Lọc. Điều này khiến nhiều cái đầu ưa thuyết âm
                  mưu đặt giả thiết đây có thể là một nhân vật hoàn toàn khác.
                </p>
                <p style={{ textAlign: "center" }}>
                  <img src="/images/loki-4.png" style={{ width: "80%" }} />
                </p>
                <p style={{ textAlign: "center" }}>
                  <em>
                    Lady Loki là kẻ khủng bố dòng thời gian với thân thế và động
                    cơ mờ ám.
                  </em>
                </p>
                <p>
                  Tiếp đến, trong credit phiên bản tiếng Tây Ban Nha của Loki,
                  nhân vật do nữ diễn viên Sophia Di Martinos thủ vai được ghi
                  danh Sylvie thay vì Lady Loki. Điều này, cùng với năng lực tẩy
                  não diện rộng, càng củng cố cho giả thiết đây là một nhân vật
                  hoàn toàn mới thay vì biến thể của Loki. Cái tên được đưa vào
                  tầm ngắm là Sylvie Lushton.
                </p>
                <p>
                  Đây là nhân vật được Loki tạo ra trong truyện tranh. Gã đã
                  trao cho Sylvie Lushton, một cư dân New York, những quyền năng
                  của mình để biến cô thành tay sai đắc lực. Lushton đã bắt
                  chước nguyên mẫu Enchantress (Amora) - kẻ thù lớn nhất của
                  Thor.
                </p>
                <p>
                  Sau bất ngờ mang tên Quicksilver “trông vậy nhưng không phải
                  vậy” ở &nbsp;
                  <a href="https://tix.vn/phim/2191-fast-furious-9">
                    <strong>WandaVision,</strong>
                  </a>
                  &nbsp; có khả năng mini-series &nbsp;
                  <a href="https://tix.vn/phim/2191-fast-furious-9">
                    <strong>“Loki”</strong>
                  </a>
                  &nbsp; sẽ trộn lẫn nguồn gốc của Sylvie Lushton với nguyên mẫu
                  Enchantress xứ Asgard nhằm lý giải cho sự xuất hiện của Lady
                  Loki trên màn ảnh.
                </p>
                <p style={{ textAlign: "right" }}>
                  <strong>STEVEN</strong>
                </p>
              </div>
            </div>
            {/* ngIf: news.author_name !== '' */}
            <div className="row" id="scrollTo">
              <div className="col-sm-12 line">
                <hr />
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <div id="detailReviewer">
                  <div className="row isFlex detailMainStyle">
                    <div
                      className="col-sm-12 col-xs-12 dadInputReviewer dadInputReviewerNews newDesign"
                      ng-click="openCommentBox()"
                    >
                      {/* <span className="imgReviewer">
             
                        <img
                          ng-if="user.user_id"
                          ng-src="https://graph.facebook.com/123455529585217/picture?type=square"
                          className="ng-scope"
                          src="https://graph.facebook.com/123455529585217/picture?type=square"
                        />
                      
                      </span>
                      <input
                        className="inputReviwer"
                        type="text"
                        placeholder="Bạn nghĩ gì về bài viết này?"
                        readOnly="readonly"
                      /> */}
                      <div className="row">
                        <div className="col-12 mx-auto">
                          <CommentBoxNews props={this.props} />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* LIST COMMENTS */}
                  <div id="listComment">
                    {/* ngRepeat: comment in newsComments */}
                  </div>
                </div>
                {/* ngIf: (newsComments.length > 5) && (showCurrentComment < newsComments.length) */}
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 line">
                <hr />
              </div>
            </div>
            <div className="news2">
              <div className="news2__item">
                <div className="news2__img">
                  <img src="/images/news2.png" alt="imagesNews" />
                </div>
                <div className="news2__info">
                  <h1>
                    Tiệc Trăng Máu chính thức cán mốc 100 tỷ chỉ sau 2 tuần công
                    chiếu
                  </h1>
                  <p>
                    Sau 2 tuần ra mắt, Tiệc Trăng Máu chính thức gia nhập câu
                    lạc bộ phim điện ảnh đạt 100 tỷ đồng doanh thu phòng vé. Dàn
                    ngôi sao “bạc tỷ” của phim cũng lần đầu tiên hội tụ đầy đủ
                    trong một khung hình để ăn mừng thành tích ấn tượng của tác
                    phẩm.
                  </p>
                  <div className="news2__icon">
                    <i className="fa fa-thumbs-up" />
                    <span>2</span>
                    <i className="fa fa-comment-alt" />
                    <span>0</span>
                  </div>
                </div>
              </div>
              <div className="news2__item">
                <div className="news2__img">
                  <img src="/images/mortal-kombat-1.png" alt="imagesNews" />
                </div>
                <div className="news2__info">
                  <h1>
                    [MORTAL KOMBAT: CUỘC CHIẾN SINH TỬ] - GỌI TÊN NHỮNG PHIM
                    ĐIỆN ẢNH NỔI TIẾNG ĐƯỢC CHUYỂN THỂ TỪ CÁC TỰA GAME ĐÌNH ĐÁM
                  </h1>
                  <p>
                    Bên cạnh những kịch bản gốc mới mẻ và đầy bất ngờ, Hollywood
                    cũng không thiếu những tác phẩm đình đám được chuyển thể từ
                    tiểu thuyết, phim hoạt hình, hay thậm chí là cả trò chơi
                    điện tử. Với ý tưởng thú vị cùng cốt truyện mang đậm tính
                    phiêu lưu, các tựa game đã trở thành nguồn cảm hứng độc đáo
                    cho các nhà làm phim. Không chỉ vậy, kể cả với các fan của
                    tựa game gốc, việc được thấy các nhân vật mình yêu thích
                    trên màn ảnh rộng cũng là một trải nghiệm không thể bỏ qua.
                    Hãy cùng điểm lại những bộ phim đình đám bậc nhất của
                    Hollywood được chuyển thể từ các trò chơi điện tử nhé.
                  </p>
                  <div className="news2__icon">
                    <i className="fa fa-thumbs-up" />
                    <span>1</span>
                    <i className="fa fa-comment-alt" />
                    <span>1</span>
                  </div>
                </div>
              </div>
              <div className="news2__item news2__item2">
                <div className="item2__row">
                  <div className="item2__info">
                    <div className="item2__img">
                      <img src="/images/news5.png" alt="imagesNews" />
                    </div>
                    <div className="item2__detail">
                      <p>
                        Dàn mỹ nhân trong thế giới điện ảnh của quái kiệt
                        Christopher Nolan
                      </p>
                    </div>
                  </div>
                </div>
                <div className="item2__row">
                  <div className="item2__info">
                    <div className="item2__img">
                      <img src="/images/news6.png" alt="imagesNews" />
                    </div>
                    <div className="item2__detail">
                      <p>
                        Truy Cùng Giết Tận - Cuộc tái ngộ của hai 'ông hoàng
                        phòng vé' xứ Hàn
                      </p>
                    </div>
                  </div>
                </div>
                <div className="item2__row">
                  <div className="item2__info">
                    <div className="item2__img">
                      <img src="/images/news7.png" alt="imagesNews" />
                    </div>
                    <div className="item2__detail">
                      <p>
                        6 đạo diễn tỉ đô làm nên thành công của những bom tấn
                        đình đám nhất Hollywood
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="news__more">
              <button>XEM THÊM</button>
            </div>
            {/* ngIf: list_related.length > 4 */}
            {/* <div
            className="row isFlex text-center ViewMoreNews ng-scope"
            ng-if="list_related.length > 4"
          >
            <button
              id="btnNewsRelated"
              className="btnViewMore"
              ng-click="viewMoreNews()"
            >
              XEM THÊM
            </button>
          </div> */}
            {/* end ngIf: list_related.length > 4 */}
          </div>
        </section>
        <Footer />
      </>
    );
  }
}
