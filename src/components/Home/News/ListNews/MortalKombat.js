import React, { Component } from "react";
import Footer from "./../../Footer/Footer";
import CommentBoxNews from "./CommentBBox";
import { scroller } from "react-scroll";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import ChatBubbleIcon from "@material-ui/icons/ChatBubble";
import db from "./../../../../homePages/LoginHome/firebase.js";
import "./../../../../assets/sass/Home/news/index.scss";
import ShareIcon from "@material-ui/icons/Share";
let taiKhoan =
  (localStorage.getItem("userGoo") &&
    JSON.parse(localStorage.getItem("userGoo")).uid) ||
  (localStorage.getItem("userFace") &&
    JSON.parse(localStorage.getItem("userFace")).uid) ||
  (localStorage.getItem("userHome") &&
    JSON.parse(localStorage.getItem("userHome")).taiKhoan);
export default class MortalKombat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      soLikeArr: [],
      soLike: 0,
      nguoiLikeFast: [],
      nguoiLikeLoki: [],
      nguoiLikeTiec: [],
      nguoiLikeKom: [],
    };
    this.newsMoreButton = React.createRef();
  }
  componentDidMount() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    db.collection("news").onSnapshot((snapshot) => {
      let arr = snapshot.docs.map((doc) => ({ id: doc.id, data: doc.data() }));
      let soLikeArr;
      db.collection("soLikeBV").onSnapshot((snap) => {
        soLikeArr = snap.docs.map((doc) => ({ id: doc.id, data: doc.data() }));
        let nguoiLikeFast;

        soLikeArr.forEach((item) => {
          if (item.data.id === "/news/mortal-kombat") {
            nguoiLikeFast = item.data.nguoiLike;
          }
        });

        this.setState({
          posts: arr,
          soLikeArr,
          nguoiLikeFast,
        });
      });
    });
  }
  likeIconBV = (id) => {
    if (
      localStorage.getItem("userHome") ||
      localStorage.getItem("userFace") ||
      localStorage.getItem("userGoo")
    ) {
      this.state.soLikeArr.forEach((item) => {
        if (
          item.data.id == id &&
          item.data.nguoiLike &&
          item.data.nguoiLike.includes(taiKhoan)
        ) {
          let soLike = item.data.soLike - 1;
          item.data.nguoiLike.splice(item.data.nguoiLike.indexOf(taiKhoan, 1));
          console.log(item.data.nguoiLike, "nguoi like");
          db.collection("soLikeBV").doc(item.id).update({
            nguoiLike: item.data.nguoiLike,
            soLike,
          });
        } else if (
          item.data.id == id &&
          !(item.data.nguoiLike && item.data.nguoiLike.includes(taiKhoan))
        ) {
          item.data.nguoiLike.push(taiKhoan);
          console.log(item.data.nguoiLike, "nguoi like");
          let soLike = item.data.soLike + 1;

          db.collection("soLikeBV").doc(item.id).update({
            nguoiLike: item.data.nguoiLike,
            soLike: soLike,
          });
        }
      });
    } else {
      console.log("props like", this.props);
      this.props.history.push("/login");
    }
  };
  renderSoLike = (id) => {
    let soLike = 0;
    this.state.soLikeArr.forEach((item) => {
      if (item.data.id === id) {
        soLike = item.data.soLike;
      }
    });
    return soLike;
  };
  renderCM = (id) => {
    let soCM = 0;
    this.state.posts.map((item) => {
      if (item.data.id === id) {
        soCM += 1;
      }
    });
    return soCM;
  };
  handleSrollTo = (name, to) => {
    scroller.scrollTo(name, {
      duration: 1000,
      smooth: true,
    });
  };

  render() {
    return (
      <>
        <section className="list-news">
          <div className="container ng-scope" id="detailNews">
            <div className="row">
              <div className="col-sm-12 title">
                <p className="ng-binding">
                  [MORTAL KOMBAT: CUỘC CHIẾN SINH TỬ] - GỌI TÊN NHỮNG PHIM ĐIỆN
                  ẢNH NỔI TIẾNG ĐƯỢC CHUYỂN THỂ TỪ CÁC TỰA GAME ĐÌNH ĐÁM
                </p>
                {/* ngIf: !isMobile */}
                <div className="author ng-binding ng-scope" ng-if="!isMobile">
                  STEVEN <span className="ng-binding" /> 21.12.21
                </div>
                {/* end ngIf: !isMobile */}
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 count">
                <div
                  className="wrapIcon like wrapIconLike news2__icon mx-0"
                  data-type={1}
                  data-id={7848}
                  ng-click="likeAction($event, news, keyCache)"
                >
                  {(localStorage.getItem("userHome") ||
                    localStorage.getItem("userFace") ||
                    localStorage.getItem("userGoo")) &&
                  this.state.nguoiLikeFast &&
                  this.state.nguoiLikeFast.includes(taiKhoan) ? (
                    <button
                      className="text-primary mr-4"
                      onClick={() => this.likeIconBV("/news/mortal-kombat")}
                    >
                      <ThumbUpIcon />
                    </button>
                  ) : (
                    <button
                      className="mr-4"
                      onClick={() => this.likeIconBV("/news/mortal-kombat")}
                    >
                      <ThumbUpIcon />
                    </button>
                  )}
                  <span className="mr-5">
                    {this.renderSoLike("/news/mortal-kombat")} LIKE
                  </span>
                  <button
                    onClick={() =>
                      this.handleSrollTo("scrollTo", "/news/mortal-kombat")
                    }
                    className="mr-4"
                  >
                    <ChatBubbleIcon />
                  </button>
                  <span className="mr-5">
                    {this.renderCM("/news/mortal-kombat")} BÌNH LUẬN
                  </span>
                  <ShareIcon className="shareIcon mr-4" />
                  <span className="amount share ng-binding">0 CHIA SẺ</span>
                </div>
                <div
                  className="wrapIcon wrapIconShare"
                  ng-click="shareNews()"
                ></div>
              </div>
            </div>
            <div className="row">
              <div
                className="col-sm-12 content ng-binding"
                ng-bind-html="newsContent"
              >
                <p>
                  Bên cạnh những kịch bản gốc mới mẻ và đầy bất ngờ, Hollywood
                  cũng không thiếu những tác phẩm đình đám được chuyển thể từ
                  tiểu thuyết, phim hoạt hình, hay thậm chí là cả trò chơi điện
                  tử. Với ý tưởng thú vị cùng cốt truyện mang đậm tính phiêu
                  lưu, các tựa game đã trở thành nguồn cảm hứng độc đáo cho các
                  nhà làm phim. Không chỉ vậy, kể cả với các fan của tựa game
                  gốc, việc được thấy các nhân vật mình yêu thích trên màn ảnh
                  rộng cũng là một trải nghiệm không thể bỏ qua. Hãy cùng điểm
                  lại những bộ phim đình đám bậc nhất của Hollywood được chuyển
                  thể từ các trò chơi điện tử nhé.&nbsp;
                </p>
                <p>
                  <strong>Max Payne (2008)</strong>
                </p>
                <p>
                  <strong>Max Payne</strong> là một bộ phim kinh dị hành động
                  được phát hành vào năm 2008 dựa theo tựa game cùng tên phát
                  triển bởi Remedy Entertainment và sau đó là Rockstar Studios.
                  Tên phim được đặt theo nhân vật chính, Max Payne, một thanh
                  tra trên hành trình trả thù cho cái chết của vợ con mình.
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    alt
                    src="https://s3img.vcdn.vn/123phim/2021/03/MV5BNzM5OTAxYmQtODRkZC00ZGZlLTlhNzYtNzFiODVjMTY4MWU2XkEyXkFqcGdeQXVyNTIzOTk5ODM@-fffa4c._V1_"
                    style={{ width: "70%" }}
                  />
                </p>
                <p>
                  Bộ phim được đạo diễn bởi John Moore, do Dune Entertainment
                  sản xuất và 20th Century Fox phân phối. Mặc dù có nhiều nhận
                  xét tiêu cực về cốt truyện và cách triển khai nhân vật, song
                  phim vẫn gây hứng thú nhất định cho khán giả nhờ phần kỹ xảo
                  có thể coi là khá ổn lúc bấy giờ. Bộ phim mở màn ở vị trí số 1
                  tại phòng vé trong tuần đầu ra mắt và cán mốc doanh thu hơn 85
                  triệu đô.&nbsp;
                </p>
                <p>
                  <strong>Need For Speed (2014)</strong>
                </p>
                <p>
                  Chuyển thể từ thương hiệu game đua xe lớn nhất lúc bấy giờ,{" "}
                  <strong>Need For Speed (tựa Việt: Đam Mê Tốc Độ) </strong>là
                  một bộ phim hành động được ra mắt vào năm 2014. Được thủ vai
                  chính bởi Aaron Paul – người vừa kết thúc vai diễn Jesse
                  Pinkman trong series <strong>Breaking Bad</strong> đình đám,
                  bộ phim kể về câu chuyện tay đua đường phố Tobey Marshall bắt
                  đầu giải đua xuyên quốc gia để trả thù cho cái chết của bạn
                  mình.
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    alt
                    src="https://s3img.vcdn.vn/123phim/2021/03/need-for-speed-movie-poster-wallpaper-hd-aqtw1-464f24.jpg"
                    style={{ width: "70%" }}
                  />
                </p>
                <p>
                  Cũng như các phim chuyển thể khác,
                  <strong> Need For Speed</strong> nhận được những đánh giá tiêu
                  cực từ nhà phê bình nhưng vẫn được khán giả đón nhận một cách
                  hồ hởi. Vai diễn Tobey của Aaron Paul vẫn thể hiện được chiều
                  sâu cũng như nét cá tính riêng của mình.
                </p>
                <p>
                  <strong>Lara Croft Tomb Raider (2001)</strong>
                </p>
                <p>
                  <strong>
                    Lara Croft: Tomb Raider (tựa Việt: Kẻ Cướp Lăng Mộ)
                  </strong>{" "}
                  là phim đầu tiên trong loạt phim do Angelina Jolie thủ vai
                  chính. Được chuyển thể từ loạt trò chơi điện tử Tomb Raider,
                  phim kể về câu chuyện Lara Croft (Angelina Jolie) – một nhà
                  thám hiểm tìm hiểu về sự mất tích bí ẩn của cha cô. Đây là bộ
                  phim điện ảnh lớn đầu tiên được quay ở Campuchia, cụ thể là
                  đền Ta Prohm ở Angkor, tỉnh Siem Reap.
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    alt
                    src="https://s3img.vcdn.vn/123phim/2021/03/images-4db036.jpg"
                    style={{ width: "70%" }}
                  />
                </p>
                <p>
                  Tuy kịch bản có đôi chút chưa thỏa mãn, Lara Croft của
                  Angelina Jolie vẫn là một phiên bản hoàn hảo bước ra từ tựa
                  game gốc. Từ ngoại hình đến những cảnh hành động, tất cả đều
                  được khắc họa đúng như những gì các fan tưởng tượng về Lara
                  Croft. Phim gặt hái được những thành công đáng kể ở khía cạnh
                  phòng vé khi thu về 48.2 triệu đô trong tuần đầu tiên ra mắt
                  và là một trong những phim được chuyển thể từ game có doanh
                  thu cao nhất.&nbsp;
                </p>
                <p>
                  <strong>Resident Evil (2002)</strong>
                </p>
                <p style={{ textAlign: "center" }}>
                  <strong>
                    <img
                      alt
                      src="https://s3img.vcdn.vn/123phim/2021/03/MV5BZmI1ZGRhNDYtOGVjZC00MmUyLThlNTktMTQyZGE3MzE1ZTdlXkEyXkFqcGdeQXVyNDE5MTU2MDE@-44d483._V1_"
                      style={{ width: "70%" }}
                    />
                  </strong>
                </p>
                <p>
                  Mượn các yếu tố từ phần 1 và phần 2 của trò chơi cùng tên,{" "}
                  <strong>Resident Evil (tựa Việt: Vùng Đất Quỷ Dữ) </strong>là
                  bộ phim kinh dị hành động ra mắt vào năm 2002 do Milla
                  Jovovich thủ vai chính. Bộ phim kể về nữ anh hùng bị mất trí
                  nhớ Alice (Milla Jovovich) và một đội biệt kích của tập đoàn
                  Umbrella khi họ cố gắng ngăn chặn sự bùng phát của T-Virus tại
                  một cơ sở bí mật dưới lòng đất. Tuy một vài yếu tố được thay
                  đổi so với phiên bản trò chơi gốc, nhìn chung{" "}
                  <strong>Resident Evi</strong>l đã giúp người hâm mộ thỏa mãn
                  sự tò mò về một thành phố Raccoon đầy Zombie. Khác với những
                  phim chuyển thể từ <strong>game</strong> khác, Resident Evil
                  có tới tận 6 phần khác nhau và chất lượng đều rất ổn định.
                  Loạt phim này hiện này vẫn là loạt phim chuyển thể từ game có
                  tổng doanh cao nhất với hơn 1.2 tỷ đô cho 6 phần phim.
                </p>
                <p>
                  <strong>Hitman (2007)</strong>
                </p>
                <p style={{ textAlign: "center" }}>
                  <strong>
                    <img
                      alt
                      src="https://s3img.vcdn.vn/123phim/2021/03/MV5BMDlmMmZhZjQtZDhlMi00MzU0LWIwYjMtNDRhOGE5YzczYjBmXkEyXkFqcGdeQXVyNDQ2MTMzODA@-efd138._V1_"
                      style={{ width: "70%" }}
                    />
                  </strong>
                </p>
                <p>
                  Chuyển thể thành công nhân vật sát thủ có bộ com-lê đen cùng
                  chiếc cà vạt đỏ, bộ phim{" "}
                  <strong>
                    Hitman: Agent 47 (tựa Việt: Sát Thủ: Mật Danh 47)
                  </strong>{" "}
                  theo chân hành trình của Đặc vụ 47 – một sát thủ được tạo ra
                  từ quá trình biến đổi gen. Anh được một tổ chức bí mật thuê
                  thực hiện một nhiệm vụ và bị gài bẫy khiến anh bị cả Interpol
                  lẫn quân đội Nga truy lùng. Các nhân vật trong bộ phim được
                  xây dựng khá tốt, đặc biệt là Đặc vụ 47 thông qua rất nhiều
                  cảnh hành động.
                </p>
                <p>
                  <strong>Prince of Persia: The Sands of Time (2010)</strong>
                </p>
                <p style={{ textAlign: "center" }}>
                  <strong>
                    <img
                      alt
                      src="https://s3img.vcdn.vn/123phim/2021/03/MV5BMTMwNDg0NzcyMV5BMl5BanBnXkFtZTcwNjg4MjQyMw@@-f24346._V1_"
                      style={{ width: "70%" }}
                    />
                  </strong>
                </p>
                <p>
                  Một tựa game đình đám khác của Ubisoft -{" "}
                  <strong>Prince Of Persia</strong> cũng đã được mang lên màn
                  ảnh rộng và gặt hái nhiều thành công về mặt thương mại. Được
                  xây dựng chủ yếu xoay quanh 2 phần game là Warrior Within và
                  The Two Thrones, phim kể về Dastan (Jake Gyllenhaal) – một
                  chàng trai vô gia cư trở thành hoàng tử của Persia. Anh cùng
                  công chúa Tamina chiến đấu để ngăn chặn người chú của mình là
                  Nizam sở hữu sức mạnh Sands of Time – sức mạnh có thể đảo
                  ngược thời gian, qua đó thống trị Persia. Suốt một khoảng thời
                  gian, <strong>Prince Of Persia </strong>đã giữ kỷ lục về mặt
                  doanh thu cho những tựa phim chuyển thể từ game có thành tích
                  tốt nhất.&nbsp;
                </p>
                <p>
                  <strong>Warcraft: The Beginning (2006)</strong>
                </p>
                <p style={{ textAlign: "center" }}>
                  <strong>
                    <img
                      alt
                      src="https://s3img.vcdn.vn/123phim/2021/03/71r1lLZ3MiL-23148d._AC_SY679_"
                      style={{ width: "70%" }}
                    />
                  </strong>
                </p>
                <p>
                  Bộ phim đình đám chuyển thể từ một tựa game đình đám,
                  Warcraft: The Beginning ra mắt vào năm 2016 kể về vương quốc
                  Azeroth đứng trên bờ vực chiến tranh từ đội quân Orc – những
                  chiến binh đang tìm vùng đất mới để thay thế cho quê hương
                  đang chết dần của họ. Cốt truyện phim được xây dựng theo các
                  sự kiện trước và trong các sự kiện của phần game đầu tiên
                  Warcraft: Orcs &amp; Humans. Bộ phim nhận được sự yêu mến từ
                  những fan hâm mộ, đặc biệt là thị trường Trung Quốc. Với ngân
                  sách khổng lồ, bộ phim sở hữu phần kỹ xảo đồ họa xuất sắc và
                  không quá khó để nó vượt mặt Prince of Persia để trở thành bộ
                  phim chuyển thể từ game có doanh thu cao nhất mọi thời đại:
                  430 triệu đô la trên toàn thế giới.
                </p>
                <p>
                  <strong>Mortal Kombat (1995)</strong>
                </p>
                <p style={{ textAlign: "center" }}>
                  <strong>
                    <img
                      alt
                      src="https://s3img.vcdn.vn/123phim/2021/03/MV5BNjY5NTEzZGItMGY3My00NzE4LThkYTUtYjJkNzk3MDBiMWE3XkEyXkFqcGdeQXVyNzg5MDE1MDk@-08fada._V1_"
                      style={{ width: "70%" }}
                    />
                  </strong>
                </p>
                <p>
                  Ra mắt năm 1995, <strong>Mortal Kombat</strong> là một bộ phim
                  hành động võ thuật chuyển thể từ tựa game đối kháng cùng tên
                  nổi tiếng lúc bấy giờ. Tựa phim theo chân hành trình của Liu
                  Kang , Johnny Cage và Sonya Blade tham gia một giải đấu võ
                  thuật bí ẩn dưới sự lãnh đạo của thần sấm Raiden. Đây là cách
                  duy nhất để ngăn cản phù thủy Thượng Tông và đội quân của hắn
                  thôn tính Địa Giới. Sự kiện phim xoanh quanh giải đấu Mortal
                  Kombat đầu tiên nhưng có bổ sung rất nhiều nhân vật nổi tiếng
                  xuất hiện ở phiên bản trò chơi thứ 2. Phim nhận được sự đón
                  nhận nồng nhiệt từ phía người hâm mộ của trò chơi điện tử,
                  đứng đầu doanh thu phòng vé Mỹ 3 tuần liên tiếp và thu về hơn
                  124 triệu đô la trên toàn thế giới. Không chỉ vậy,{" "}
                  <strong>Mortal Kombat</strong> còn là bộ phim chuyển thể đầu
                  tiên thành công về mặt doanh thu cũng như là giữ kỷ lục doanh
                  thu này trong một khoảng thời gian dài.
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    alt
                    src="https://s3img.vcdn.vn/123phim/2021/03/mortal-caf027.jpg"
                    style={{ width: "70%" }}
                  />
                </p>
                <p>
                  Năm 2021, khán giả sẽ có cơ hội thưởng thức phiên bản điện ảnh
                  mới toanh của{" "}
                  <strong>
                    Mortal Kombat (tựa Việt: Mortal Kombat: Cuộc Chiến Sinh Tử).
                  </strong>{" "}
                  Trong trailer vừa phá vỡ kỷ lục “trailer nhãn R được xem nhiều
                  nhất” của phim, ta sẽ theo chân nhân vật mới Cole Young (Lewis
                  Tan) tìm hiểu về giải đấu bí mật Mortal Kombat - nơi mà Cole
                  Young cùng những võ sĩ ưu tú nhất của Địa Giới như Lưu Khang,
                  Công Lão, Sonya hay Jax sẽ hợp sức để chống lại tên phù thủy
                  độc ác Thượng Tông cùng âm mưu xâm chiếm Địa Giới của hắn. Với
                  việc gắn nhãn Red Band 17+ giống trò chơi nguyên tác, phim sẽ
                  không bị giới hạn bởi những cảnh hành động, những chiêu
                  fatality đặc trưng, qua đó chuyển thể được trọn vẹn hơn những
                  nét đặc sắc nhất trong trò chơi nguyên tác.&nbsp;
                </p>
                <p>
                  <strong>
                    MORTAL KOMBAT (tựa Việt: MORTAL KOMBAT: CUỘC CHIẾN SINH TỬ){" "}
                  </strong>
                  – dự kiến khởi chiếu 09.04.2021 trên toàn quốc với các định
                  dạng 2D, IMAX 2D.
                </p>
                <p style={{ textAlign: "right" }}>
                  <strong>STEVEN</strong>
                </p>
              </div>
            </div>
            {/* ngIf: news.author_name !== '' */}
            <div className="row" id="scrollTo">
              <div className="col-sm-12 line">
                <hr />
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <div id="detailReviewer">
                  <div className="row isFlex detailMainStyle">
                    <div
                      className="col-sm-12 col-xs-12 dadInputReviewer dadInputReviewerNews newDesign"
                      ng-click="openCommentBox()"
                    >
                      {/* <span className="imgReviewer">
                    
                        <img
                          ng-if="user.user_id"
                          ng-src="https://graph.facebook.com/123455529585217/picture?type=square"
                          className="ng-scope"
                          src="https://graph.facebook.com/123455529585217/picture?type=square"
                        />
                        
                      </span>
                      <input
                        className="inputReviwer"
                        type="text"
                        placeholder="Bạn nghĩ gì về bài viết này?"
                        readOnly="readonly"
                      /> */}
                      <div className="row">
                        <div className="col-12 mx-auto">
                          <CommentBoxNews props={this.props} />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* LIST COMMENTS */}
                  <div id="listComment">
                    {/* ngRepeat: comment in newsComments */}
                  </div>
                </div>
                {/* ngIf: (newsComments.length > 5) && (showCurrentComment < newsComments.length) */}
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 line">
                <hr />
              </div>
            </div>
            <div className="news2">
              <div className="news2__item">
                <div className="news2__img">
                  <img src="/images/news2.png" alt="imagesNews" />
                </div>
                <div className="news2__info">
                  <h1>
                    Tiệc Trăng Máu chính thức cán mốc 100 tỷ chỉ sau 2 tuần công
                    chiếu
                  </h1>
                  <p>
                    Sau 2 tuần ra mắt, Tiệc Trăng Máu chính thức gia nhập câu
                    lạc bộ phim điện ảnh đạt 100 tỷ đồng doanh thu phòng vé. Dàn
                    ngôi sao “bạc tỷ” của phim cũng lần đầu tiên hội tụ đầy đủ
                    trong một khung hình để ăn mừng thành tích ấn tượng của tác
                    phẩm.
                  </p>
                  <div className="news2__icon">
                    <i className="fa fa-thumbs-up" />
                    <span>2</span>
                    <i className="fa fa-comment-alt" />
                    <span>0</span>
                  </div>
                </div>
              </div>
              <div className="news2__item">
                <div className="news2__img">
                  <img src="/images/matrix-1.jpg" alt="imagesNews" />
                </div>
                <div className="news2__info">
                  <h1>Những nhân vật có thể quay trở lại trong The Matrix 4</h1>
                  <p>
                    The Oracle là một nhân vật quan trọng trong cả ba bộ phim
                    The Matrix. Mặc dù, chức năng chính của cô là tiên tri về sự
                    ra đời của The The One, nhưng cô cũng được tiết lộ là một
                    chương trình linh hoạt không thể tách rời với chính bản chất
                    và sự tồn tại của Ma trận. Thật khó để tưởng tượng làm thế
                    nào Ma trận 4 có thể mở ra mà không có sự hiện diện của The
                    Oracle với tư cách là một trong hai lực cân bằng của Ma
                    trận; cô đại diện cho yếu tố con người rõ ràng của chương
                    trình.
                  </p>
                  <div className="news2__icon">
                    <i className="fa fa-thumbs-up" />
                    <span>2</span>
                    <i className="fa fa-comment-alt" />
                    <span>0</span>
                  </div>
                </div>
              </div>
              <div className="news2__item news2__item2">
                <div className="item2__row">
                  <div className="item2__info">
                    <div className="item2__img">
                      <img src="/images/news5.png" alt="imagesNews" />
                    </div>
                    <div className="item2__detail">
                      <p>
                        Dàn mỹ nhân trong thế giới điện ảnh của quái kiệt
                        Christopher Nolan
                      </p>
                    </div>
                  </div>
                </div>
                <div className="item2__row">
                  <div className="item2__info">
                    <div className="item2__img">
                      <img src="/images/news6.png" alt="imagesNews" />
                    </div>
                    <div className="item2__detail">
                      <p>
                        Truy Cùng Giết Tận - Cuộc tái ngộ của hai 'ông hoàng
                        phòng vé' xứ Hàn
                      </p>
                    </div>
                  </div>
                </div>
                <div className="item2__row">
                  <div className="item2__info">
                    <div className="item2__img">
                      <img src="/images/news7.png" alt="imagesNews" />
                    </div>
                    <div className="item2__detail">
                      <p>
                        6 đạo diễn tỉ đô làm nên thành công của những bom tấn
                        đình đám nhất Hollywood
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="news__more">
              <button>XEM THÊM</button>
            </div>
            {/* ngIf: list_related.length > 4 */}
            {/* <div
              className="row isFlex text-center ViewMoreNews ng-scope"
              ng-if="list_related.length > 4"
            >
              <button
                id="btnNewsRelated"
                className="btnViewMore"
                ng-click="viewMoreNews()"
              >
                XEM THÊM
              </button>
            </div> */}
            {/* end ngIf: list_related.length > 4 */}
          </div>
        </section>
        <Footer />
      </>
    );
  }
}
