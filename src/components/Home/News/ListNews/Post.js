import React from "react";
import db from "./../../../../homePages/LoginHome/firebase";
import Avatar from "@material-ui/core/Avatar";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import Box from "@material-ui/core/Box";
export default function Post({
  profilePic,
  image,
  username,
  timestamp,
  message,

  likeComment,
  id,
  props,
  nguoiLike,
}) {
  console.log("id post", id);
  let taiKhoan =
    (localStorage.getItem("userGoo") &&
      JSON.parse(localStorage.getItem("userGoo")).uid) ||
    (localStorage.getItem("userFace") &&
      JSON.parse(localStorage.getItem("userFace")).uid) ||
    (localStorage.getItem("userHome") &&
      JSON.parse(localStorage.getItem("userHome")).taiKhoan);
  let d = new Date(timestamp?.toDate()) + "";
  const likeIcon = () => {
    if (
      localStorage.getItem("userHome") ||
      localStorage.getItem("userFace") ||
      localStorage.getItem("userGoo")
    ) {
      if (nguoiLike && nguoiLike.includes(taiKhoan)) {
        likeComment = likeComment - 1;
        nguoiLike.splice(nguoiLike.indexOf(taiKhoan, 1));
        db.collection("news").doc(id).update({
          likeComment: likeComment,
          nguoiLike: nguoiLike,
        });
      } else {
        nguoiLike.push(taiKhoan);
        console.log("like");
        likeComment += 1;
        console.log(id);
        console.log(likeComment);
        db.collection("news").doc(id).update({
          likeComment: likeComment,
          nguoiLike: nguoiLike,
        });
      }
    } else {
      props.props.history.push("/login");
    }
  };
  return (
    <div className="post border">
      <div className="d-flex justify-content-between">
        <div className="post__Top">
          <Avatar src={profilePic} className="post__Avatar" />
          <div className="post__TopInfo">
            <h3>{username}</h3>
            <p>{d}</p>
          </div>
        </div>
        {/* <HoverRating danhGia={danhGia} /> */}
      </div>
      <div className="post__Bottom">
        <p>{message}</p>
      </div>
      <div className="post__image">
        <img src={image} alt="" />
      </div>
      <div className="post__options">
        <div className="post__option">
          {(localStorage.getItem("userHome") ||
            localStorage.getItem("userFace") ||
            localStorage.getItem("userGoo")) &&
          nguoiLike &&
          nguoiLike.includes(taiKhoan) ? (
            <button className="text-primary" onClick={likeIcon}>
              <ThumbUpIcon />
            </button>
          ) : (
            <button onClick={likeIcon}>
              <ThumbUpIcon />
            </button>
          )}
          <p>{likeComment}</p>
        </div>
      </div>
    </div>
  );
}
