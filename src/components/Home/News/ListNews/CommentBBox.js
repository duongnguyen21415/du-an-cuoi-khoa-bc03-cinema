import React, { Component } from "react";
import Avatar from "@material-ui/core/Avatar";
import "./../../../../adminPage/AddFilm/taoLichChieu.css";
import "./../../../../assets/sass/Home/Comment/index.scss";
import db from "./../../../../homePages/LoginHome/firebase";
import firebase from "firebase";
import RenderComment from "./RenderComment";
export default class CommentBoxNews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valuesMes: "",
      imgUrl: "",
      posts: [],
    };
    this.close = React.createRef();
  }
  dangNhapPage = () => {
    this.props.props.history.push("/login");
  };
  handleOnSubmit = (e) => {
    e.preventDefault();
    console.log("handle", this.state.valuesMes, this.state.imgUrl);
    if (
      localStorage.getItem("userGoo") ||
      localStorage.getItem("userFace") ||
      localStorage.getItem("userHome")
    ) {
      db.collection("news").add({
        message: this.state.valuesMes,
        timestamp: firebase.firestore.FieldValue.serverTimestamp(),
        profilePic:
          (localStorage.getItem("userGoo") &&
            JSON.parse(localStorage.getItem("userGoo")).photoURL) ||
          (localStorage.getItem("userFace") &&
            JSON.parse(localStorage.getItem("userFace")).photoURL) ||
          (localStorage.getItem("userHome") &&
            JSON.parse(localStorage.getItem("userHome"))
              .hoTen.slice(0, 1)
              .toUpperCase()),
        username:
          (localStorage.getItem("userHome") &&
            JSON.parse(localStorage.getItem("userHome")).hoTen) ||
          (localStorage.getItem("userGoo") &&
            JSON.parse(localStorage.getItem("userGoo")).displayName) ||
          (localStorage.getItem("userFace") &&
            JSON.parse(localStorage.getItem("userFace")).displayName),
        image: this.state.imgUrl,

        id: this.props.props.match.path,
        likeComment: 0,
        nguoiLike: [],
      });
      console.log("thanh cong");
    } else {
      this.props.props.history.push("/login");
    }

    this.setState({
      valueMes: "",
      imgUrl: "",
    });
    this.close.current.click();

    // if(      localStorage.getItem("userGoo") ||
    // localStorage.getItem("userFace") ||
    // localStorage.getItem("userHome")){
    //     db.collection("news").add({

    //     })
    // }
  };
  componentDidMount() {
    db.collection("news").onSnapshot((snapshot) => {
      let arr = snapshot.docs.map((doc) => ({ id: doc.id, data: doc.data() }));

      if (arr) {
        let arrSapXep = arr.sort(function (a, b) {
          if (a.data && b.data) {
            return (
              new Date(b.data.timestamp?.toDate()) -
              new Date(a.data.timestamp?.toDate())
            );
          }
        });
        this.setState({
          posts: arrSapXep,
        });
      }
    });
  }

  render() {
    console.log(this.props.props);
    return (
      <div>
        <div className="w-100">
          {localStorage.getItem("userHome") ||
          localStorage.getItem("userGoo") ||
          localStorage.getItem("userFace") ? (
            <button
              type="button"
              className="btn btn-primary d-flex w-100 align-items-center justify-content-between bg-white"
              data-toggle="modal"
              data-target="#exampleModalCenter"
            >
              <div className="d-flex align-items-center">
                {" "}
                {(localStorage.getItem("userHome") && (
                  <Avatar>
                    {JSON.parse(localStorage.getItem("userHome"))
                      .hoTen.slice(0, 1)
                      .toUpperCase()}
                  </Avatar>
                )) ||
                  (localStorage.getItem("userGoo") && (
                    <Avatar
                      src={JSON.parse(localStorage.getItem("userGoo")).photoURL}
                    />
                  )) ||
                  (localStorage.getItem("userFace") && (
                    <Avatar
                      src={
                        JSON.parse(localStorage.getItem("userFace")).photoURL
                      }
                    />
                  )) || <Avatar />}
                <span className="ml-1 text-dark">
                  Bạn nghĩ gì về bài viết này?
                </span>
              </div>
            </button>
          ) : (
            <button
              onClick={this.dangNhapPage}
              className="btn btn-outline-danger w-100"
            >
              Nhấp vào đây để đăng nhập và bình luận nào
            </button>
          )}
          <div
            className="modal fade"
            id="exampleModalCenter"
            tabIndex={-1}
            role="dialog"
            aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true"
          >
            <div className="modal-dialog modal-dialog-centered" role="document">
              <div className="modal-content px-3">
                <button
                  type="button"
                  className="close text-right"
                  data-dismiss="modal"
                  aria-label="Close"
                  ref={this.close}
                >
                  <span aria-hidden="true">×</span>
                </button>
                <div className="modal-body d-flex flex-column align-items-center">
                  {/* <HoverRating danhGia={this.danhGiaFun} /> */}
                  <form className="messageSender w-100 mb-3">
                    <input
                      value={this.state.valueMes}
                      className="messageSender_input w-100 px-1 pt-1 pb-5 mb-2"
                      placeholder="Bạn nghĩ gì về bài viết này?"
                      onChange={(e) =>
                        this.setState({ valuesMes: e.target.value })
                      }
                    />
                    <div className="d-flex justify-content-between">
                      <input
                        className="btn btn-outline-success"
                        placeholder="Ảnh URL"
                        value={this.state.imgUrl}
                        onChange={(e) => {
                          this.setState({ imgUrl: e.target.value });
                          console.log(e.target.value);
                        }}
                      />
                      <button
                        type="submit"
                        onClick={this.handleOnSubmit}
                        className="btn btn-danger"
                      >
                        Đăng
                      </button>
                    </div>
                  </form>
                  <div></div>
                </div>
              </div>
            </div>
          </div>
          {/* <RenderComment
          posts={this.state.posts}
          id={this.props.props.match.params.id}
          props={this.props}
        /> */}
          <RenderComment
            posts={this.state.posts}
            id={this.props.props.match.path}
            props={this.props}
          />
        </div>
      </div>
    );
  }
}
