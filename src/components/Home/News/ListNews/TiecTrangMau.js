import React, { Component } from "react";
import Footer from "./../../Footer/Footer";
import CommentBoxNews from "./CommentBBox";
import { scroller } from "react-scroll";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import ChatBubbleIcon from "@material-ui/icons/ChatBubble";
import db from "./../../../../homePages/LoginHome/firebase.js";
import "./../../../../assets/sass/Home/news/index.scss";
import ShareIcon from "@material-ui/icons/Share";
let taiKhoan =
  (localStorage.getItem("userGoo") &&
    JSON.parse(localStorage.getItem("userGoo")).uid) ||
  (localStorage.getItem("userFace") &&
    JSON.parse(localStorage.getItem("userFace")).uid) ||
  (localStorage.getItem("userHome") &&
    JSON.parse(localStorage.getItem("userHome")).taiKhoan);
export default class TiecTrangMau extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      soLikeArr: [],
      soLike: 0,
      nguoiLikeFast: [],
      nguoiLikeLoki: [],
      nguoiLikeTiec: [],
      nguoiLikeKom: [],
    };
    this.newsMoreButton = React.createRef();
  }
  componentDidMount() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    db.collection("news").onSnapshot((snapshot) => {
      let arr = snapshot.docs.map((doc) => ({ id: doc.id, data: doc.data() }));
      let soLikeArr;
      db.collection("soLikeBV").onSnapshot((snap) => {
        soLikeArr = snap.docs.map((doc) => ({ id: doc.id, data: doc.data() }));
        let nguoiLikeFast;

        soLikeArr.forEach((item) => {
          if (item.data.id === "/news/tiec-trang-mau") {
            nguoiLikeFast = item.data.nguoiLike;
          }
        });

        this.setState({
          posts: arr,
          soLikeArr,
          nguoiLikeFast,
        });
      });
    });
  }
  likeIconBV = (id) => {
    if (
      localStorage.getItem("userHome") ||
      localStorage.getItem("userFace") ||
      localStorage.getItem("userGoo")
    ) {
      this.state.soLikeArr.forEach((item) => {
        if (
          item.data.id == id &&
          item.data.nguoiLike &&
          item.data.nguoiLike.includes(taiKhoan)
        ) {
          let soLike = item.data.soLike - 1;
          item.data.nguoiLike.splice(item.data.nguoiLike.indexOf(taiKhoan, 1));
          console.log(item.data.nguoiLike, "nguoi like");
          db.collection("soLikeBV").doc(item.id).update({
            nguoiLike: item.data.nguoiLike,
            soLike,
          });
        } else if (
          item.data.id == id &&
          !(item.data.nguoiLike && item.data.nguoiLike.includes(taiKhoan))
        ) {
          item.data.nguoiLike.push(taiKhoan);
          console.log(item.data.nguoiLike, "nguoi like");
          let soLike = item.data.soLike + 1;

          db.collection("soLikeBV").doc(item.id).update({
            nguoiLike: item.data.nguoiLike,
            soLike: soLike,
          });
        }
      });
    } else {
      console.log("props like", this.props);
      this.props.history.push("/login");
    }
  };
  renderSoLike = (id) => {
    let soLike = 0;
    this.state.soLikeArr.forEach((item) => {
      if (item.data.id === id) {
        soLike = item.data.soLike;
      }
    });
    return soLike;
  };
  renderCM = (id) => {
    let soCM = 0;
    this.state.posts.map((item) => {
      if (item.data.id === id) {
        soCM += 1;
      }
    });
    return soCM;
  };
  handleSrollTo = (name, to) => {
    scroller.scrollTo(name, {
      duration: 1000,
      smooth: true,
    });
  };

  render() {
    return (
      <>
        <section className="list-news">
          <div className="container ng-scope" id="detailNews">
            <div className="row">
              <div className="col-sm-12 title">
                <p className="ng-binding">
                  Tiệc Trăng Máu chính thức cán mốc 100 tỷ chỉ sau 2 tuần công
                  chiếu{" "}
                </p>
                {/* ngIf: !isMobile */}
                <div className="author ng-binding ng-scope" ng-if="!isMobile">
                  STEVEN <span className="ng-binding" /> 21.12.21
                </div>
                {/* end ngIf: !isMobile */}
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 count">
                <div
                  className="wrapIcon like wrapIconLike news2__icon mx-0"
                  data-type={1}
                  data-id={7848}
                  ng-click="likeAction($event, news, keyCache)"
                >
                  {(localStorage.getItem("userHome") ||
                    localStorage.getItem("userFace") ||
                    localStorage.getItem("userGoo")) &&
                  this.state.nguoiLikeFast &&
                  this.state.nguoiLikeFast.includes(taiKhoan) ? (
                    <button
                      className="text-primary mr-4"
                      onClick={() => this.likeIconBV("/news/tiec-trang-mau")}
                    >
                      <ThumbUpIcon />
                    </button>
                  ) : (
                    <button
                      className="mr-4"
                      onClick={() => this.likeIconBV("/news/tiec-trang-mau")}
                    >
                      <ThumbUpIcon />
                    </button>
                  )}
                  <span className="mr-5">
                    {this.renderSoLike("/news/tiec-trang-mau")} LIKE
                  </span>
                  <button
                    onClick={() =>
                      this.handleSrollTo("scrollTo", "/news/tiec-trang-mau")
                    }
                    className="mr-4"
                  >
                    <ChatBubbleIcon />
                  </button>
                  <span className="mr-5">
                    {this.renderCM("/news/tiec-trang-mau")} BÌNH LUẬN
                  </span>
                  <ShareIcon className="shareIcon mr-4" />
                  <span className="amount share ng-binding">0 CHIA SẺ</span>
                </div>
                <div
                  className="wrapIcon wrapIconShare"
                  ng-click="shareNews()"
                ></div>
              </div>
            </div>
            <div className="row">
              <div
                className="col-sm-12 content ng-binding"
                ng-bind-html="newsContent"
              >
                <p>
                  <strong>
                    Tiệc Trăng Máu chính thức cán mốc 100 tỷ chỉ sau 2 tuần công
                    chiếu&nbsp;
                  </strong>
                </p>
                <p>
                  Sau 2 tuần ra mắt, Tiệc Trăng Máu chính thức gia nhập câu lạc
                  bộ phim điện ảnh đạt 100 tỷ đồng doanh thu phòng vé. Dàn ngôi
                  sao “bạc tỷ” của phim cũng lần đầu tiên hội tụ đầy đủ trong
                  một khung hình để ăn mừng thành tích ấn tượng của tác
                  phẩm.&nbsp;
                </p>
                <p>
                  Sau tuần đầu mở màn thành công, bộ phim điện ảnh Tiệc Trăng
                  Máu của đạo diễn Nguyễn Quang Dũng và nhà sản xuất Phan Gia
                  Nhật Linh đã chính thức cán mốc 100 tỷ đồng doanh thu phòng vé
                  chỉ sau tuần thứ hai công chiếu (tính từ ngày 26/10 – hết ngày
                  2/11), theo số liệu được công bố chính từ nhà phát hành Lotte
                  Entertainment.&nbsp;
                </p>
                <p>
                  <img
                    src="https://s3img.vcdn.vn/123phim/2020/11/a8963d351c6d3fd2764171e2bc066d04.jpg"
                    style={{ heigth: "auto", width: "auto" }}
                  />
                </p>
                <p>
                  Tính đến nay, bộ phim đã thu hút hơn 1,4 triệu lượt khán giả
                  ra rạp thưởng thức bộ phim. Theo đó, Tiệc Trăng Máu chính thức
                  trở thành bộ phim điện ảnh có doanh thu cao thứ nhì trong năm
                  2020, chỉ xếp sau Gái Già Lắm Chiêu 3 khởi chiếu dịp Tết vừa
                  qua.&nbsp;
                </p>
                <p>
                  Đặc biệt, dù bước sang tuần chiếu thứ hai, bộ phim của Nguyễn
                  Quang Dũng vẫn đem lại doanh thu ấn tượng với gần 27 tỷ đồng
                  chỉ trong 3 ngày cuối tuần, chỉ giảm rất ít so với tuần trước
                  đó. Tiệc Trăng Máu là một trong những bộ phim Việt có sự sụt
                  giảm thấp nhất khi sang đến tuần chiếu thứ hai.&nbsp;
                </p>
                <p>
                  <img
                    src="https://s3img.vcdn.vn/123phim/2020/11/7d18092bec10275fb90c21fe163b7649.png"
                    style={{ heigth: "auto", width: "auto" }}
                  />
                </p>
                <p>
                  Tỷ lệ giảm thấp này chứng tỏ sức hút không hề giảm của bộ phim
                  bởi chất lượng đã được nhiều khán giả kiểm chứng cùng với yếu
                  tố truyền miệng mạnh mẽ. Nhiều khả năng, bộ phim sẽ tiếp tục
                  dẫn đầu phòng vé trong tuần tới, đạt kỷ lục 3 tuần liên tiếp
                  tuần đứng nhất mà trước đó chỉ có phim Mắt Biếc làm
                  được.&nbsp;
                </p>
                <p>
                  Đặc biệt, đây chính là bộ phim đầu tiên của đạo diễn Nguyễn
                  Quang Dũng chạm mốc 100 tỷ đồng doanh thu phòng vé. Trước đó,
                  bộ phim có thành tích cao nhất của anh là Tháng Năm Rực Rỡ
                  (2018) với 84 tỷ đồng.&nbsp;
                </p>
                <p>
                  <img
                    src="https://s3img.vcdn.vn/123phim/2020/11/c9752b4209f181263a3891a8dceb64d6.png"
                    style={{ heigth: "auto", width: "auto" }}
                  />
                </p>
                <p>
                  Không giấu được cảm xúc, đạo diễn Nguyễn Quang Dũng bày tỏ:
                  “Thật sự thở phào nhẹ nhõm vì thấy đây là thành tích xứng đáng
                  dành cho công sức của đoàn phim. Nhưng hơn hết, tôi thật sự
                  vui mừng khi thấy thị trường rạp chiếu phim sôi động trở lại
                  khi có được&nbsp;
                </p>
                <p>
                  một bộ phim nhận được sự yêu mến của khán giả. Được thấy khán
                  giả háo hức đến rạp sau một năm quá ảm đạm là điều quá đỗi
                  hạnh phúc với nhà làm phim”.&nbsp;&nbsp;
                </p>
                <p>
                  Thành tích ấn tượng của Tiệc Trăng Máu là phần thưởng xứng
                  đáng, một lần nữa khẳng định chất lượng và sức hấp dẫn của bộ
                  phim. Tác phẩm không chỉ giúp đem lại những giây phút giải trí
                  cho khán giả mà còn đọng lại trong lòng họ những trăn trở, suy
                  nghĩ, những tiếng cười chua cay trước những câu chuyện, mắt
                  tối về cuộc sống, gia đình được thể hiện một cách trào phúng,
                  đầy châm biếm trong phim.&nbsp;
                </p>
                <p>
                  <img
                    src="https://s3img.vcdn.vn/123phim/2020/11/28452aca97f025e13db5e9c374865a42.png"
                    style={{ heigth: "auto", width: "auto" }}
                  />
                </p>
                <p>
                  Với nhà sản xuất Phan Gia Nhật Linh anh dành lời cảm ơn khán
                  giả đã ủng hộ bộ phim nói riêng và cả ngành điện ảnh nói
                  chung. “Mặc dù Tiệc Trăng Máu ra mắt trong một năm nhiều khó
                  khăn, từ bệnh dịch đến mưa lũ nhưng rất cám ơn đoàn phim đã
                  làm hết mình để khán giả có những giây phút 'thoát ly thực
                  tại' để được cười khóc, thư giãn và suy ngẫm”, anh chia
                  sẻ.&nbsp;
                </p>
                <p>
                  Thành công của Tiệc Trăng Máu đã góp phần không nhỏ trong việc
                  hâm nóng phòng vé và khiến cho thị trường điện ảnh mùa cuối
                  năm trở nên sôi động và đáng mong chờ hơn với nhiều bộ phim đã
                  lên lịch ra mắt trong thời gian tới.&nbsp;
                </p>
                <p>
                  Nhiều khán giả đã dần quay lại với thói quen ra rạp xem phim
                  và sẵn sàng bỏ tiền mua vé ủng hộ những tác phẩm điện ảnh có
                  chất lượng. Đây là tín hiệu đáng mừng cho cả ngành điện ảnh
                  Việt nói chung và hệ thống rạp chiếu phim trên toàn quốc vốn
                  đã phải chịu nhiều tổn thất sau mùa dịch kéo dài.&nbsp;
                </p>
                <p>
                  Ngoài ra, để ăn mừng bộ phim đạt được doanh thu như mong đợi,
                  đạo diễn Nguyễn Quang Dũng cùng dàn diễn viên ngôi sao từ Thái
                  Hoà, Đức Thịnh, Thu Trang, Hứa Vĩ Văn, Hồng Ánh, Kiều Minh
                  Tuấn và Kaity Nguyễn đã cùng hội tụ trong một bộ ảnh độc
                  đáo.&nbsp;
                </p>
                <p>
                  Thành công của Tiệc Trăng Máu đã góp phần thúc đẩy phòng vé và
                  mở đường cho nhiều bộ phim tiếp theo ra rạp.&nbsp;
                </p>
                <p>
                  Đặc biệt, đây là lần đầu tiên mà đầy đủ dàn diễn viên của Tiệc
                  Trăng Máu góp mặt cùng nhau trong một khung hình kể từ khi
                  phim đóng máy đến nay. Bởi lẽ, tất cả dàn diễn viên của phim
                  đều sở hữu lịch trình cực kỳ bận rộn khi đóng nhiều vai trò từ
                  diễn viên, đạo diễn đến nhà sản xuất. Vì vậy, bộ ảnh đặc biệt
                  này chính là lời tri ân của ekip Tiệc Trăng Máu gửi tặng cho
                  những khán giả đã ủng hộ bộ phim và giúp bộ phim tiến xa hơn
                  nữa trong thời gian tới.&nbsp;
                </p>
                <p>Tiệc Trăng Máu đang chiếu tại các rạp trên toàn quốc.</p>
                <p style={{ textAlign: "right" }}>
                  <strong>STEVEN</strong>
                </p>
              </div>
            </div>
            {/* ngIf: news.author_name !== '' */}
            <div className="row" id="scrollTo">
              <div className="col-sm-12 line">
                <hr />
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <div id="detailReviewer">
                  <div className="row isFlex detailMainStyle">
                    <div
                      className="col-sm-12 col-xs-12 dadInputReviewer dadInputReviewerNews newDesign"
                      ng-click="openCommentBox()"
                    >
                      {/* <span className="imgReviewer">
                
                        <img
                          ng-if="user.user_id"
                          ng-src="https://graph.facebook.com/123455529585217/picture?type=square"
                          className="ng-scope"
                          src="https://graph.facebook.com/123455529585217/picture?type=square"
                        />
                      
                      </span>
                      <input
                        className="inputReviwer"
                        type="text"
                        placeholder="Bạn nghĩ gì về bài viết này?"
                        readOnly="readonly"
                      /> */}
                      <div className="row">
                        <div className="col-12 mx-auto">
                          <CommentBoxNews props={this.props} />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* LIST COMMENTS */}
                  <div id="listComment">
                    {/* ngRepeat: comment in newsComments */}
                  </div>
                </div>
                {/* ngIf: (newsComments.length > 5) && (showCurrentComment < newsComments.length) */}
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 line">
                <hr />
              </div>
            </div>
            <div className="news2">
              <div className="news2__item">
                <div className="news2__img">
                  <img src="/images/matrix-1.jpg" alt="imagesNews" />
                </div>
                <div className="news2__info">
                  <h1>Những nhân vật có thể quay trở lại trong The Matrix 4</h1>
                  <p>
                    The Oracle là một nhân vật quan trọng trong cả ba bộ phim
                    The Matrix. Mặc dù, chức năng chính của cô là tiên tri về sự
                    ra đời của The The One, nhưng cô cũng được tiết lộ là một
                    chương trình linh hoạt không thể tách rời với chính bản chất
                    và sự tồn tại của Ma trận. Thật khó để tưởng tượng làm thế
                    nào Ma trận 4 có thể mở ra mà không có sự hiện diện của The
                    Oracle với tư cách là một trong hai lực cân bằng của Ma
                    trận; cô đại diện cho yếu tố con người rõ ràng của chương
                    trình.
                  </p>
                  <div className="news2__icon">
                    <i className="fa fa-thumbs-up" />
                    <span>2</span>
                    <i className="fa fa-comment-alt" />
                    <span>0</span>
                  </div>
                </div>
              </div>
              <div className="news2__item">
                <div className="news2__img">
                  <img src="/images/mortal-kombat-1.png" alt="imagesNews" />
                </div>
                <div className="news2__info">
                  <h1>
                    [MORTAL KOMBAT: CUỘC CHIẾN SINH TỬ] - GỌI TÊN NHỮNG PHIM
                    ĐIỆN ẢNH NỔI TIẾNG ĐƯỢC CHUYỂN THỂ TỪ CÁC TỰA GAME ĐÌNH ĐÁM
                  </h1>
                  <p>
                    Bên cạnh những kịch bản gốc mới mẻ và đầy bất ngờ, Hollywood
                    cũng không thiếu những tác phẩm đình đám được chuyển thể từ
                    tiểu thuyết, phim hoạt hình, hay thậm chí là cả trò chơi
                    điện tử. Với ý tưởng thú vị cùng cốt truyện mang đậm tính
                    phiêu lưu, các tựa game đã trở thành nguồn cảm hứng độc đáo
                    cho các nhà làm phim. Không chỉ vậy, kể cả với các fan của
                    tựa game gốc, việc được thấy các nhân vật mình yêu thích
                    trên màn ảnh rộng cũng là một trải nghiệm không thể bỏ qua.
                    Hãy cùng điểm lại những bộ phim đình đám bậc nhất của
                    Hollywood được chuyển thể từ các trò chơi điện tử nhé.
                  </p>
                  <div className="news2__icon">
                    <i className="fa fa-thumbs-up" />
                    <span>1</span>
                    <i className="fa fa-comment-alt" />
                    <span>1</span>
                  </div>
                </div>
              </div>
              <div className="news2__item news2__item2">
                <div className="item2__row">
                  <div className="item2__info">
                    <div className="item2__img">
                      <img src="/images/news5.png" alt="imagesNews" />
                    </div>
                    <div className="item2__detail">
                      <p>
                        Dàn mỹ nhân trong thế giới điện ảnh của quái kiệt
                        Christopher Nolan
                      </p>
                    </div>
                  </div>
                </div>
                <div className="item2__row">
                  <div className="item2__info">
                    <div className="item2__img">
                      <img src="/images/news6.png" alt="imagesNews" />
                    </div>
                    <div className="item2__detail">
                      <p>
                        Truy Cùng Giết Tận - Cuộc tái ngộ của hai 'ông hoàng
                        phòng vé' xứ Hàn
                      </p>
                    </div>
                  </div>
                </div>
                <div className="item2__row">
                  <div className="item2__info">
                    <div className="item2__img">
                      <img src="/images/news7.png" alt="imagesNews" />
                    </div>
                    <div className="item2__detail">
                      <p>
                        6 đạo diễn tỉ đô làm nên thành công của những bom tấn
                        đình đám nhất Hollywood
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="news__more">
              <button>XEM THÊM</button>
            </div>
            {/* ngIf: list_related.length > 4 */}
            {/* <div
              className="row isFlex text-center ViewMoreNews ng-scope"
              ng-if="list_related.length > 4"
            >
              <button
                id="btnNewsRelated"
                className="btnViewMore"
                ng-click="viewMoreNews()"
              >
                XEM THÊM
              </button>
            </div> */}
            {/* end ngIf: list_related.length > 4 */}
          </div>
        </section>
        <Footer />
      </>
    );
  }
}
