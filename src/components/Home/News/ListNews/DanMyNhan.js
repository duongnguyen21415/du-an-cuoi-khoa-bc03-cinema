import React, { Component } from "react";
import Footer from "./../../Footer/Footer";
import CommentBoxNews from "./CommentBBox";
import { scroller } from "react-scroll";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import ChatBubbleIcon from "@material-ui/icons/ChatBubble";
import db from "./../../../../homePages/LoginHome/firebase.js";
import "./../../../../assets/sass/Home/news/index.scss";
import ShareIcon from "@material-ui/icons/Share";
let taiKhoan =
  (localStorage.getItem("userGoo") &&
    JSON.parse(localStorage.getItem("userGoo")).uid) ||
  (localStorage.getItem("userFace") &&
    JSON.parse(localStorage.getItem("userFace")).uid) ||
  (localStorage.getItem("userHome") &&
    JSON.parse(localStorage.getItem("userHome")).taiKhoan);
export default class DanMyNhan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      soLikeArr: [],
      soLike: 0,
      nguoiLikeFast: [],
      nguoiLikeLoki: [],
      nguoiLikeTiec: [],
      nguoiLikeKom: [],
    };
    this.newsMoreButton = React.createRef();
  }
  componentDidMount() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    db.collection("news").onSnapshot((snapshot) => {
      let arr = snapshot.docs.map((doc) => ({ id: doc.id, data: doc.data() }));
      let soLikeArr;
      db.collection("soLikeBV").onSnapshot((snap) => {
        soLikeArr = snap.docs.map((doc) => ({ id: doc.id, data: doc.data() }));
        let nguoiLikeFast;

        soLikeArr.forEach((item) => {
          if (item.data.id === "/news/dan-my-nhan") {
            nguoiLikeFast = item.data.nguoiLike;
          }
        });

        this.setState({
          posts: arr,
          soLikeArr,
          nguoiLikeFast,
        });
      });
    });
  }
  likeIconBV = (id) => {
    if (
      localStorage.getItem("userHome") ||
      localStorage.getItem("userFace") ||
      localStorage.getItem("userGoo")
    ) {
      this.state.soLikeArr.forEach((item) => {
        if (
          item.data.id == id &&
          item.data.nguoiLike &&
          item.data.nguoiLike.includes(taiKhoan)
        ) {
          let soLike = item.data.soLike - 1;
          item.data.nguoiLike.splice(item.data.nguoiLike.indexOf(taiKhoan, 1));
          console.log(item.data.nguoiLike, "nguoi like");
          db.collection("soLikeBV").doc(item.id).update({
            nguoiLike: item.data.nguoiLike,
            soLike,
          });
        } else if (
          item.data.id == id &&
          !(item.data.nguoiLike && item.data.nguoiLike.includes(taiKhoan))
        ) {
          item.data.nguoiLike.push(taiKhoan);
          console.log(item.data.nguoiLike, "nguoi like");
          let soLike = item.data.soLike + 1;

          db.collection("soLikeBV").doc(item.id).update({
            nguoiLike: item.data.nguoiLike,
            soLike: soLike,
          });
        }
      });
    } else {
      console.log("props like", this.props);
      this.props.history.push("/login");
    }
  };
  renderSoLike = (id) => {
    let soLike = 0;
    this.state.soLikeArr.forEach((item) => {
      if (item.data.id === id) {
        soLike = item.data.soLike;
      }
    });
    return soLike;
  };
  renderCM = (id) => {
    let soCM = 0;
    this.state.posts.map((item) => {
      if (item.data.id === id) {
        soCM += 1;
      }
    });
    return soCM;
  };
  handleSrollTo = (name, to) => {
    scroller.scrollTo(name, {
      duration: 1000,
      smooth: true,
    });
  };

  render() {
    return (
      <>
        <section className="list-news">
          <div className="container ng-scope" id="detailNews">
            <div className="row">
              <div className="col-sm-12 title">
                <p className="ng-binding">
                  Dàn mỹ nhân trong thế giới điện ảnh của quái kiệt Christopher
                  Nolan
                </p>
                {/* ngIf: !isMobile */}
                <div className="author ng-binding ng-scope" ng-if="!isMobile">
                  STEVEN <span className="ng-binding" /> 21.12.21
                </div>
                {/* end ngIf: !isMobile */}
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 count">
                <div
                  className="wrapIcon like wrapIconLike news2__icon mx-0"
                  data-type={1}
                  data-id={7848}
                  ng-click="likeAction($event, news, keyCache)"
                >
                  {/* <img
                    className="iconFacebook postLike"
                    ng-src="app/assets/img/icons/like.png"
                    src="/images/like.png"
                  />
                  <span className="amount like ng-binding">0 LIKES</span>
                </div>
                <div className="wrapIcon wrapIconComment" ng-click="cmt()">
                  <img
                    className="iconFacebook"
                    src="/images/comment.png"
                    ng-click="cmt()"
                  /> */}
                  {(localStorage.getItem("userHome") ||
                    localStorage.getItem("userFace") ||
                    localStorage.getItem("userGoo")) &&
                  this.state.nguoiLikeFast &&
                  this.state.nguoiLikeFast.includes(taiKhoan) ? (
                    <button
                      className="text-primary mr-4"
                      onClick={() => this.likeIconBV("/news/dan-my-nhan")}
                    >
                      <ThumbUpIcon />
                    </button>
                  ) : (
                    <button
                      className="mr-4"
                      onClick={() => this.likeIconBV("/news/dan-my-nhan")}
                    >
                      <ThumbUpIcon />
                    </button>
                  )}
                  <span className="mr-5">
                    {this.renderSoLike("/news/dan-my-nhan")} LIKE
                  </span>
                  <button
                    onClick={() =>
                      this.handleSrollTo("scrollTo", "/news/dan-my-nhan")
                    }
                    className="mr-4"
                  >
                    <ChatBubbleIcon />
                  </button>
                  <span className="mr-5">
                    {this.renderCM("/news/dan-my-nhan")} BÌNH LUẬN
                  </span>
                  <ShareIcon className="shareIcon mr-4" />
                  <span className="amount share ng-binding">0 CHIA SẺ</span>
                  {/* <span className="amount ng-binding">0 BÌNH LUẬN</span> */}
                </div>
                <div className="wrapIcon wrapIconShare" ng-click="shareNews()">
                  {/* <span className="amount share ng-binding">0 CHIA SẺ</span> */}
                </div>
              </div>
            </div>
            <div className="row">
              <div
                className="col-sm-12 content ng-binding"
                ng-bind-html="newsContent"
              >
                <p>
                  Trong hơn 20 năm qua, Nolan đã thành công chiêu mộ và chỉ đạo
                  nhiều diễn viên nổi tiếng trong những tác phẩm kinh điển
                  như&nbsp;
                  <em>The Dark Knight, Inception, Interstellar, Dunkirk</em>
                  &nbsp;và sắp tới đây là&nbsp;<em>Tenet</em>...
                </p>
                <p>
                  Được cộng tác với Nolan là một vinh dự cho bất kỳ diễn viên
                  nào, và không ít lần thành công của dự án phim “cộp mác Nolan”
                  đã trở thành bệ phóng tên tuổi cho các gương mặt tiềm năng.
                  Bên cạnh những cái tên như Heath Ledger, Michael Cain,
                  Christian Bale, Tom Hardy..., có không ít mỹ nhân Hollywood đã
                  khẳng định tài năng của mình khi xuất hiện trong thế giới của
                  đạo diễn tỉ đô. Cùng điểm mặt những nhan sắc đã góp phần làm
                  nên thành công cho “vũ trụ điện ảnh Nolan” trước thềm bom
                  tấn&nbsp;
                  <strong>
                    <em>Tenet</em>
                  </strong>
                  &nbsp;ra rạp tháng 8 này.&nbsp;
                </p>
                <p>
                  <strong>1.&nbsp;Scarlett Johansson - The Prestige</strong>
                </p>
                <p style={{ textAlign: "center" }}>
                  <strong>
                    <img
                      alt
                      src="https://s3img.vcdn.vn/123phim/2020/08/Scarlett 3-21a283.jpg"
                      style={{ width: "90%" }}
                    />
                  </strong>
                </p>
                <p>
                  Trước khi làm mưa làm gió tại vũ trụ điện ảnh Marvel, Góa phụ
                  đen đã từng gây bao thương nhớ khi sánh bước bên “Wolverine”
                  Hugh Jackman và “Batman” Christian Bale trong&nbsp;
                  <em>The Prestige</em>&nbsp;(tạm dịch:&nbsp;
                  <em>Màn Cao Trào / Ảo Thuật Gia Đấu Trí</em>) – một trong
                  những bộ phim làm nên tên tuổi của đạo diễn Christopher Nolan.
                  Phim xoay quanh cuộc đấu trí nghẹt thở và màn so tài sống còn
                  của hai ảo thuật gia Alfred Borden (Christian Bale)
                  và&nbsp;Robert Angier (Hugh Jackman).&nbsp;
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    alt
                    src="https://s3img.vcdn.vn/123phim/2020/08/Scarlett 2-982acc.jpg"
                    style={{ width: "90%" }}
                  />
                </p>
                <p>
                  Ngay từ đầu, Christopher Nolan đã muốn Johansson đảm trách vai
                  diễn cô trợ lý xinh đẹp quyến rũ. Và bản thân Scarlett cũng bị
                  thuyết phục ngay lập tức khi đọc kịch bản. Vẻ kiều diễm của
                  “quả bom tóc vàng” lại càng thêm phần lôi cuốn trong&nbsp;
                  <em>The Prestige</em>&nbsp;nhờ bàn tay của nhà thiết kế trang
                  phục Joan Bergin.&nbsp;
                </p>
                <p>
                  <strong>
                    2.&nbsp;Anne Hathaway - The Dark Knight Rises và
                    Interstellar
                  </strong>
                </p>
                <p style={{ textAlign: "center" }}>
                  <strong>
                    <img
                      alt
                      src="https://s3img.vcdn.vn/123phim/2020/08/Anne Hatthaway 1-c4585a.jpg"
                      style={{ width: "90%" }}
                    />
                  </strong>
                </p>
                <p style={{ textAlign: "center" }}>
                  <em>
                    Anne Hatthaway sắm vai Catwoman/Selina Kyle trong&nbsp;
                    <strong>The Dark Knight Rise</strong>...
                  </em>
                </p>
                <p style={{ textAlign: "center" }}>
                  <em>
                    <img
                      alt
                      src="https://s3img.vcdn.vn/123phim/2020/08/Anne Hatthaway 2-b1302c.jpg"
                      style={{ width: "90%" }}
                    />
                  </em>
                </p>
                <p style={{ textAlign: "center" }}>
                  <em>
                    … và&nbsp; Tiến sĩ Amelia Brand trong&nbsp;
                    <strong>Interstellar</strong>
                  </em>
                </p>
                <p>
                  Anne Hathaway chia sẻ từng đến gặp Christopher Nolan mà cứ
                  đinh ninh rằng mình đến thử vai Harley Quinn. Người đẹp không
                  khỏi bật cười khi nhớ lại cô đã chọn một chiếc áo kỳ cục mà
                  không biết rằng vị đạo diễn đang tìm kiếm một người vào vai
                  Catwoman (Miêu Nữ) cho siêu phẩm&nbsp;
                  <em>The Dark Knight Rises</em>. Khi phim ra mắt, diễn xuất đầy
                  bí ẩn và ma mị của Hathaway nhận được rất nhiều sự yêu mến từ
                  phía khán giả.&nbsp;
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    alt
                    src="https://s3img.vcdn.vn/123phim/2020/08/Anne Hatthaway 3-fced08.jpg"
                    style={{ width: "90%" }}
                  />
                </p>
                <p>
                  Tinh thần làm việc chuyên nghiệp của nữ diễn viên trong&nbsp;
                  <em>Interstellar</em>&nbsp;tiếp tục chứng minh Nolan không
                  nhìn nhầm người. Chỉ riêng cảnh phim hai nhà du hành trong bộ
                  đồ phi hành gia đáp xuống hành tinh đầy nước đã khiến Hathaway
                  vật lộn trong bộ quần áo vừa nặng vừa cứng ngập trong bể nước
                  hàng giờ. Thay vì phàn nàn, Anne Hathaway đã tự động viên bản
                  thân và hoàn thành phân cảnh vốn có thể gây khó cho bất kỳ nam
                  diễn viên cường tráng nào.&nbsp;&nbsp;
                </p>
                <p>
                  <strong>
                    3.&nbsp;Marion Cotillard - Inception và The Dark Knight
                  </strong>
                </p>
                <p style={{ textAlign: "center" }}>
                  <strong>
                    <img
                      alt
                      src="https://s3img.vcdn.vn/123phim/2020/08/Marion-bfbaac.jpg"
                      style={{ width: "90%" }}
                    />
                  </strong>
                </p>
                <p style={{ textAlign: "center" }}>
                  <strong>
                    <img
                      alt
                      src="https://s3img.vcdn.vn/123phim/2020/08/Marion 2-2549d5.jpg"
                      style={{ width: "90%" }}
                    />
                  </strong>
                </p>
                <p style={{ textAlign: "center" }}>
                  <em>
                    Marion Cotillard sắm vai Mal trong&nbsp;
                    <strong>Inception&nbsp;</strong>
                  </em>
                  và Talia Al Ghul/Miranda Tate trong&nbsp;
                  <strong>
                    <em>The Dark Knight Rises.</em>
                  </strong>
                </p>
                <p>
                  Nhan sắc mặn mà đậm chất Pháp cùng diễn xuất ấn tượng của chủ
                  nhân tượng vàng Oscar đã được chứng minh qua hai tác phẩm của
                  Nolan. Trong&nbsp;<em>Inception</em>, cô vào vai Mal, vợ của
                  Dom - một đạo chích bậc thầy có khả năng xâm nhập vào cõi vô
                  thức của người khác thông qua giấc mơ và đánh cắp những bí mật
                  sâu kín của người đó. Mal là một người vợ mang nhiều hoài nghi
                  và nỗi đau. Cô ra đi nhưng vẫn sống mãi trong tiềm thức của
                  Dom như một nỗi đau day dứt đầy ám ảnh.&nbsp;
                </p>
                <p>
                  Với&nbsp;<em>The Dark Knight Rises</em>, Marion Cotillard thể
                  hiện một nữ doanh nhân thông minh và cực kỳ mạnh mẽ Miranda
                  Tate, đồng thời cũng vô cùng quỷ quyệt khi bị lật mặt trong
                  thân phận Talia Al Ghul, con gái của Ra's al Ghul, một trong
                  những kẻ thù nguy hiểm của Batman.&nbsp;
                </p>
                <p>
                  Nói về tài năng và sự chuyên nghiệp của Marion, Nolan chia
                  sẻ:&nbsp;
                  <em>
                    “Cô ấy quá xuất sắc để bạn không muốn phí hoài tài năng ấy.
                    Quả là một nữ siêu nhân”.
                  </em>
                </p>
                <p>
                  <strong>4.&nbsp;Elizabeth Debicki - Tenet</strong>
                </p>
                <p style={{ textAlign: "center" }}>
                  <strong>
                    <img
                      alt
                      src="https://s3img.vcdn.vn/123phim/2020/08/Elizabeth 3-dd6630.jpg"
                      style={{ width: "90%" }}
                    />
                  </strong>
                </p>
                <p>
                  Người đẹp 9x đến từ Australia có màn ra mắt thành công tại
                  Hollywood năm 2013 với&nbsp;<em>The Great Gatsby</em>. Sau đó,
                  cô liên tục xuất hiện trong nhiều tựa phim bom tấn như&nbsp;
                  <em>The Man from U.N.C.L.E</em>.,&nbsp;
                  <em>Guardians of the Galaxy Vol. 2</em>,&nbsp;
                  <em>The Cloverfield Paradox</em>&nbsp;hay gần đây nhất&nbsp;
                  <em>Widows</em>&nbsp;của đạo diễn Steve McQueen. Chiều cao ấn
                  tượng (1m9), mái tóc vàng, làn da trắng sứ cùng nhan sắc quyến
                  rũ của Debicki khiến cô nổi bật trước dàn diễn viên nữ tại
                  Hollywood.&nbsp;
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    alt
                    src="https://s3img.vcdn.vn/123phim/2020/08/Elizabeth 5-aa703d.jpg"
                    style={{ width: "90%" }}
                  />
                </p>
                <p>
                  Bên cạnh ngoại hình xuất sắc, Elizabeth cũng chứng minh được
                  mình không hề là một “bình hoa di động”. Cô từng hóa thân vào
                  nhân vật Alice trong&nbsp;<em>Widows</em>&nbsp;xuất sắc đến
                  nỗi đạo diễn Christopher Nolan hiểu nhầm Debicki là một người
                  Mỹ thực sự và suýt nữa đã từ chối trao cho cô vai diễn trong
                  bom tấn&nbsp;<em>Tenet</em>. Cho đến khi biết được Elizabeth
                  thử vai&nbsp; vị đạo diễn kiệt xuất ngay lập tức chọn cô cho
                  nhân vật mà ông cho là “phải toát lên được vẻ đẹp quý phái của
                  một bông hồng nước Anh trong siêu phẩm của mình”.&nbsp;
                </p>
                <p style={{ textAlign: "center" }}>
                  <img
                    alt
                    src="https://s3img.vcdn.vn/123phim/2020/08/Elizabeth 4-004637.jpg"
                    style={{ width: "90%" }}
                  />
                </p>
                <p>
                  <strong>
                    <em>Tenet</em>
                  </strong>
                  &nbsp;là bộ phim hành động đề tài điệp viên, theo chân một tổ
                  chức gián điệp bí ẩn nắm trong tay thứ vũ khí có khả năng thao
                  túng và nghịch đảo thời gian, thực thi sứ mệnh ngăn chặn Chiến
                  tranh Thế giới thứ III từ trước khi nó xảy ra.&nbsp;
                </p>
                <p>
                  Đây chắc chắn là bệ phóng cho tên tuổi của Debicki khi được
                  cộng tác với những cái tên đình đám nhất như đạo diễn
                  Christopher Nolan cùng loạt diễn viên Robert Pattinson,
                  Michael Caine hay John David Washington. Vai diễn mà cô thực
                  hiện cho tới nay vẫn là một ẩn số, nhưng chỉ cần nhìn sự thận
                  trọng mà Nolan khắc họa các nữ nhân trong phim của ông thì
                  người hâm mộ có thể yên tâm vào một tuyến nhân vật thú vị, đầy
                  chiều sâu mà Debicki đảm trách.&nbsp;
                </p>
                <p>
                  <strong>TENET</strong>&nbsp;được quay trên định dạng 70mm và
                  IMAX - dự kiến khởi chiếu tại Việt Nam với định dạng 2D và
                  IMAX 2D từ<strong>&nbsp;28.08.2020</strong>
                </p>
                <p>&nbsp;</p>
                <p style={{ textAlign: "right" }}>
                  <strong>STEVEN</strong>
                </p>
              </div>
            </div>
            {/* ngIf: news.author_name !== '' */}
            <div className="row" id="scrollTo">
              <div className="col-sm-12 line">
                <hr />
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <div id="detailReviewer">
                  <div className="row isFlex detailMainStyle">
                    <div
                      className="col-sm-12 col-xs-12 dadInputReviewer dadInputReviewerNews newDesign"
                      ng-click="openCommentBox()"
                    >
                      {/* <span className="imgReviewer">

                        <img
                          ng-if="user.user_id"
                          ng-src="https://graph.facebook.com/123455529585217/picture?type=square"
                          className="ng-scope"
                          src="https://graph.facebook.com/123455529585217/picture?type=square"
                        />
                        
                      </span>
                      <input
                        className="inputReviwer"
                        type="text"
                        placeholder="Bạn nghĩ gì về bài viết này?"
                        readOnly="readonly"
                      /> */}
                      <div className="row">
                        <div className="col-12 mx-auto">
                          <CommentBoxNews props={this.props} />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 line">
                <hr />
              </div>
            </div>
            <div className="news2">
              <div className="news2__item">
                <div className="news2__img">
                  <img src="/images/news2.png" alt="imagesNews" />
                </div>
                <div className="news2__info">
                  <h1>
                    Tiệc Trăng Máu chính thức cán mốc 100 tỷ chỉ sau 2 tuần công
                    chiếu
                  </h1>
                  <p>
                    Sau 2 tuần ra mắt, Tiệc Trăng Máu chính thức gia nhập câu
                    lạc bộ phim điện ảnh đạt 100 tỷ đồng doanh thu phòng vé. Dàn
                    ngôi sao “bạc tỷ” của phim cũng lần đầu tiên hội tụ đầy đủ
                    trong một khung hình để ăn mừng thành tích ấn tượng của tác
                    phẩm.
                  </p>
                  <div className="news2__icon">
                    <i className="fa fa-thumbs-up" />
                    <span>2</span>
                    <i className="fa fa-comment-alt" />
                    <span>0</span>
                  </div>
                </div>
              </div>
              <div className="news2__item">
                <div className="news2__img">
                  <img src="/images/mortal-kombat-1.png" alt="imagesNews" />
                </div>
                <div className="news2__info">
                  <h1>
                    [MORTAL KOMBAT: CUỘC CHIẾN SINH TỬ] - GỌI TÊN NHỮNG PHIM
                    ĐIỆN ẢNH NỔI TIẾNG ĐƯỢC CHUYỂN THỂ TỪ CÁC TỰA GAME ĐÌNH ĐÁM
                  </h1>
                  <p>
                    Bên cạnh những kịch bản gốc mới mẻ và đầy bất ngờ, Hollywood
                    cũng không thiếu những tác phẩm đình đám được chuyển thể từ
                    tiểu thuyết, phim hoạt hình, hay thậm chí là cả trò chơi
                    điện tử. Với ý tưởng thú vị cùng cốt truyện mang đậm tính
                    phiêu lưu, các tựa game đã trở thành nguồn cảm hứng độc đáo
                    cho các nhà làm phim. Không chỉ vậy, kể cả với các fan của
                    tựa game gốc, việc được thấy các nhân vật mình yêu thích
                    trên màn ảnh rộng cũng là một trải nghiệm không thể bỏ qua.
                    Hãy cùng điểm lại những bộ phim đình đám bậc nhất của
                    Hollywood được chuyển thể từ các trò chơi điện tử nhé.
                  </p>
                  <div className="news2__icon">
                    <i className="fa fa-thumbs-up" />
                    <span>1</span>
                    <i className="fa fa-comment-alt" />
                    <span>1</span>
                  </div>
                </div>
              </div>
              <div className="news2__item news2__item2">
                <div className="item2__row">
                  <div className="item2__info">
                    <div className="item2__img">
                      <img src="/images/matrix-1.jpg" alt="imagesNews" />
                    </div>
                    <div className="item2__detail">
                      <p>
                        Những nhân vật có thể quay trở lại trong The Matrix 4
                      </p>
                    </div>
                  </div>
                </div>
                <div className="item2__row">
                  <div className="item2__info">
                    <div className="item2__img">
                      <img src="/images/news6.png" alt="imagesNews" />
                    </div>
                    <div className="item2__detail">
                      <p>
                        Truy Cùng Giết Tận - Cuộc tái ngộ của hai 'ông hoàng
                        phòng vé' xứ Hàn
                      </p>
                    </div>
                  </div>
                </div>
                <div className="item2__row">
                  <div className="item2__info">
                    <div className="item2__img">
                      <img src="/images/news7.png" alt="imagesNews" />
                    </div>
                    <div className="item2__detail">
                      <p>
                        6 đạo diễn tỉ đô làm nên thành công của những bom tấn
                        đình đám nhất Hollywood
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="news__more">
              <button>XEM THÊM</button>
            </div>
            {/* ngIf: list_related.length > 4 */}
            {/* <div
              className="row isFlex text-center ViewMoreNews ng-scope"
              ng-if="list_related.length > 4"
            >
              <button
                id="btnNewsRelated"
                className="btnViewMore"
                ng-click="viewMoreNews()"
              >
                XEM THÊM
              </button>
            </div> */}
            {/* end ngIf: list_related.length > 4 */}
          </div>
        </section>
        <Footer />
      </>
    );
  }
}
