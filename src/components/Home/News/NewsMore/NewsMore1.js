import React, { Component } from "react";

export default class NewsMore1 extends Component {
  render() {
    return (
      <>
        <div className="NewsMore1">
          <div className="news1">
            <div className="news1__item">
              <img src="./images/review-1.png" alt="poster" />
              <h1>
                Review: Tàn Tích Quỷ Ám (Relic) - Ba thế hệ và mối liên kết
              </h1>
              <p>
                Điểm nhấn của phim kinh dị năm 2020 chính là Tàn Tích Quỷ Ám
              </p>
              <div className="news__icon">
                <i className="fa fa-thumbs-up" />
                <span>1</span>
                <i className="fa fa-comment-alt" />
                <span>0</span>
              </div>
            </div>
            <div className="news1__item">
              <img src="./images/review-2.png" alt="poster" />
              <h1>Review: Dinh Thự Oan Khuất (Ghost Of War)</h1>
              <p>
                Tuy là một bộ phim có chất lượng tốt, nhưng có vẻ Dinh Thự Oan
                Khuất vẫn chưa đủ để đem khán giả trở lại phòng vé!
              </p>
              <div className="news__icon">
                <i className="fa fa-thumbs-up" />
                <span>2</span>
                <i className="fa fa-comment-alt" />
                <span>0</span>
              </div>
            </div>
          </div>
          <div className="news2">
            <div className="news2__item">
              <div className="news2__img">
                <img src="./images/review-3.png" alt />
              </div>
              <div className="news2__info">
                <h1>‘BlacKkKlansman’ - cốc nước lạnh để người Mỹ thức tỉnh</h1>
                <p>
                  Tác phẩm nhận đề cử Phim truyện xuất sắc tại Oscar 2019 của
                  đạo diễn Spike Lee là một lát cắt nữa về nạn phân biệt chủng
                  tộc - nỗi đau gây nhức
                </p>
                <div className="news2__icon">
                  <i className="fa fa-thumbs-up" />
                  <span>2</span>
                  <i className="fa fa-comment-alt" />
                  <span>0</span>
                </div>
              </div>
            </div>
            <div className="news2__item">
              <div className="news2__img">
                <img src="./images/review-4.png" alt />
              </div>
              <div className="news2__info">
                <h1>American Sniper - Chính nghĩa hay phi nghĩa?</h1>
                <p>
                  American Sniper khắc họa một tay súng bắn tỉa “huyền thoại”
                  của Hải quân Mỹ với 4 lần tham chiến ở Trung Đông. Câu chuyện
                  phim chậm rãi đưa người xem qua từng thời khắc khác nhau của
                  Kyle, từ thửa nhỏ, thiếu niên, rồi gia nhập quân đội, rồi tham
                  chiến. Từng khoảnh khắc bắt đầu nhẹ nhàng như những nét thanh
                  trên trang giấy trắng và thêm một nét đậm mạnh mẽ ở phát súng
                  bắn tỉa đầu tiên.
                </p>
                <div className="news2__icon">
                  <i className="fa fa-thumbs-up" />
                  <span>1</span>
                  <i className="fa fa-comment-alt" />
                  <span>1</span>
                </div>
              </div>
            </div>
            <div className="news2__item news2__item2">
              <div className="item2__row">
                <div className="item2__info">
                  <div className="item2__img">
                    <img src="./images/review-5.png" alt />
                  </div>
                  <div className="item2__detail">
                    <p>Review: Spider-Man: Into The Spider-Vesre</p>
                  </div>
                </div>
              </div>
              <div className="item2__row">
                <div className="item2__info">
                  <div className="item2__img">
                    <img src="./images/review-6.jpg" alt />
                  </div>
                  <div className="item2__detail">
                    <p>
                      [Review] Siêu Vệ Sĩ Sợ Vợ - Giải cứu Tổng thống chưa bao
                      giờ lầy lội và hài hước đến thế
                    </p>
                  </div>
                </div>
              </div>
              <div className="item2__row">
                <div className="item2__info">
                  <div className="item2__img">
                    <img src="./images/review-7.jpg" alt />
                  </div>
                  <div className="item2__detail">
                    <p>
                      [Review] Bloodshot - Mở màn ấn tượng cho Vũ trụ Siêu anh
                      hùng Valiant
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
