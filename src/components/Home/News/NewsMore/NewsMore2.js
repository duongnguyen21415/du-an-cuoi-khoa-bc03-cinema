import React, { Component } from "react";
import { Link } from "react-router-dom";
import { scroller } from "react-scroll";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import ChatBubbleIcon from "@material-ui/icons/ChatBubble";
import db from "./../../../../homePages/LoginHome/firebase.js";
let taiKhoan =
  (localStorage.getItem("userGoo") &&
    JSON.parse(localStorage.getItem("userGoo")).uid) ||
  (localStorage.getItem("userFace") &&
    JSON.parse(localStorage.getItem("userFace")).uid) ||
  (localStorage.getItem("userHome") &&
    JSON.parse(localStorage.getItem("userHome")).taiKhoan);
export default class NewsMore2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      soLikeArr: [],
      soLike: 0,
      nguoiLikeFast: [],
      nguoiLikeLoki: [],
      nguoiLikeTiec: [],
      nguoiLikeKom: [],
    };
    this.newsMoreButton = React.createRef();
  }
  componentDidMount() {
    db.collection("news").onSnapshot((snapshot) => {
      let arr = snapshot.docs.map((doc) => ({ id: doc.id, data: doc.data() }));
      let soLikeArr;
      db.collection("soLikeBV").onSnapshot((snap) => {
        soLikeArr = snap.docs.map((doc) => ({ id: doc.id, data: doc.data() }));
        let nguoiLikeFast;
        let nguoiLikeLoki;
        let nguoiLikeTiec;
        let nguoiLikeKom;
        soLikeArr.forEach((item) => {
          if (item.data.id === "/news/fast9") {
            nguoiLikeFast = item.data.nguoiLike;
          } else if (item.data.id === "/news/loki") {
            nguoiLikeLoki = item.data.nguoiLike;
          } else if (item.data.id === "/news/tiec-trang-mau") {
            nguoiLikeTiec = item.data.nguoiLike;
          } else if (item.data.id === "/news/mortal-kombat") {
            nguoiLikeKom = item.data.nguoiLike;
          }
        });
        console.log("arr", soLikeArr, arr);
        this.setState({
          posts: arr,
          soLikeArr,
          nguoiLikeFast,
          nguoiLikeKom,
          nguoiLikeLoki,
          nguoiLikeTiec,
        });
      });
    });
  }

  likeIconBV = (id) => {
    const { props } = this.props;
    if (
      localStorage.getItem("userHome") ||
      localStorage.getItem("userFace") ||
      localStorage.getItem("userGoo")
    ) {
      this.state.soLikeArr.forEach((item) => {
        if (
          item.data.id == id &&
          item.data.nguoiLike &&
          item.data.nguoiLike.includes(taiKhoan)
        ) {
          let soLike = item.data.soLike - 1;
          item.data.nguoiLike.splice(item.data.nguoiLike.indexOf(taiKhoan, 1));
          console.log(item.data.nguoiLike, "nguoi like");
          db.collection("soLikeBV").doc(item.id).update({
            nguoiLike: item.data.nguoiLike,
            soLike,
          });
        } else if (
          item.data.id == id &&
          !(item.data.nguoiLike && item.data.nguoiLike.includes(taiKhoan))
        ) {
          item.data.nguoiLike.push(taiKhoan);
          console.log(item.data.nguoiLike, "nguoi like");
          let soLike = item.data.soLike + 1;

          db.collection("soLikeBV").doc(item.id).update({
            nguoiLike: item.data.nguoiLike,
            soLike: soLike,
          });
        }
      });
    } else {
      console.log("props like", this.props);
      props.history.push("/login");
    }
  };
  renderSoLike = (id) => {
    let soLike = 0;
    this.state.soLikeArr.forEach((item) => {
      if (item.data.id === id) {
        soLike = item.data.soLike;
      }
    });
    return soLike;
  };
  renderCM = (id) => {
    let soCM = 0;
    this.state.posts.map((item) => {
      if (item.data.id === id) {
        soCM += 1;
      }
    });
    return soCM;
  };
  handleSrollTo = (name, to) => {
    console.log(to);

    this.props.props.history.push(to);
    setTimeout(() => {
      scroller.scrollTo(name, {
        duration: 1000,
        smooth: true,
      });
    }, 1000);
  };
  render() {
    return (
      <>
        <div className="NewsMore2">
          <div className="news1">
            <div className="news1__item">
              <Link to="/news/fast9">
                <img src="./images/fast9-1.jpg" alt="poster" />
              </Link>
              <Link to="/news/fast9">
                <h1>
                  Lộ diện phản diện mới của series Fast and Furious, 'người đặc
                  biệt' đã trở lại!
                </h1>
              </Link>
              <p>
                Sau phần ngoại truyện Fast & Furious: Hobbs & Shaw cực kì mãn
                nhãn, điều mà fan hâm mộ của thương hiệu tốc độ mong ngóng nhất
                chính là sự ra mắt của phần phim thứ 9. Mới đây, bom tấn "hàng
                khủng" Fast & Furious 9 đã bất ngờ trình làng trailer đầy kịch
                tính và căng não tột độ với hàng loạt bất ngờ này đến cú sốc
                khác.
              </p>
              <div className="news__icon">
                {(localStorage.getItem("userHome") ||
                  localStorage.getItem("userFace") ||
                  localStorage.getItem("userGoo")) &&
                this.state.nguoiLikeFast &&
                this.state.nguoiLikeFast.includes(taiKhoan) ? (
                  <button
                    className="text-primary"
                    onClick={() => this.likeIconBV("/news/fast9")}
                  >
                    <ThumbUpIcon />
                  </button>
                ) : (
                  <button onClick={() => this.likeIconBV("/news/fast9")}>
                    <ThumbUpIcon />
                  </button>
                )}
                <span>{this.renderSoLike("/news/fast9")}</span>
                <button
                  onClick={() => this.handleSrollTo("scrollTo", "/news/fast9")}
                >
                  <ChatBubbleIcon />
                </button>
                <span>{this.renderCM("/news/fast9")}</span>
              </div>
            </div>
            <div className="news1__item">
              <Link to="/news/loki">
                <img src="./images/loki-1.jpg" alt="poster" />
              </Link>
              <Link to="/news/loki">
                <h1>Bí mật đằng sau phiên bản nữ của Loki</h1>
              </Link>
              <p>
                Mini-series “Loki” đã hé lộ danh tính kẻ khủng bố dòng thời gian
                mà TVA đang truy đuổi. Xung quanh thân thế của nhân vật là nhiều
                bí mật chưa được giải đáp.
              </p>
              <div className="news__icon">
                {(localStorage.getItem("userHome") ||
                  localStorage.getItem("userFace") ||
                  localStorage.getItem("userGoo")) &&
                this.state.nguoiLikeLoki &&
                this.state.nguoiLikeLoki.includes(taiKhoan) ? (
                  <button
                    className="text-primary"
                    onClick={() => this.likeIconBV("/news/loki")}
                  >
                    <ThumbUpIcon />
                  </button>
                ) : (
                  <button onClick={() => this.likeIconBV("/news/loki")}>
                    <ThumbUpIcon />
                  </button>
                )}
                <span>{this.renderSoLike("/news/loki")}</span>
                <button
                  onClick={() => this.handleSrollTo("scrollTo", "/news/loki")}
                >
                  <ChatBubbleIcon />
                </button>
                <span>{this.renderCM("/news/loki")}</span>
              </div>
            </div>
          </div>
          <div className="news2">
            <div className="news2__item">
              <div className="news2__img">
                <Link to="/news/tiec-trang-mau">
                  <img src="./images/news2.png" alt="imagesNews" />
                </Link>
              </div>
              <div className="news2__info">
                <Link to="/news/tiec-trang-mau">
                  <h1>
                    Tiệc Trăng Máu chính thức cán mốc 100 tỷ chỉ sau 2 tuần công
                    chiếu
                  </h1>
                </Link>
                <p>
                  Sau 2 tuần ra mắt, Tiệc Trăng Máu chính thức gia nhập câu lạc
                  bộ phim điện ảnh đạt 100 tỷ đồng doanh thu phòng vé. Dàn ngôi
                  sao “bạc tỷ” của phim cũng lần đầu tiên hội tụ đầy đủ trong
                  một khung hình để ăn mừng thành tích ấn tượng của tác phẩm.
                </p>
                <div className="news2__icon">
                  {(localStorage.getItem("userHome") ||
                    localStorage.getItem("userFace") ||
                    localStorage.getItem("userGoo")) &&
                  this.state.nguoiLikeTiec &&
                  this.state.nguoiLikeTiec.includes(taiKhoan) ? (
                    <button
                      className="text-primary"
                      onClick={() => this.likeIconBV("/news/tiec-trang-mau")}
                    >
                      <ThumbUpIcon />
                    </button>
                  ) : (
                    <button
                      onClick={() => this.likeIconBV("/news/tiec-trang-mau")}
                    >
                      <ThumbUpIcon />
                    </button>
                  )}
                  <span>{this.renderSoLike("/news/tiec-trang-mau")}</span>
                  <button
                    onClick={() =>
                      this.handleSrollTo("scrollTo", "/news/tiec-trang-mau")
                    }
                  >
                    <ChatBubbleIcon />
                  </button>
                  <span>{this.renderCM("/news/tiec-trang-mau")}</span>
                </div>
              </div>
            </div>
            <div className="news2__item">
              <div className="news2__img">
                <Link to="/news/mortal-kombat">
                  <img src="./images/mortal-kombat-1.png" alt="imagesNews" />
                </Link>
              </div>
              <div className="news2__info">
                <Link to="/news/mortal-kombat">
                  <h1>
                    [MORTAL KOMBAT: CUỘC CHIẾN SINH TỬ] - GỌI TÊN NHỮNG PHIM
                    ĐIỆN ẢNH NỔI TIẾNG ĐƯỢC CHUYỂN THỂ TỪ CÁC TỰA GAME ĐÌNH ĐÁM
                  </h1>
                </Link>
                <p>
                  Bên cạnh những kịch bản gốc mới mẻ và đầy bất ngờ, Hollywood
                  cũng không thiếu những tác phẩm đình đám được chuyển thể từ
                  tiểu thuyết, phim hoạt hình, hay thậm chí là cả trò chơi điện
                  tử. Với ý tưởng thú vị cùng cốt truyện mang đậm tính phiêu
                  lưu, các tựa game đã trở thành nguồn cảm hứng độc đáo cho các
                  nhà làm phim. Không chỉ vậy, kể cả với các fan của tựa game
                  gốc, việc được thấy các nhân vật mình yêu thích trên màn ảnh
                  rộng cũng là một trải nghiệm không thể bỏ qua. Hãy cùng điểm
                  lại những bộ phim đình đám bậc nhất của Hollywood được chuyển
                  thể từ các trò chơi điện tử nhé.
                </p>
                <div className="news2__icon">
                  {(localStorage.getItem("userHome") ||
                    localStorage.getItem("userFace") ||
                    localStorage.getItem("userGoo")) &&
                  this.state.nguoiLikeKom &&
                  this.state.nguoiLikeKom.includes(taiKhoan) ? (
                    <button
                      className="text-primary"
                      onClick={() => this.likeIconBV("/news/mortal-kombat")}
                    >
                      <ThumbUpIcon />
                    </button>
                  ) : (
                    <button
                      onClick={() => this.likeIconBV("/news/mortal-kombat")}
                    >
                      <ThumbUpIcon />
                    </button>
                  )}
                  <span>{this.renderSoLike("/news/mortal-kombat")}</span>
                  <button
                    onClick={() =>
                      this.handleSrollTo("scrollTo", "/news/mortal-kombat")
                    }
                  >
                    <ChatBubbleIcon />
                  </button>
                  <span>{this.renderCM("/news/mortal-kombat")}</span>
                </div>
              </div>
            </div>
            <div className="news2__item news2__item2">
              <div className="item2__row">
                <div className="item2__info">
                  <div className="item2__img">
                    <Link to="/news/dan-my-nhan">
                      <img src="./images/news5.png" alt="imagesNews" />
                    </Link>
                  </div>
                  <div className="item2__detail">
                    <Link to="/news/dan-my-nhan">
                      <p>
                        Dàn mỹ nhân trong thế giới điện ảnh của quái kiệt
                        Christopher Nolan
                      </p>
                    </Link>
                  </div>
                </div>
              </div>
              <div className="item2__row">
                <div className="item2__info">
                  <div className="item2__img">
                    <Link to="/news/truy-cung-giet-tan">
                      <img src="./images/news6.png" alt="imagesNews" />
                    </Link>
                  </div>
                  <div className="item2__detail">
                    <Link to="/news/truy-cung-giet-tan">
                      <p>
                        Truy Cùng Giết Tận - Cuộc tái ngộ của hai 'ông hoàng
                        phòng vé' xứ Hàn
                      </p>
                    </Link>
                  </div>
                </div>
              </div>
              <div className="item2__row">
                <div className="item2__info">
                  <div className="item2__img">
                    <Link to="/news/dao-dien-ty-usd">
                      <img src="./images/news7.png" alt="imagesNews" />
                    </Link>
                  </div>
                  <div className="item2__detail">
                    <Link to="/news/dao-dien-ty-usd">
                      <p>
                        6 đạo diễn tỉ đô làm nên thành công của những bom tấn
                        đình đám nhất Hollywood
                      </p>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
