import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Loader from "../../Loader/Loader";
import { actModalHomeSuccess } from "../Modal/Modules/action";
import { fetchListMovieHome } from "../ModulesListMovie/action";
import { fetchTheaterToolsHome } from "./Tools/ModulesTools/action";
import TheaterTools from "./Tools/TheaterTools";
import "./../../../assets/sass/layouts/slider.scss";
class Slides extends Component {
  componentDidMount() {
    console.log("slides", this.props);
    this.props.getSlideMovies();
  }

  renderSlidesMovie = () => {
    const { loading, data } = this.props;
    if (loading) return <Loader />;
    if (data) {
      let listMovie = data.sort((nextItem, item) => {
        let nextItemMovie = nextItem.ngayKhoiChieu.toLowerCase();
        let itemMovie = item.ngayKhoiChieu.toLocaleLowerCase();
        if (nextItemMovie > itemMovie) {
          return -1;
        }
        if (nextItemMovie < itemMovie) {
          return 1;
        }
        return 1;
      });
      let slidesMovie = [];
      slidesMovie = listMovie.slice(3, 6);
      console.log(slidesMovie);
      return (
        <>
          <div className="carousel-item active">
            <Link to={`/detail-movie/${slidesMovie[0].maPhim}`}>
              <img
                className="img-slides"
                src={slidesMovie[0].hinhAnh}
                alt="..."
              />
              <div className="shadow"></div>
            </Link>
            <button
              type="button"
              className="play__video"
              data-toggle="modal"
              data-target={"#" + slidesMovie[0].biDanh}
              onClick={() => this.props.getModalItem(slidesMovie[0])}
            >
              <img src="./images/play-video.png" alt />
            </button>
          </div>
          <div className="carousel-item">
            <Link to={`/detail-movie/${slidesMovie[1].maPhim}`}>
              <img
                className="img-slides"
                src={slidesMovie[1].hinhAnh}
                alt="..."
              />
              <div className="shadow"></div>
            </Link>
            <button
              type="button"
              className="play__video"
              data-toggle="modal"
              data-target={"#" + slidesMovie[1].biDanh}
              onClick={() => this.props.getModalItem(slidesMovie[1])}
            >
              <img src="./images/play-video.png" alt />
            </button>
          </div>
          <div className="carousel-item">
            <Link to={`/detail-movie/${slidesMovie[2].maPhim}`}>
              <img
                className="img-slides"
                src={slidesMovie[2].hinhAnh}
                alt="..."
              />
              <div className="shadow"></div>
            </Link>
            <button
              type="button"
              className="play__video"
              data-toggle="modal"
              data-target={"#" + slidesMovie[2].biDanh}
              onClick={() => this.props.getModalItem(slidesMovie[2])}
            >
              <img src="./images/play-video.png" alt />
            </button>
          </div>
        </>
      );
    }
  };

  renderMovieTools = () => {
    const { loading, data } = this.props;
    if (loading) return <Loader />;
    if (data) {
      console.log(data);
      return data.map((movie) => {
        return (
          <>
            <a
              key={movie.maPhim}
              className="dropdown-item"
              href="#"
              onClick={this.handlechosenMovie}
            >
              {movie.tenPhim}
            </a>
          </>
        );
      });
    }
  };

  handlechosenMovie = (event) => {
    event.preventDefault();
    document.getElementById("Phim").innerHTML = event.target.childNodes[0].data;
    document.getElementById("Rap").innerHTML = "Rạp";
    document.getElementById("ngayXem").innerHTML = "Ngày Xem";
    document.getElementById("suatChieu").innerHTML = "Suất Chiếu";
    this.fetchTheaterTools();
  };

  fetchTheaterTools = () => {
    const { data } = this.props;
    if (data) {
      let tenPhim = document.getElementById("Phim").innerHTML;
      let index = data.findIndex((movie) => movie.tenPhim === tenPhim);
      console.log(index);
      this.props.getTheaterToolsMovie(data[index].maPhim);
    }
  };

  render() {
    return (
      <section className="slides">
        <div
          id="carouselExampleIndicators"
          className="carousel slide"
          data-ride="carousel"
        >
          <ol className="carousel-indicators">
            <li
              data-target="#carouselExampleIndicators"
              data-slide-to={0}
              className="active"
            />
            <li data-target="#carouselExampleIndicators" data-slide-to={1} />
            <li data-target="#carouselExampleIndicators" data-slide-to={2} />
          </ol>
          <div className="carousel-inner">{this.renderSlidesMovie()}</div>
          <a
            className="carousel-control-prev icon__slides"
            href="#carouselExampleIndicators"
            role="button"
            data-slide="prev"
          >
            <img src="./images/back-session.png" alt />
            <span className="sr-only">Previous</span>
          </a>
          <a
            className="carousel-control-next icon__slides"
            href="#carouselExampleIndicators"
            role="button"
            data-slide="next"
          >
            <img src="./images/next-session.png" alt />
            <span className="sr-only">Next</span>
          </a>
        </div>
        <div className="select__menu">
          <div className="select__film">
            <div
              className="btn"
              id="Phim"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              Phim
            </div>
            <div className="dropdown-menu" aria-labelledby="Phim">
              {this.renderMovieTools()}
            </div>
          </div>
          <TheaterTools />
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.listMovieHomeReducer.loading,
    data: state.listMovieHomeReducer.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getSlideMovies: () => {
      dispatch(fetchListMovieHome());
    },
    getModalItem: (movie) => {
      dispatch(actModalHomeSuccess(movie));
    },
    getTheaterToolsMovie: (id) => {
      dispatch(fetchTheaterToolsHome(id));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Slides);
