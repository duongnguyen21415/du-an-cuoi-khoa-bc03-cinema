import React, { Component } from "react";
import { connect } from "react-redux";
import Loader from "../../../Loader/Loader";
import { actShowTimesToolsSuccess } from "./ModulesTools/action";
import DateTools from "./DateTools";

class TheaterTools extends Component {
  renderTheaterMovie = () => {
    const { loading, data } = this.props;
    if (!data) return <p>Hãy chọn Phim</p>;
    if (loading) return <Loader />;
    console.log(data);
    if (data && data.heThongRapChieu[0] !== undefined) {
      return data.heThongRapChieu.map((item) =>
        item.cumRapChieu.map((rap) => (
          <a key={rap.maCumRap} className="dropdown-item" href="#">
            {rap.tenCumRap}
          </a>
        ))
      );
    }
    return <p>Hiện chưa có lịch chiếu phim này...</p>;
  };

  handleChosenTheater = (event) => {
    event.preventDefault();
    document.getElementById("Rap").innerHTML = event.target.childNodes[0].data;
    document.getElementById("ngayXem").innerHTML = "Ngày Xem";
    document.getElementById("suatChieu").innerHTML = "Suất Chiếu";
    this.fetchShowTimeTools();
  };

  fetchShowTimeTools = () => {
    const { data } = this.props;
    if (data && data.heThongRapChieu[0] !== undefined) {
      let tenRap = document.getElementById("Rap").innerHTML;
      let indexArray = data.heThongRapChieu.map((item) => {
        return item.cumRapChieu.findIndex(
          (cumRap) => cumRap.tenCumRap === tenRap
        );
      });
      let index = indexArray.filter((item) => item > -1);
      let indexCumRap = +index;
      let indexHeThongRap = data.heThongRapChieu.findIndex((item) => {
        if (item.cumRapChieu[indexCumRap]) {
          return item.cumRapChieu[indexCumRap].tenCumRap === tenRap;
        }
        return null;
      });
      console.log(indexArray, indexHeThongRap, indexCumRap);
      if (data.heThongRapChieu[indexHeThongRap]) {
        this.props.getShowTimesTools(
          data.heThongRapChieu[indexHeThongRap].cumRapChieu[indexCumRap]
            .lichChieuPhim
        );
      }
    }
  };

  render() {
    return (
      <>
        <div className="btn-group">
          <div className="dropdown detail__menu rap">
            <div
              className="btn"
              id="Rap"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              Rạp
            </div>
            <div
              className="dropdown-menu"
              aria-labelledby="Rap"
              onClick={this.handleChosenTheater}
            >
              {this.renderTheaterMovie()}
            </div>
          </div>
          <DateTools />
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.theaterToolsHomeReducer.loading,
    data: state.theaterToolsHomeReducer.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getShowTimesTools: (lichChieuPhim) => {
      dispatch(actShowTimesToolsSuccess(lichChieuPhim));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TheaterTools);
