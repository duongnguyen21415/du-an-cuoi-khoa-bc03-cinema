import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

class TimesTools extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: null,
    };
  }

  handleChosenDate = (event) => {
    event.preventDefault();
    document.getElementById("suatChieu").innerHTML =
      event.target.childNodes[0].data;
  };

  renderTimesTools = () => {
    const { data } = this.props;
    if (data) {
      console.log(data);
      return data.map((item) => {
        return (
          <a key={item.maLichChieu} className="dropdown-item" href="#">
            {new Date(item.ngayChieuGioChieu).toLocaleTimeString()}
          </a>
        );
      });
    }
    return <p> Hãy chọn Ngày xem</p>;
  };

  getBooking = () => {
    if (
      document.getElementById("Rap").innerHTML !== "Rạp" &&
      document.getElementById("Rap").innerHTML !==
        "Hiện chưa có lịch chiếu phim này..." &&
      document.getElementById("Phim").innerHTML !== "Phim" &&
      document.getElementById("ngayXem").innerHTML !== "Ngày Xem" &&
      document.getElementById("suatChieu").innerHTML !== "Suất Chiếu" &&
      document.getElementById("suatChieu").innerHTML !==
        "Hiện chưa có lịch chiếu phim này..."
    ) {
      let hoursChosen = document.getElementById("suatChieu").innerHTML;
      const { data } = this.props;
      let index = data.findIndex(
        (item) =>
          new Date(item.ngayChieuGioChieu).toLocaleTimeString() === hoursChosen
      );
      this.setState({
        redirect: `/booking/${data[index].maLichChieu}`,
      });
    } else if (
      document.getElementById("Rap").innerHTML ===
      "Hiện chưa có lịch chiếu phim này..."
    ) {
      alert("Phim bạn chọn chưa có lịch chiếu. Vui lòng chọn phim khác!");
    } else {
      alert(
        "vui lòng chọn đầy đủ thông tin: Phim, Rạp, Ngày xem và Suất chiếu"
      );
    }
  };

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />;
    }
    return (
      <>
        <div className="dropdown detail__menu">
          <div
            className="btn"
            id="suatChieu"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Suất Chiếu
          </div>
          <div
            className="dropdown-menu"
            aria-labelledby="suatChieu"
            onClick={this.handleChosenDate}
          >
            {this.renderTimesTools()}
          </div>
        </div>
        <div className="book__tickets detail__menu">
          <button onClick={this.getBooking}>MUA VÉ NGAY</button>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.theaterToolsHomeReducer.hoursTools,
  };
};

export default connect(mapStateToProps, null)(TimesTools);
