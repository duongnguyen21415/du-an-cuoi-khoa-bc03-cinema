import React, { Component } from "react";
import { connect } from "react-redux";
import { actHoursToolsSuccess } from "./ModulesTools/action";
import TimesTools from "./TimesTools";

class DateTools extends Component {
  conversionDate = (day) => {
    let ngayChieu = new Date(day);
    let ngay = ngayChieu.getDate();
    let thang = ngayChieu.getMonth() + 1;
    let nam = ngayChieu.getFullYear();
    if (String(ngay).length < 2) {
      ngay = "0" + ngay;
    }
    if (String(thang).length < 2) {
      thang = "0" + thang;
    }
    return ngay + "." + thang + "." + nam;
  };

  renderDateTools = () => {
    const { data } = this.props;
    if (!data) return <p> Hãy chọn Rạp</p>;
    if (data) {
      return data.map((item) => {
        return (
          <a key={item.maLichChieu} className="dropdown-item" href="#">
            {this.conversionDate(item.ngayChieuGioChieu)}
          </a>
        );
      });
    }
    return <p>Hiện chưa có lịch chiếu phim này...</p>;
  };

  handleChosenDate = (event) => {
    event.preventDefault();
    document.getElementById("ngayXem").innerHTML =
      event.target.childNodes[0].data;
    document.getElementById("suatChieu").innerHTML = "Suất Chiếu";
    this.fetchHoursTools();
  };

  fetchHoursTools = () => {
    const { data } = this.props;
    if (data) {
      let ngayXem = document.getElementById("ngayXem").innerHTML;
      let dateArray = data.filter(
        (item) => this.conversionDate(item.ngayChieuGioChieu) === ngayXem
      );
      console.log(dateArray);
      this.props.getHoursTools(dateArray);
    }
  };

  render() {
    return (
      <>
        <div className="dropdown detail__menu">
          <div
            className="btn"
            id="ngayXem"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Ngày Xem
          </div>
          <div
            className="dropdown-menu"
            aria-labelledby="ngayXem"
            onClick={this.handleChosenDate}
          >
            {this.renderDateTools()}
          </div>
        </div>
        <TimesTools />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.theaterToolsHomeReducer.showTimesTools,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getHoursTools: (hoursTools) => {
      dispatch(actHoursToolsSuccess(hoursTools));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DateTools);
