import * as ActionType from "./constants";
import axios from "axios";

export const fetchTheaterToolsHome = (id) => {
  return (dispatch) => {
    dispatch(actTheaterToolsHomeRequest());
    axios({
      url: `https://movie0706.cybersoft.edu.vn/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`,
      method: "GET",
    })
      .then((res) => {
        console.log("data tong trong theater", res.data);
        let rapKhong = res.data.heThongRapChieu.filter((item) => {
          let phimKhong = item.cumRapChieu.filter((phim) => {
            let data = phim.lichChieuPhim.filter((lich) => {
              return new Date(lich.ngayChieuGioChieu) >= new Date();
            });
            phim.lichChieuPhim = data;
            return phim.lichChieuPhim.length !== 0;
          });
          item.cumRapChieu = phimKhong;
          return item.cumRapChieu.length !== 0;
        });
        res.data.heThongRapChieu = rapKhong;
        console.log("sau filter", res.data);
        dispatch(actTheaterToolsHomeSuccess(res.data));
      })
      .catch((err) => {
        dispatch(actTheaterToolsHomeFail(err));
      });
  };
};

const actTheaterToolsHomeRequest = () => {
  return {
    type: ActionType.THEATER_TOOLS_HOME_REQUEST,
  };
};

const actTheaterToolsHomeSuccess = (data) => {
  return {
    type: ActionType.THEATER_TOOLS_HOME_SUCCESS,
    payload: data,
  };
};

const actTheaterToolsHomeFail = (err) => {
  return {
    type: ActionType.THEATER_TOOLS_HOME_FAIL,
    payload: err,
  };
};

export const actShowTimesToolsSuccess = (data) => {
  return {
    type: ActionType.SHOWTIMES_TOOLS_HOME_SUCCESS,
    payload: data,
  };
};

export const actHoursToolsSuccess = (data) => {
  return {
    type: ActionType.HOURS_TOOLS_HOME_SUCCESS,
    payload: data,
  };
};

export const actTheaterClusterInDetailMovie = (data) => {
  return {
    type: ActionType.THEATER_CLUSTER_IN_DETAIL_MOVIE,
    payload: data,
  };
};

export const actTheaterShowTimesInDetailMovie = (data) => {
  return {
    type: ActionType.THEATER_SHOWTIMES_IN_DETAIL_MOVIE,
    payload: data,
  };
};
