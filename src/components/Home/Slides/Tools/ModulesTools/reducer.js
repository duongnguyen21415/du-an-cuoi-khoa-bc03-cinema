import * as ActionType from "./constants";

let initialState = {
  loading: false,
  data: null,
  err: null,
  showTimesTools: null,
  hoursTools: null,
  theaterClusterInDetailMovie: null,
  theaterShowTimesInDetailMovie: null,
};

const theaterToolsHomeReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.THEATER_TOOLS_HOME_REQUEST:
      state.loading = true;
      state.data = null;
      state.err = null;
      return { ...state };
    case ActionType.THEATER_TOOLS_HOME_SUCCESS:
      console.log(action.payload, "scoob");
      state.loading = false;
      state.data = action.payload;
      state.err = null;
      return { ...state };
    case ActionType.THEATER_TOOLS_HOME_FAIL:
      state.loading = false;
      state.data = null;
      state.err = action.payload;
      return { ...state };
    case ActionType.SHOWTIMES_TOOLS_HOME_SUCCESS:
      state.showTimesTools = action.payload;
      return { ...state };
    case ActionType.HOURS_TOOLS_HOME_SUCCESS:
      state.hoursTools = action.payload;
      return { ...state };
    case ActionType.THEATER_CLUSTER_IN_DETAIL_MOVIE:
      state.theaterClusterInDetailMovie = action.payload;
      return { ...state };
    case ActionType.THEATER_SHOWTIMES_IN_DETAIL_MOVIE:
      state.theaterShowTimesInDetailMovie = action.payload;
      return { ...state };
    default:
      return { ...state };
  }
};

export default theaterToolsHomeReducer;
