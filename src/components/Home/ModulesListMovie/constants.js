export const LIST_MOVIE_HOME_REQUEST =
  "@listMovieHomeReducer/LIST_MOVIE_HOME_REQUEST";
export const LIST_MOVIE_HOME_SUCCESS =
  "@listMovieHomeReducer/LIST_MOVIE_HOME_SUCCESS";
export const LIST_MOVIE_HOME_FAIL =
  "@listMovieHomeReducer/LIST_MOVIE_HOME_FAIL";
