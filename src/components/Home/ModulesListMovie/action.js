import * as ActionType from "./constants";
import axios from "axios";

export const fetchListMovieHome = () => {
  return (dispatch) => {
    dispatch(actListMovieHomeRequest());
    axios({
      url: "https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhimTheoNgay?maNhom=GP09&soTrang=1&soPhanTuTrenTrang=30&tuNgay=01%2F06%2F2020&denNgay=01%2F03%2F2021",
      method: "GET",
    })
      .then((res) => {
        console.log("data trong slide nè", res.data);
        dispatch(actListMovieHomeSuccess(res.data));
      })
      .catch((err) => {
        dispatch(actListMovieHomeFail(err));
      });
  };
};

const actListMovieHomeRequest = () => {
  return {
    type: ActionType.LIST_MOVIE_HOME_REQUEST,
  };
};

const actListMovieHomeSuccess = (data) => {
  return {
    type: ActionType.LIST_MOVIE_HOME_SUCCESS,
    payload: data,
  };
};

const actListMovieHomeFail = (err) => {
  return {
    type: ActionType.LIST_MOVIE_HOME_FAIL,
    payload: err,
  };
};
