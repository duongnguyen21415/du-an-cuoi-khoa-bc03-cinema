import React, { Component } from "react";
import { connect } from "react-redux";

class Modal extends Component {
  handleVideo = () => {
    console.log(document.getElementsByClassName(this.props.movie.biDanh));
    document.getElementsByClassName(this.props.movie.biDanh)[0].src =
      document.getElementsByClassName(this.props.movie.biDanh)[0].src;
  };

  render() {
    const { movie } = this.props;
    console.log(movie);
    return movie ? (
      <div
        className="modal"
        id={movie.biDanh}
        tabIndex={-1}
        onClick={this.handleVideo}
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-body">
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.handleVideo}
              >
                <img src="/images/close.png" alt />
              </button>
              <iframe
                className={movie.biDanh}
                width={800}
                height={500}
                src={movie.trailer}
                frameBorder={0}
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
              />
            </div>
          </div>
        </div>
      </div>
    ) : (
      <></>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.modalHomeReducer.loading,
    movie: state.modalHomeReducer.modal,
  };
};

export default connect(mapStateToProps, null)(Modal);
