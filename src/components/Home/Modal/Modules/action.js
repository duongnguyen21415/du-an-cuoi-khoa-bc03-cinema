import * as ActionType from "./constants";

export const actModalHomeSuccess = (modal) => {
  return {
    type: ActionType.MODAL_HOME_SUCCESS,
    payload: modal,
  };
};
