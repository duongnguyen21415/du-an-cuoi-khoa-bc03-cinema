import * as ActionType from "./constants";

const initialState = {
  loading: false,
  modal: null,
  err: null,
};

const modalHomeReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.MODAL_HOME_SUCCESS:
      state.loading = false;
      state.modal = action.payload;
      state.err = null;
      return { ...state };
    default:
      return { ...state };
  }
};

export default modalHomeReducer;
