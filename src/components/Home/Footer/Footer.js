import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <footer className="footer">
        <div className="container">
          <div className="footer__info">
            <div className="info__item">
              <h1>D-L</h1>
              <div className="info__rule">
                <div className="rule__item">
                  <p>
                    <a href="#">FAQ</a>
                  </p>
                  <p>
                    <a href="#">Brand Guidelines</a>
                  </p>
                </div>
                <div className="rule__item">
                  <p>
                    <a href="#">Thỏa thuận sử dụng</a>
                  </p>
                  <p>
                    <a href="#">Chính sách bảo mật</a>
                  </p>
                </div>
              </div>
            </div>
            <div className="info__item info__partner">
              <h1>ĐỐI TÁC</h1>
              <div className=" partner__img">
                <a href="https://www.bhdstar.vn/" target="_blank">
                  <img src="/images/logo1.png" alt />
                </a>
                <a href="http://cinestar.com.vn/" target="_blank">
                  <img src="/images/logo2.png" alt />
                </a>
                <a href="https://www.cgv.vn/" target="_blank">
                  <img src="/images/logo-cgv.png" alt />
                </a>
                <a href="https://www.megagscinemas.vn/" target="_blank">
                  <img src="/images/logo4.png" alt />
                </a>
                <a
                  href="https://lottecinemavn.com/LCHS/index.aspx"
                  target="_blank"
                >
                  <img src="/images/logo6.png" alt />
                </a>
              </div>
            </div>
            <div className="info__item info__connect">
              <div className="connect__item">
                <h1>MOBILE APP</h1>
                <a
                  href="#"
                  onClick={(e) => {
                    e.preventDefault();
                    alert(
                      "Tính năng đang được xây dựng. Vui lòng quay lại sau"
                    );
                  }}
                >
                  <img src="/images/apple-logo.png" alt />
                </a>
                <a
                  href="#"
                  onClick={(e) => {
                    e.preventDefault();
                    alert(
                      "Tính năng đang được xây dựng. Vui lòng quay lại sau"
                    );
                  }}
                >
                  <img src="/images/android-logo.png" alt />
                </a>
              </div>
              <div className="connect__item">
                <h1>SOCIAL</h1>
                <a href="https://www.facebook.com/" target="_blank">
                  <img src="/images/facebook-logo.png" alt />
                </a>
                <a href="https://zalo.me/pc" target="_blank">
                  <img src="/images/zalo-logo.png" alt />
                </a>
              </div>
            </div>
          </div>
          <div className="footer__mobile-info">
            <div className="rule__item">
              <p>
                <a href="#">Thỏa thuận sử dụng</a>
              </p>
              <p>
                <a href="#">Chính sách bảo mật</a>
              </p>
            </div>
            <div className="connect__item">
              <a href="https://www.facebook.com/" target="_blank">
                <img src="/images/facebook-logo.png" alt />
              </a>
              <a href="https://zalo.me/pc" target="_blank">
                <img src="/images/zalo-logo.png" alt />
              </a>
            </div>
          </div>
          <div className="footer__contact">
            <div className="contact__logo">
              <img src="/images/logo-zion.jpg" alt />
            </div>
            <div className="contact__content">
              <h1>CINEMA – SẢN PHẨM CỦA CÔNG TY CỔ PHẦN D-L</h1>
              <p>
                Địa chỉ: Z06 Đường số 13, Phường Tân Thuận Đông, Quận 7, Tp. Hồ
                Chí Minh, Việt Nam.
              </p>
              <p>Giấy chứng nhận đăng ký kinh doanh số: 0101659783,</p>
              <p>
                đăng ký thay đổi lần thứ 30, ngày 22 tháng 01 năm 2020 do Sở kế
                hoạch và đầu tư Thành phố Hồ Chí Minh cấp.
              </p>
              <p>Số Điện Thoại (Hotline): 1900 545 436</p>
              <p>
                Email: <span>support@cinema.vn</span>
              </p>
            </div>
            <div className="contact__check">
              <a href="http://online.gov.vn/" target="_blank">
                <img src="/images/logo-checked.png" alt />
              </a>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}
