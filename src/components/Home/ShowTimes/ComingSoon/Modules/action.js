import * as ActionType from "./constants";
import axios from "axios";

export const fetchComingSoonHome = () => {
  return (dispatch) => {
    dispatch(actComingSoonHomeRequest());
    axios({
      url: "https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhimTheoNgay?maNhom=GP09&soTrang=1&soPhanTuTrenTrang=30&tuNgay=01%2F03%2F2021&denNgay=01%2F12%2F2021",
      method: "GET",
    })
      .then((res) => {
        console.log("action trong coming soon", res.data);
        dispatch(actComingSoonHomeSuccess(res.data));
      })
      .catch((err) => {
        dispatch(actComingSoonHomeFail(err));
      });
  };
};

const actComingSoonHomeRequest = () => {
  return {
    type: ActionType.COMING_SOON_HOME_REQUEST,
  };
};

const actComingSoonHomeSuccess = (data) => {
  return {
    type: ActionType.COMING_SOON_HOME_SUCCESS,
    payload: data,
  };
};

const actComingSoonHomeFail = (err) => {
  return {
    type: ActionType.COMING_SOON_HOME_FAIL,
    payload: err,
  };
};
