export const COMING_SOON_HOME_REQUEST =
  "@comingSoonHomeReducer/COMING_SOON_HOME_REQUEST";
export const COMING_SOON_HOME_SUCCESS =
  "@comingSoonHomeReducer/COMING_SOON_HOME_SUCCESS";
export const COMING_SOON_HOME_FAIL =
  "@comingSoonHomeReducer/COMING_SOON_HOME_FAIL";
