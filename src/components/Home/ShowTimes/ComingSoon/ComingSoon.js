import React, { Component } from "react";
import { connect } from "react-redux";
import Loader from "./../../../Loader/Loader";
import MovieItem from "./../MovieItem";

class ComingSoon extends Component {
  renderComingSoon = () => {
    const { loading, data } = this.props;
    if (loading) {
      return <Loader />;
    }
    if (data && data.length > 8) {
      let comingSoon = data.slice(0, 8);
      return comingSoon.map((movie) => {
        return <MovieItem key={movie.maPhim} movie={movie} />;
      });
    }
  };

  renderComingSoon2 = () => {
    const { loading, data } = this.props;
    if (loading) {
      return <Loader />;
    }
    if (data && data.length > 8) {
      let comingSoon = data.slice(8, 16);
      return comingSoon.map((movie) => {
        return <MovieItem key={movie.maPhim} movie={movie} />;
      });
    }
  };

  render() {
    console.log(this.props.data);
    return (
      <>
        <div
          id="indicators2"
          className="carousel slide slides__film"
          data-ride="carousel"
        >
          <div className="carousel-inner">
            <div className="carousel-item active">
              <div className="show__film">{this.renderComingSoon()}</div>
            </div>
            <div className="carousel-item">
              <div className="show__film">{this.renderComingSoon2()}</div>
            </div>
          </div>
          <a
            className="carousel-control-prev arrow1__slides"
            href="#indicators2"
            role="button"
            data-slide="prev"
          >
            <img className="arrow__img" src="./images/back-session.png" alt />
            <span className="sr-only">Previous</span>
          </a>
          <a
            className="carousel-control-next arrow2__slides"
            href="#indicators2"
            role="button"
            data-slide="next"
          >
            <img className="arrow__img" src="./images/next-session.png" alt />
            <span className="sr-only">Next</span>
          </a>
        </div>
        <div className="showTimes-mobile-small d-none">
          <div className="show__film">
            {this.renderComingSoon()}
            {this.renderComingSoon2()}
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.comingSoonHomeReducer.loading,
    data: state.comingSoonHomeReducer.data,
  };
};

export default connect(mapStateToProps, null)(ComingSoon);
