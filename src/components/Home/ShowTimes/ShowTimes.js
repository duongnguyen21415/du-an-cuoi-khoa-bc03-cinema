import React, { Component } from "react";
import { fetchListMovieHome } from "../ModulesListMovie/action";
import { connect } from "react-redux";
import Loader from "./../../Loader/Loader";
import MovieItem from "./MovieItem";
import ComingSoon from "./ComingSoon/ComingSoon";
import { fetchComingSoonHome } from "./ComingSoon/Modules/action";
import classNames from "classnames";
import { Element } from "react-scroll";

class ShowTimes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMovie: true,
      styleActive: true,
    };
  }

  handleShowMovie = (event) => {
    event.preventDefault();
    this.setState({ showMovie: true, styleActive: true });
  };

  handleShowComingSoon = (event) => {
    event.preventDefault();
    this.setState({ showMovie: false, styleActive: false });
  };

  componentDidMount() {
    this.props.fetchShowTimes();
    this.props.fetchComingSoon();
    console.log(document.getElementById("lich-chieu").getBoundingClientRect());
  }

  renderShowTimes = () => {
    const { loading, data } = this.props;
    if (loading) {
      return <Loader />;
    }
    if (data && data.length > 8) {
      let showTime = data.slice(0, 8);
      return showTime.map((movie) => {
        return <MovieItem key={movie.maPhim} movie={movie} />;
      });
    }
  };

  renderShowTimes2 = () => {
    const { loading, data } = this.props;
    if (loading) {
      return <Loader />;
    }
    if (data && data.length > 8) {
      let showTime = data.slice(8, 16);
      return showTime.map((movie) => {
        return <MovieItem key={movie.maPhim} movie={movie} />;
      });
    }
  };

  render() {
    console.log(this.props.data);
    return (
      <Element id="lich-chieu" className="showTimes">
        <div className="container">
          <ul className="nav__film">
            <li className={classNames({ styleActive: this.state.styleActive })}>
              <a href="" onClick={this.handleShowMovie}>
                Đang Chiếu
              </a>
            </li>
            <li
              className={classNames({ styleActive: !this.state.styleActive })}
            >
              <a href="" onClick={this.handleShowComingSoon}>
                Sắp Chiếu
              </a>
            </li>
          </ul>
          {this.state.showMovie ? (
            <>
              <div
                id="indicators"
                className="carousel slide slides__film"
                data-ride="carousel"
              >
                <div className="carousel-inner">
                  <div className="carousel-item active">
                    <div className="show__film">{this.renderShowTimes()}</div>
                  </div>
                  <div className="carousel-item">
                    <div className="show__film">{this.renderShowTimes2()}</div>
                  </div>
                </div>
                <a
                  className="carousel-control-prev arrow1__slides"
                  href="#indicators"
                  role="button"
                  data-slide="prev"
                >
                  <img
                    className="arrow__img"
                    src="./images/back-session.png"
                    alt
                  />
                  <span className="sr-only">Previous</span>
                </a>
                <a
                  className="carousel-control-next arrow2__slides"
                  href="#indicators"
                  role="button"
                  data-slide="next"
                >
                  <img
                    className="arrow__img"
                    src="./images/next-session.png"
                    alt
                  />
                  <span className="sr-only">Next</span>
                </a>
              </div>
              <div className="showTimes-mobile-small d-none">
                <div className="show__film">
                  {this.renderShowTimes()}
                  {this.renderShowTimes2()}
                </div>
              </div>
            </>
          ) : (
            <ComingSoon />
          )}
        </div>
      </Element>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.listMovieHomeReducer.loading,
    data: state.listMovieHomeReducer.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchShowTimes: () => {
      dispatch(fetchListMovieHome());
    },
    fetchComingSoon: () => {
      dispatch(fetchComingSoonHome());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ShowTimes);
