import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchTheaterToolsHome } from "../Slides/Tools/ModulesTools/action";
import Loader from "./../../Loader/Loader";
import { fetchTheaterInfoHome } from "./Modules/action";
import TheaterCluster from "./TheaterCluster/TheaterCluster";
import TheaterShowTimes from "./TheaterShowTimes/TheaterShowTimes";
import { Element } from "react-scroll";

class Theaters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      logoIndex: 0,
    };
  }

  componentDidMount() {
    // const { movie } = this.props;
    // this.props.getTheaterMovie(movie[0].maPhim);
    this.props.getTheaterMovie(1322);
  }

  renderLogoThearterSystem = () => {
    const { loading, thearterSystem } = this.props;
    if (loading) return <Loader />;
    if (thearterSystem) {
      console.log(thearterSystem.heThongRapChieu);
      return thearterSystem.heThongRapChieu.map((item, index) => {
        console.log(item, thearterSystem.heThongRapChieu);
        if (index === 0) {
          return (
            <>
              <li key={item.maHeThongRap} name={item.maHeThongRap}>
                <a
                  onClick={(e) => {
                    this.handleTheaterClusterShowtimes(item.maHeThongRap);
                    let arrayLogoName = document.getElementsByClassName("logo");
                    console.log(arrayLogoName, arrayLogoName[0].className);
                    for (let i = 0; i < arrayLogoName.length; i++) {
                      arrayLogoName[i].className = arrayLogoName[
                        i
                      ].className.replace("active", "");
                    }
                    e.target.className += " active";
                  }}
                >
                  <img src={item.logo} alt="logo" className="logo active" />
                </a>
              </li>
            </>
          );
        } else {
          return (
            <>
              <li key={item.maHeThongRap} name={item.maHeThongRap}>
                <a
                  onClick={(e) => {
                    this.handleTheaterClusterShowtimes(item.maHeThongRap);
                    let arrayLogoName = document.getElementsByClassName("logo");
                    console.log(arrayLogoName, arrayLogoName[0].className);
                    for (let i = 0; i < arrayLogoName.length; i++) {
                      arrayLogoName[i].className = arrayLogoName[
                        i
                      ].className.replace("active", "");
                    }
                    e.target.className += " active";
                  }}
                >
                  <img src={item.logo} alt="logo" className="logo" />
                </a>
              </li>
            </>
          );
        }
      });
    }
  };

  handleTheaterClusterShowtimes = (maHeThongRap) => {
    this.props.getTheaterDetail(maHeThongRap);
  };

  render() {
    const { thearterSystem } = this.props;
    console.log("theater", thearterSystem);
    return (
      <>
        <Element id="cum-rap" className="theaters">
          <div className="container">
            <div className="theaters__info">
              <ul className="theaters__logo">
                {this.renderLogoThearterSystem()}
              </ul>
              <div className="theaters__address scroll-bar">
                {thearterSystem ? (
                  <>
                    {this.handleTheaterClusterShowtimes(
                      this.props.thearterSystem.heThongRapChieu[0].maHeThongRap
                    )}
                  </>
                ) : (
                  <></>
                )}
                <TheaterCluster />
              </div>
              <div className="theaters__film scroll-bar">
                <TheaterShowTimes />
              </div>
            </div>
          </div>
        </Element>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    // movie: state.listMovieHomeReducer.data,
    loading: state.theaterToolsHomeReducer.loading,
    thearterSystem: state.theaterToolsHomeReducer.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getTheaterMovie: (maPhim) => {
      dispatch(fetchTheaterToolsHome(maPhim));
    },
    getTheaterDetail: (maHeThongRap) => {
      dispatch(fetchTheaterInfoHome(maHeThongRap));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Theaters);
