import React, { Component } from "react";
import { connect } from "react-redux";
import { actTheaterDetailHomeSuccess } from "../Modules/action";

class TheaterCluster extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imagesCinema: [
        {
          maCumRap: "bhd",
          img: "./images/bhd-img-theater.jpg",
        },
        {
          maCumRap: "cgv",
          img: "./images/cgv-img-theater.png",
        },
        {
          maCumRap: "cns",
          img: "./images/cns-img-theater.jpg",
        },
        {
          maCumRap: "glx",
          img: "./images/galaxy-img-theater.jpg",
        },
        {
          maCumRap: "lot",
          img: "./images/lotte-img-theater.jpg",
        },
        {
          maCumRap: "meg",
          img: "./images/megastar-img-theater.jpg",
        },
      ],
      theaterAddress: "",
      theaterName: "",
    };
  }

  renderInfoTheater = () => {
    const { TheaterCluster } = this.props;
    console.log(TheaterCluster);
    if (TheaterCluster) {
      return TheaterCluster[0].lstCumRap.map((item, index) => {
        let imgCinema = this.state.imagesCinema.filter(
          (items) => items.maCumRap === item.maCumRap.slice(0, 3)
        );
        if (index === 0) {
          return (
            <>
              <div
                key={item.maCumRap}
                className="address__item"
                onClick={(e) => {
                  console.log(e.target);
                  this.handleTheaterShowtimes(item.diaChi, item.tenCumRap);
                  let array = document.getElementsByClassName("shadow__item");
                  console.log(array, array[0].className);
                  for (let i = 0; i < array.length; i++) {
                    array[i].className = array[i].className.replace(
                      "active",
                      ""
                    );
                  }
                  e.target.className += " active";
                }}
              >
                <div className="address__img">
                  <img src={imgCinema[0].img} alt="imgCinema" />
                </div>
                <div className="address__info">
                  <span style={{ color: "#8bc541" }}>{item.tenCumRap}</span>
                  <p>{item.diaChi}</p>
                </div>
                <div className="shadow__item active"></div>
              </div>
            </>
          );
        } else {
          return (
            <>
              <div
                key={item.maCumRap}
                className="address__item"
                onClick={(e) => {
                  console.log(e.target);
                  this.handleTheaterShowtimes(item.diaChi, item.tenCumRap);
                  let array = document.getElementsByClassName("shadow__item");
                  console.log(array, array[0].className);
                  for (let i = 0; i < array.length; i++) {
                    array[i].className = array[i].className.replace(
                      "active",
                      ""
                    );
                  }
                  e.target.className += " active";
                }}
              >
                <div className="address__img">
                  <img src={imgCinema[0].img} alt="imgCinema" />
                </div>
                <div className="address__info">
                  <span style={{ color: "#8bc541" }}>{item.tenCumRap}</span>
                  <p>{item.diaChi}</p>
                </div>
                <div className="shadow__item"></div>
              </div>
            </>
          );
        }
      });
    }
  };

  handleTheaterShowtimes = (diaChi, tenCumRap) => {
    const { TheaterCluster } = this.props;
    let listTheaterMovie = TheaterCluster[0].lstCumRap.filter(
      (item) => item.diaChi === diaChi && item.tenCumRap === tenCumRap
    );
    this.props.getListTheaterMovie(listTheaterMovie);
  };

  handleTheaterIndex = (diaChi, tenCumRap) => {
    this.setState({
      theaterAddress: diaChi,
      theaterName: tenCumRap,
    });
  };

  render() {
    const { TheaterCluster } = this.props;
    return (
      <>
        {this.renderInfoTheater()}
        {TheaterCluster ? (
          <>
            {this.handleTheaterShowtimes(
              TheaterCluster[0].lstCumRap[0].diaChi,
              TheaterCluster[0].lstCumRap[0].tenCumRap
            )}
          </>
        ) : (
          <></>
        )}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    TheaterCluster: state.theaterInfoHomeReducer.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getListTheaterMovie: (listTheaterMovie) => {
      dispatch(actTheaterDetailHomeSuccess(listTheaterMovie));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TheaterCluster);
