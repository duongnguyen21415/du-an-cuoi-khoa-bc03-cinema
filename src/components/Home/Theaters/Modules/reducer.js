import * as ActionType from "./constants";

let initialState = {
  loading: false,
  data: null,
  err: null,
  listTheaterMovie: null,
};

const theaterInfoHomeReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.THEATER_INFO_HOME_REQUEST:
      state.loading = true;
      state.data = null;
      state.err = null;
      return { ...state };
    case ActionType.THEATER_INFO_HOME_SUCCESS:
      state.loading = false;
      state.data = action.payload;
      state.err = null;
      return { ...state };
    case ActionType.THEATER_INFO_HOME_FAIL:
      state.loading = false;
      state.data = null;
      state.err = action.payload;
      return { ...state };

    case ActionType.THEATER_DETAIL_HOME_SUCCESS:
      state.listTheaterMovie = action.payload;
      return { ...state };
    default:
      return { ...state };
  }
};

export default theaterInfoHomeReducer;
