import * as ActionType from "./constants";
import axios from "axios";

export const fetchTheaterInfoHome = (idSystem) => {
  return (dispatch) => {
    dispatch(actTheaterInfoHomeRequest());
    axios({
      url: `https://movie0706.cybersoft.edu.vn/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maHeThongRap=${idSystem}&maNhom=GP09`,
      method: "GET",
    })
      .then((res) => {
        console.log("data trong action theater", res.data);
        let rapKhong = res.data[0].lstCumRap.filter((item) => {
          console.log(item);

          let phimKhong = item.danhSachPhim.filter((phim) => {
            console.log(phim);

            let data = phim.lstLichChieuTheoPhim.filter((lich) => {
              return new Date(lich.ngayChieuGioChieu) >= new Date();
            });
            phim.lstLichChieuTheoPhim = data;
            console.log(data, phim);
            return phim.lstLichChieuTheoPhim.length !== 0;
          });
          item.danhSachPhim = phimKhong;
          console.log(item);
          return item.danhSachPhim.length !== 0;
        });
        res.data[0].lstCumRap = rapKhong;
        console.log("sau khi filter", res.data);
        dispatch(actTheaterInfoHomeSuccess(res.data));
      })
      .catch((err) => {
        dispatch(actTheaterInfoHomeFail(err));
      });
  };
};

const actTheaterInfoHomeRequest = () => {
  return {
    type: ActionType.THEATER_INFO_HOME_REQUEST,
  };
};

const actTheaterInfoHomeSuccess = (data) => {
  return {
    type: ActionType.THEATER_INFO_HOME_SUCCESS,
    payload: data,
  };
};

const actTheaterInfoHomeFail = (err) => {
  return {
    type: ActionType.THEATER_INFO_HOME_FAIL,
    payload: err,
  };
};

export const actTheaterDetailHomeSuccess = (data) => {
  return {
    type: ActionType.THEATER_DETAIL_HOME_SUCCESS,
    payload: data,
  };
};
