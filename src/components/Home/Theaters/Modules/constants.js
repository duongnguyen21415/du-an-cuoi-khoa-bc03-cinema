export const THEATER_INFO_HOME_REQUEST =
  "@theaterInfoHomeReducer/THEATER_INFO_HOME_REQUEST";
export const THEATER_INFO_HOME_SUCCESS =
  "@theaterInfoHomeReducer/THEATER_INFO_HOME_SUCCESS";
export const THEATER_INFO_HOME_FAIL =
  "@theaterInfoHomeReducer/THEATER_INFO_HOME_FAIL";
export const THEATER_DETAIL_HOME_SUCCESS =
  "@theaterInfoHomeReducer/THEATER_DETAIL_HOME_SUCCESS";
