import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class TheaterShowTimes extends Component {
  renderTheaterShowTimes = () => {
    const { listTheaterMovie } = this.props;
    console.log(listTheaterMovie);
    if (listTheaterMovie) {
      return listTheaterMovie[0].danhSachPhim.map((item) => {
        return (
          <>
            <div key={item.maPhim} className="film__item">
              <div className="film__info">
                <div className="film__img">
                  <Link to={`/detail-movie/${item.maPhim}`}>
                    <img src={item.hinhAnh} alt />
                  </Link>
                </div>
                <div className="film__detail">
                  <p className="name">
                    <span>{item.maPhim}</span>
                    {item.tenPhim}
                  </p>
                  <p className="time">Cinema - IMDB - Steven</p>
                </div>
              </div>
              <div className="film__schedule">
                <div className="film__quality">2D Digital</div>
                <div className="film__time">
                  {item.lstLichChieuTheoPhim.map((items) => {
                    return (
                      <Link
                        key={items.maLichChieu}
                        to={`/booking/${items.maLichChieu}`}
                      >
                        <span>
                          {new Date(
                            items.ngayChieuGioChieu
                          ).toLocaleTimeString()}
                        </span>
                      </Link>
                    );
                  })}
                </div>
              </div>
            </div>
          </>
        );
      });
    }
  };

  render() {
    return <>{this.renderTheaterShowTimes()}</>;
  }
}

const mapStateToProps = (state) => {
  return {
    listTheaterMovie: state.theaterInfoHomeReducer.listTheaterMovie,
  };
};

export default connect(mapStateToProps, null)(TheaterShowTimes);
