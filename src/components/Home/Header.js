import React, { Component } from "react";
import { NavLink, Redirect, Link } from "react-router-dom";
import { connect } from "react-redux";
import { actFetchLogoutHome } from "./../../homePages/LoginHome/Modules/action";
import Avatar from "@material-ui/core/Avatar";
import { scroller } from "react-scroll";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogin: true,
      redirect: null,
    };
  }
  logOutHome = () => {
    localStorage.removeItem("userHome") ||
      localStorage.removeItem("userGoo") ||
      localStorage.removeItem("userFace");
    this.setState({
      isLogin: !this.state.isLogin,
    });
  };

  handleScrollTo = (name) => {
    if (window.location.href === "http://localhost:3000/") {
      scroller.scrollTo(name, {
        duration: 1000,
        smooth: true,
      });
    } else {
      this.setState({
        redirect: "/",
      });
      setTimeout(() => {
        scroller.scrollTo(name, {
          duration: 1000,
          smooth: true,
        });
      }, 1000);
    }
  };

  render() {
    if (this.state.redirect) {
      this.setState({
        redirect: null,
      });
      return <Redirect to={this.state.redirect} />;
    }
    return (
      <header className="header">
        <nav className="navbar navbar-expand-lg">
          <NavLink className="navbar-brand" to="/">
            <img src="/images/logo-cinema-2.jpg" alt="logo" />
          </NavLink>
          <div className="navbar-2" id="navbarNavDropdown">
            <div className="nav__1">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <a
                    className="nav-link"
                    onClick={() => {
                      this.handleScrollTo("lich-chieu");
                    }}
                  >
                    Lịch Chiếu
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    className="nav-link"
                    onClick={() => {
                      this.handleScrollTo("cum-rap");
                    }}
                  >
                    Cụm rạp
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    className="nav-link"
                    onClick={() => {
                      this.handleScrollTo("tin-tuc");
                    }}
                  >
                    Tin Tức
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    className="nav-link"
                    onClick={() => {
                      this.handleScrollTo("ung-dung");
                    }}
                  >
                    Ứng Dụng
                  </a>
                </li>
              </ul>
            </div>
            <div className="nav__2 navbar-light">
              <ul className="navbar-nav align-items-center">
                {localStorage.getItem("userHome") ||
                localStorage.getItem("userGoo") ||
                localStorage.getItem("userFace") ? (
                  <>
                    <li className="nav-item user-home d-flex align-items-center">
                      {(localStorage.getItem("userHome") && (
                        <Avatar>
                          {JSON.parse(localStorage.getItem("userHome"))
                            .hoTen.slice(0, 1)
                            .toUpperCase()}
                        </Avatar>
                      )) ||
                        (localStorage.getItem("userGoo") && (
                          <Avatar
                            src={
                              JSON.parse(localStorage.getItem("userGoo"))
                                .photoURL
                            }
                          />
                        )) ||
                        (localStorage.getItem("userFace") && (
                          <Avatar
                            src={
                              JSON.parse(localStorage.getItem("userFace"))
                                .photoURL
                            }
                          />
                        ))}
                      <span className="border-right pr-3 pl-1">
                        {(localStorage.getItem("userHome") &&
                          JSON.parse(localStorage.getItem("userHome")).hoTen) ||
                          (localStorage.getItem("userGoo") &&
                            JSON.parse(localStorage.getItem("userGoo"))
                              .displayName) ||
                          (localStorage.getItem("userFace") &&
                            JSON.parse(localStorage.getItem("userFace"))
                              .displayName)}
                      </span>
                      <button
                        type="button"
                        className="btn btn-secondary btn-sigh-out"
                        onClick={this.logOutHome}
                      >
                        Đăng Xuất
                      </button>
                    </li>
                  </>
                ) : (
                  <li className="nav-item">
                    <NavLink
                      className="nav-link icon__1 d-flex align-items-center"
                      to="/login"
                    >
                      <Avatar />
                      <span className="border-right pr-3 pl-1">Đăng Nhập</span>
                    </NavLink>
                  </li>
                )}
                {/* <li className="nav-item dropdown">
                  <a
                    className="nav-link dropdown-toggle"
                    href="#"
                    id="navbarDropdownMenuLink"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <i className="fa fa-map-marker-alt mr-1" />
                    Hồ Chí Minh
                  </a>
                  <div
                    className="dropdown-menu"
                    aria-labelledby="navbarDropdownMenuLink"
                  >
                    <a className="dropdown-item" href="#">
                      Hồ Chí Minh
                    </a>
                    <a className="dropdown-item" href="#">
                      Bình Dương
                    </a>
                    <a className="dropdown-item" href="#">
                      Biên Hòa
                    </a>
                    <a className="dropdown-item" href="#">
                      Vũng Tàu
                    </a>
                    <a className="dropdown-item" href="#">
                      Đà Lạt
                    </a>
                  </div>
                </li> */}
              </ul>
            </div>
          </div>
        </nav>
        <div className="header-mobile btn-group dropdown">
          <NavLink className="navbar-brand" to="/">
            <img src="/images/logo-cinema-2.jpg" alt="logo" />
          </NavLink>
          <button
            type="button"
            className="main"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            <i className="fa fa-bars" />
          </button>
          <div className="dropdown-menu">
            <ul className="navbar-nav">
              {localStorage.getItem("userHome") ||
              localStorage.getItem("userGoo") ||
              localStorage.getItem("userFace") ? (
                <>
                  <li className="nav-item user-home d-flex">
                    {(localStorage.getItem("userHome") && (
                      <Avatar>
                        {JSON.parse(localStorage.getItem("userHome"))
                          .hoTen.slice(0, 1)
                          .toUpperCase()}
                      </Avatar>
                    )) ||
                      (localStorage.getItem("userGoo") && (
                        <Avatar
                          src={
                            JSON.parse(localStorage.getItem("userGoo")).photoURL
                          }
                        />
                      )) ||
                      (localStorage.getItem("userFace") && (
                        <Avatar
                          src={
                            JSON.parse(localStorage.getItem("userFace"))
                              .photoURL
                          }
                        />
                      ))}
                    <span>
                      {(localStorage.getItem("userHome") &&
                        JSON.parse(localStorage.getItem("userHome")).hoTen) ||
                        (localStorage.getItem("userGoo") &&
                          JSON.parse(localStorage.getItem("userGoo"))
                            .displayName) ||
                        (localStorage.getItem("userFace") &&
                          JSON.parse(localStorage.getItem("userFace"))
                            .displayName)}
                    </span>
                  </li>
                </>
              ) : (
                <li className="nav-item">
                  <NavLink
                    className="nav-link icon__1 d-flex align-items-center"
                    to="/login"
                  >
                    <Avatar />
                    <span className="border-right pr-3 pl-1">Đăng Nhập</span>
                  </NavLink>
                </li>
              )}
            </ul>
            <ul className="navbar-nav">
              <li className="nav-item">
                <a
                  className="nav-link"
                  onClick={() => {
                    this.handleScrollTo("lich-chieu");
                  }}
                >
                  Lịch Chiếu
                </a>
              </li>
              <li className="nav-item cum-rap-mobile">
                <a
                  className="nav-link"
                  onClick={() => {
                    this.handleScrollTo("cum-rap");
                  }}
                >
                  Cụm rạp
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link"
                  onClick={() => {
                    this.handleScrollTo("tin-tuc");
                  }}
                >
                  Tin Tức
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link"
                  onClick={() => {
                    this.handleScrollTo("ung-dung");
                  }}
                >
                  Ứng Dụng
                </a>
              </li>
            </ul>
            <ul>
              {localStorage.getItem("userHome") ? (
                <>
                  <button
                    type="button"
                    className="btn btn-secondary"
                    onClick={this.logOutHome}
                  >
                    Đăng Xuất
                  </button>
                </>
              ) : (
                <></>
              )}
            </ul>
          </div>
        </div>
      </header>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    infoUserHome: state.loginHomeReducer.data,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    fetchLogoutHome: () => {
      dispatch(actFetchLogoutHome());
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Header);
