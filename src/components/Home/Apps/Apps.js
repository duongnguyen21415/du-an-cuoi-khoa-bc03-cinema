import React, { Component } from "react";
import Slider from "react-slick";
import { Element } from "react-scroll";

export default class Apps extends Component {
  render() {
    const settingsSlick = {
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
    };
    return (
      <>
        <Element id="ung-dung" className="apps">
          <div className="apps__bg">
            <div className="container">
              <div className="apps__content">
                <div className="apps__item1">
                  <h1>Ứng dụng tiện lợi dành cho</h1>
                  <h1>người yêu điện ảnh</h1>
                  <p>
                    Không chỉ đặt vé, bạn còn có thể bình luận phim, chấm điểm
                    rạp và đổi quà hấp dẫn.
                  </p>
                  <button
                    onClick={() => {
                      alert(
                        "Tính năng đang được xây dựng. Vui lòng quay lại sau"
                      );
                    }}
                  >
                    App miễn phí - Tải về ngay!
                  </button>
                  <p>
                    CINEMA có hai phiên bản : &nbsp;
                    <a
                      href="#"
                      onClick={(e) => {
                        e.preventDefault();
                        alert(
                          "Tính năng đang được xây dựng. Vui lòng quay lại sau"
                        );
                      }}
                    >
                      iOS
                    </a>
                    &nbsp; &amp; &nbsp;
                    <a
                      href="#"
                      onClick={(e) => {
                        e.preventDefault();
                        alert(
                          "Tính năng đang được xây dựng. Vui lòng quay lại sau"
                        );
                      }}
                    >
                      {" "}
                      Android
                    </a>
                  </p>
                </div>
                <div className="apps__item2">
                  <div className="apps__item2-img">
                    <img src="./images/img-app.png" alt />
                    <div className="apps__item2-slick">
                      <Slider {...settingsSlick}>
                        <div>
                          <img src="./images/apps-1.jpg" alt />
                        </div>
                        <div>
                          <img src="./images/apps-2.jpg" alt />
                        </div>
                        <div>
                          <img src="./images/apps-3.jpg" alt />
                        </div>
                        <div>
                          <img src="./images/apps-4.jpg" alt />
                        </div>
                      </Slider>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Element>
      </>
    );
  }
}
