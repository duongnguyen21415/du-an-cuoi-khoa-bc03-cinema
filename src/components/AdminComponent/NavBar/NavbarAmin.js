import React, { useState } from "react";
import { Link, NavLink } from "react-router-dom";
import Avatar from "@material-ui/core/Avatar";
import "./../../../assets/sass/admin/reponsiveDash.scss";
import "./../../../assets/css/admin.css";

export default function NavbarAdmin() {
  const renderName = () => {
    let name = JSON.parse(localStorage.getItem("admin")).hoTen;
    return name && <h5>Hello,{name}</h5>;
  };
  const logOut = () => {
    localStorage.removeItem("admin");
    // props.history.replace("/admin/auth");
  };
  return (
    <>
      <div className="sidebar" data-color="blue">
        {/* using: data-color="blue | azure | green | orange | red | purple */}
        <div className="sidebar-wrapper">
          <div className="logo position-relative">
            {renderName()}
            <Link
              className="btn btn-danger button_logout position-absolute"
              onClick={logOut}
              to="/admin/auth"
            >
              Đăng xuất
            </Link>
          </div>
          <ul className="nav routeAdmin">
            <li className="nav-item">
              <NavLink className="nav-link" to="/admin/dashboard">
                <i className="nc-icon nc-chart-pie-35" />
                <p>Dashboard</p>
              </NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/admin/addfilm">
                <i className="nc-icon nc-circle-09" />
                <p>Quản lí Phim</p>
              </NavLink>
            </li>

            <li>
              <NavLink className="nav-link" to="/admin/addAdmin">
                <i className="nc-icon nc-notes" />
                <p>Quản Lí Tài Khoản Admin</p>
              </NavLink>
            </li>
          </ul>
          <div className="container__mobile">
            <p className="routeAdmin-mobile">
              <a
                className="btn btn-primary"
                data-toggle="collapse"
                href="#multiCollapseExample1"
                role="button"
                aria-expanded="false"
                aria-controls="multiCollapseExample1"
              >
                <i class="fa fa-chevron-down"></i>
              </a>
            </p>
            <div className="row navbar__mobile">
              <div className="col">
                <div
                  className="collapse multi-collapse"
                  id="multiCollapseExample1"
                >
                  <ul className="nav">
                    <li className="nav-item">
                      <NavLink className="nav-link" to="/admin/dashboard">
                        <i className="nc-icon nc-chart-pie-35" />
                        <p>Dashboard</p>
                      </NavLink>
                    </li>
                    <li>
                      <NavLink className="nav-link" to="/admin/addfilm">
                        <i className="nc-icon nc-circle-09" />
                        <p>Quản lí Phim</p>
                      </NavLink>
                    </li>

                    <li>
                      <NavLink className="nav-link" to="/admin/addAdmin">
                        <i className="nc-icon nc-notes" />
                        <p>Quản Lí Tài Khoản Admin</p>
                      </NavLink>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
