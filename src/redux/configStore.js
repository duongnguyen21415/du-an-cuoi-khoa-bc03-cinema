import { combineReducers, createStore, compose, applyMiddleware } from "redux";
import loginHomeReducer from "./../homePages/LoginHome/Modules/reducer";
import thunk from "redux-thunk";
import authReducer from "./../adminPage/AuthPage/module/reducer";
import sighUpHomeReducer from "./../homePages/SighUp/Modules/reducer";
import addADMINReducer from "./../adminPage/AddAdmin/module/reducer";
import listMovieHomeReducer from "../components/Home/ModulesListMovie/reducer";
import modalHomeReducer from "./../components/Home/Modal/Modules/reducer";
import quanliMovieReducer from "./../adminPage/AddFilm/module/reducer";
import detailMovieHomeReducer from "../homePages/DetailMovie/Modules/reducer";
import theaterToolsHomeReducer from "../components/Home/Slides/Tools/ModulesTools/reducer";
import quanliDashboardReducer from "./../adminPage/Dashboard/module/reducers";
import comingSoonHomeReducer from "./../components/Home/ShowTimes/ComingSoon/Modules/reducer";
import listBookingHomeReducer from "./../homePages/Booking/Modules/reducer";
import bookingHomeReducer from "./../homePages/Booking/Modules/BookingTickets/reducer";
import theaterInfoHomeReducer from "../components/Home/Theaters/Modules/reducer";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
  loginHomeReducer,
  authReducer,
  sighUpHomeReducer,
  addADMINReducer,
  listMovieHomeReducer,
  modalHomeReducer,
  quanliMovieReducer,
  detailMovieHomeReducer,
  theaterToolsHomeReducer,
  quanliDashboardReducer,
  comingSoonHomeReducer,
  listBookingHomeReducer,
  bookingHomeReducer,
  theaterInfoHomeReducer,
});

const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);

export default store;
